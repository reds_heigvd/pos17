/*
 * Copyright (c) 2014, 2015 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 * Copyright (c) 2016, 2017 REDS Institute, HEIG-VD
 *
 * Contributors:
 * - Daniel Rossier (2015-2017)
 *
 * This file contains some piece of code for various tests in SO3.
 */

#include <delay.h>
#include <thread.h>
#include <tinyprintf.h>

#include <mach/fpga.h>

int fn1(void *args) {
	int i = 0;

	printk("Thread #1\n");
	while (1) {
		printk("--> th 1: %d\n", i++);
	}

	return 0;
}

int fn2(void *args) {
	int i = 0;

	printk("Thread #1\n");
	while (1) {
		printk("--> th 2: %d\n", i++);
	}

	return 0;
}

int test_mmu(void *args) {
	printk("LEDs tester\n");

	while (1) {
		lwfpga_set_bits_hword(0x00, 0xFF);
		udelay(1000000);
		lwfpga_clear_bits_hword(0x00, 0xFF);
		udelay(1000000);
	}
}

extern int schedcount;

/*
 * First SO3 main kernel thread
 */
int main_kernel(void *args)
{

	/* Kernel never returns ! */
	printk("***********************************************\n");
	printk("Going to infinite loop...\n");
	printk("Kill Qemu with CTRL-a + x or reset the board\n");
	printk("***********************************************\n");

	kernel_thread(fn1, "fn1", NULL);
	kernel_thread(fn2, "fn2", NULL);
	//kernel_thread(fn1, "fn1", NULL);
	//kernel_thread(fn2, "fn2", NULL);
	kernel_thread(test_mmu, "test_mmu", NULL);

	while (1);


	return 0;
}
