/* jitter.c
 *
 * Auteur : Sydney Hauke
 */

#include <jitter.h>
#include <device/timer.h>
#include <types.h>
#include <tinyprintf.h>

void
jitter_init(jitter_measure_t *measures)
{
  memset(measures, 0, sizeof(jitter_measure_t));
}

void
jitter_measure(jitter_measure_t *jit)
{
  if(jit->cursor != (NR_MEASURES - 1)) {
    jit->measures[jit->cursor] = clocksource_timer.read();
    jit->cursor++;
  }
  else {
    uint8_t i = 1;
    for(; i < NR_MEASURES; i++) {
      jit->measures[i - 1] = jit->measures[i];
    }

    jit->measures[NR_MEASURES-1] = clocksource_timer.read();
  }
}

uint64_t
jitter_get(jitter_measure_t *jit)
{
  /* Find the maximum relative jitter */
  uint64_t max = 0;

  /* We need at least 3 measures */
  if(jit->cursor < 2) {
    return max;
  }

  int64_t delay0, delay1, variation;
  uint8_t i = 1;
  for(; i < (NR_MEASURES-1); i++) {
    delay0 = ((int64_t)jit->measures[i] - (int64_t)jit->measures[i-1]);
    delay1 = ((int64_t)jit->measures[i+1] - (int64_t)jit->measures[i]);

    variation = delay0 - delay1;
    variation *= (variation >= 0) ? 1:-1;

    if((uint64_t)variation > max) {
      max = variation;
    }
  }

  return max;
}


uint64_t
jitter_latency(jitter_measure_t *jit)
{
  /* We need at least 2 measures */

  if(jit->cursor < 1) {
    return 0;
  }

  int64_t last_delay;
  last_delay = ((int64_t)jit->measures[NR_MEASURES-1] - (int64_t)jit->measures[NR_MEASURES - 2]);

  last_delay += (last_delay >= 0) ? 1:-1;

  return (uint64_t)last_delay;
}
