 /*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2014: Romain Bornet
 * - 2014-2017: Daniel Rossier
 * - 2017: Lucas Elisei & David Truan
 */

// Enable to print debug messages.
#if 1
#define DEBUG
#endif

// Enable to test deadline_handler.
#if 0
#define DO_TESTS
#endif

#include <common.h>
#include <tinyprintf.h>
#include <calibrate.h>
#include <schedule.h>
#include <memory.h>
#include <timer.h>

#include <device/driver.h>
#include <device/timer.h>

#include <asm/atomic.h>
#include <asm/setup.h>

#include <apps/main_thread.h>

#ifdef DO_TESTS
/**
 * Deadline handlers for testing.
 * Prints a message and sets a deadline.
 */
static void deadline_test_1(void) {
	printk("deadline_test_1\n");

	add_deadline(NSEC, deadline_test_1);
}

static void deadline_test_5(void) {
	printk("deadline_test_5\n");

	add_deadline(5 * NSEC, deadline_test_5);
}
#endif

void kernel_panic(void)
{
	printk("%s: entering infinite loop...\n", __func__);
	while (1);
}

void _bug(char *file, int line)
{
	printk("BUG in %s at line: %d\n", file, line); \
	kernel_panic();
}

int rest_init(void *dummy) {

	/* Start the idle thread */
	tcb_idle = kernel_thread(thread_idle, "idle", NULL);

	/* Start a first SO3 thread (main app thread) */

	kernel_thread(main_kernel, "main_kernel", NULL);

	thread_exit(0);

	return 0;
}

void kernel_start(void) {

	/* Basic low-level initialization */
	setup_arch();

	printk("\n\n********** Smart Object Oriented SO3 Operating System **********\n");
	printk("Copyright (c) 2014-2017 REDS Institute, HEIG-VD, Yverdon\n");
	printk("Copyright (c) 2016-2017 Sootech SA\n");
	printk("Version 2017.2.0\n");

	printk("\n\nNow bootstraping the kernel ...\n");

	/* Memory manager subsystem initialization */
	memory_init();

	devices_init();

	DBG("******** SO3 - Smart Object Oriented OS ********\n");
	DBG("Kernel is running !!!\n");
	DBG("************************************************\n\n");

#ifdef DO_TEST
	// Set the bits of the leds (0x00)
	lwfpga_set_bits_hword(0x00, 0xAA);
	// Read the bits of the leds (you should read 0xAA)
	printk("Read HAL: 0x%X\n", lwfpga_read_word(0x00));
#endif

	/* Scheduler init */
	scheduler_init();

	DBG("Scheduler initialization done\n");

	// Initialize deadlines_handler.
	deadlines_handler_init();

<<<<<<< HEAD
	DBG("Deadlines handler initialization done\n");

	local_irq_enable();

	DBG("Local irq enabled\n");

	// Can calibrate delay now that we have periodic timer interrupt.
=======
>>>>>>> rpi3
	calibrate_delay();

#ifdef DO_TESTS
	// Add deadlines for testing.
	add_deadline(NSEC, deadline_test_1);
	add_deadline(5 * NSEC, deadline_test_5);
#else
	/*
	 * Perform the rest of bootstrap sequence in a separate thread, so that
	 * we can rely on the scheduler for subsequent threads.
	 */
	kernel_thread(rest_init, "so3_boot", NULL);
<<<<<<< HEAD
#endif
=======

	schedule();
>>>>>>> rpi3

	/*
	 * We loop forever, just the time the scheduler gives the hand to a ready thread.
	 * After that, this code will never be executed anymore ...
	 */
	while (true)
		__asm("wfi");

}
