
/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2017: Daniel Rossier
 *
 */

#include <sync.h>
#include <schedule.h>
#include <spinlock.h>

/*
 * Wait for completion function.
 * The critical section is managed by the spinlock of the waitqueue.
 * When the thread has to wait, IRQs are disabled to avoid race condition between
 * the call to waiting(). The IRQ
 */
void wait_for_completion(completion_t *completion) {
	queue_thread_t q_tcb;

	ASSERT(local_irq_is_enabled());

	spin_lock(&completion->wait.lock);

	q_tcb.tcb = current();

	if (!completion->count) {

		list_add_tail(&q_tcb.list, &completion->wait.tcb_list);

		do {
			local_irq_disable();

			spin_unlock(&completion->wait.lock);

			waiting();

			local_irq_enable();

			spin_lock(&completion->wait.lock);
		} while (!completion->count);
	}
	completion->count--;

	spin_unlock(&completion->wait.lock);

}

/*
 * Wake a thread waiting on a completion.
 */
void complete(completion_t *completion) {
	queue_thread_t *curr, *next;

	spin_lock(&completion->wait.lock);

	completion->count++;

	list_for_each_entry_safe(curr, next, &completion->wait.tcb_list, list) {

		ready(curr->tcb);

		list_del(&curr->list);

	}


	spin_unlock(&completion->wait.lock);

}

void init_completion(completion_t *completion) {
	INIT_LIST_HEAD(&completion->wait.tcb_list);
	spin_lock_init(&completion->wait.lock);

	completion->count = 0;
}
