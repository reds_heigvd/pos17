/*
 * delay.c
 *
 */

#include <delay.h>
#include <schedule.h>


/**
 * Wait-free loop based on the jiffy_ref
 */
void udelay(u64 us) {
	u64 delay = 0ull;
	u64 target;

	target = (us  * jiffies_ref) / ( 2 * 1000000ull / HZ);

	while (delay < target) {
		delay++;
	}
}
