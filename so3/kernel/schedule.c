/*
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 *
 * Contributors:
 * - 2014-2017: Daniel Rossier
 * - 2017: Lucas Elisei & David Truan
 *
 * This file contains some piece of code for various tests in SO3.
 */

#if 0
#define DEBUG
#endif

#include <compiler.h>
#include <schedule.h>
#include <thread.h>
#include <heap.h>
#include <softirq.h>
#include <mutex.h>

#include <device/timer.h>

static struct list_head readyThreads;
static struct list_head zombieThreads;

/* Global list of process */
struct list_head proc_list;
struct tcb *tcb_idle;

static tcb_t *current_thread;

u64 jiffies = 0ull;
u64 jiffies_ref = 0ull;

tcb_t *current(void) {
	return current_thread;
}

inline void set_current(struct tcb *tcb) {
	current_thread = tcb;
}

/*
 * Insert a new thread in the ready list.
 */
void ready(tcb_t *tcb) {
	uint32_t flags;
	queue_thread_t *cur;

	flags = local_irq_save();

	tcb->state = THREAD_STATE_READY;
	cur = (queue_thread_t *) malloc(sizeof(queue_thread_t));
	BUG_ON(cur == NULL);

	cur->tcb = tcb;

	/* Insert the thread at the end of the list */
	list_add_tail(&cur->list, &readyThreads);

	local_irq_restore(flags);
}

/*
 * Put a thread in the waiting state and invoke the scheduler.
 * It is assumed that the caller manages the waiting queue.
 */
void waiting(void) {
	uint32_t flags;

	flags = local_irq_save();

	current()->state = THREAD_STATE_WAITING;

	schedule();

	local_irq_restore(flags);
}

/*
 * Put a thread into the zombie queue.
 */
void zombie(void) {
	queue_thread_t *cur;
	uint32_t flags;

	ASSERT(current()->state == THREAD_STATE_RUNNING);
	ASSERT(!list_empty(&readyThreads));

	flags = local_irq_save();

	current()->state = THREAD_STATE_ZOMBIE;

	cur = (queue_thread_t *) malloc(sizeof(queue_thread_t));
	BUG_ON(cur == NULL);

	cur->tcb = current();

	/* Insert the thread at the end of the list */
	list_add_tail(&cur->list, &zombieThreads);

	local_irq_restore(flags);

	schedule();
}

/*
 * Remove a thread from the zombie list.
 */
void remove_zombie(struct tcb *tcb) {
	queue_thread_t *cur;
	tcb_t *_tcb;
	struct list_head *pos, *q;

	local_irq_disable();

	ASSERT(tcb != NULL);
	ASSERT(tcb->state == THREAD_STATE_ZOMBIE);

	list_for_each_safe(pos, q, &zombieThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);

		_tcb = cur->tcb;

		if (tcb == _tcb) {
			list_del(pos);

			free(cur);
			return ;
		}

	}

	printk("%s: zombie thread %d not found.\n", __func__, tcb->tid);

	BUG();

}

/*
 * Wake up a thread which is in waiting state.
 * If the thread passed as argument is not sleeping, we just call schedule().
 */
void wake_up(struct tcb *tcb) {

	ready(tcb);
	schedule();
}

/*
 * Pick up the next ready thread to be scheduled according
 * to the scheduling policy.
 * IRQs are off.
 */
static tcb_t *next_thread(void) {

	queue_thread_t *cur;
	tcb_t *tcb;
	struct list_head *pos, *q;

	ASSERT(!local_irq_is_enabled());

	list_for_each_safe(pos, q, &readyThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);

		tcb = cur->tcb;
		list_del(pos);

		free(cur);

		return tcb;
	}

	return NULL;
}

/*
 * Main scheduling function.
 */
void schedule(void) {

	tcb_t *prev, *next;
	uint32_t flags;

	flags = local_irq_save();

	/* Scheduling policy: at the moment start the first ready thread */

	prev = current();
	next = next_thread();

	if (unlikely(next == NULL))
		next = tcb_idle;

	if ((next != NULL) && (next != prev)) {
		DBG("Now scheduling thread ID: %d name: %s\n", next->tid, next->name);

		/*
		 * The current threads (here prev) can be in different states, not only running; it may be in *waiting* or *zombie*
		 * depending on the thread activities. Hence, we put it in the ready state ONLY if the thread is in *running*.
		 */
		if ((prev != NULL) && (prev->state == THREAD_STATE_RUNNING) && (likely(prev != tcb_idle)))
			ready(prev);

		next->state = THREAD_STATE_RUNNING;
		set_current(next);

		__switch_context(prev, next);
	}


	local_irq_restore(flags);

}

/*
 * Dump the ready threads
 */
void dump_ready(void) {
	struct list_head *pos;
	queue_thread_t *cur;

	printk("Dumping the ready-threads queue: \n");

	if (list_empty(&readyThreads)) {
		printk("  <empty>\n");
		return ;
	}

	list_for_each(pos, &readyThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);
		printk("  Thread ID: %d name: %s state: %d\n", cur->tcb->tid, cur->tcb->name, cur->tcb->state);

	}

}

/*
 * Dump the zombie threads
 */
void dump_zombie(void) {
	struct list_head *pos;
	queue_thread_t *cur;

	printk("Dumping the zombie-threads queue: \n");

	if (list_empty(&zombieThreads)) {
		printk("  <empty>\n");
		return ;
	}

	list_for_each(pos, &zombieThreads)
	{
		cur = list_entry(pos, queue_thread_t, list);
		printk("  Thread ID: %d state: %d\n", cur->tcb->tid, cur->tcb->state);

	}

}

/*
 * Dump information of the scheduling waitqueues.
 */
void dump_sched(void) {
	dump_ready();
	dump_zombie();
}

void scheduler_init(void) {

	/* Low-level thread initialization */
	threads_init();

	/* Initialize the main queues addressed by the scheduler */
	INIT_LIST_HEAD(&readyThreads);
	INIT_LIST_HEAD(&zombieThreads);

	/* Initialize the global list of processes */
	INIT_LIST_HEAD(&proc_list);

	/* Convert period to nanoseconds (10 ms = 10 * 10^6 ns). */
	periodic_timer.period = (NSEC / HZ);

	/* Start periodic timer. */
	if (periodic_timer.start)
		periodic_timer.start();

	register_softirq(SCHEDULE_SOFTIRQ, schedule);

	set_current(NULL);

}
