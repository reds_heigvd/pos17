/*
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 *
 * Contributors:
 * - Daniel Rossier (2015-2017)
 *
 */

#include <bitops.h>
#include <types.h>
#include <spinlock.h>
#include <softirq.h>

uint32_t softirq_bitmap;

static softirq_handler softirq_handlers[NR_SOFTIRQS];

DEFINE_SPINLOCK(softirq_pending_lock);

/*
 * Perform actions of related pending softirqs if any.
 */
void do_softirq(void)
{

	unsigned int i;
	unsigned int pending;
	unsigned int loopmax;

	loopmax = 0;

	for ( ; ; )
	{
		//spin_lock(&softirq_pending_lock);

		/* Any pending softirq ? */
		if ((pending = softirq_bitmap) == 0) {
			spin_unlock(&softirq_pending_lock);
			break;
		}

		i = find_first_bit(&pending, BITS_PER_INT);

		if (loopmax > 100)   /* Probably something wrong ;-) */
			printk("%s: Warning trying to process softirq for quite a long time (i = %d)...\n", __func__, i);

		clear_bit(i, (unsigned long *) &softirq_bitmap);

		spin_unlock(&softirq_pending_lock);

		(*softirq_handlers[i])();

		loopmax++;
	}
}

void register_softirq(int nr, softirq_handler handler)
{
    ASSERT(nr < NR_SOFTIRQS);

    softirq_handlers[nr] = handler;
}


void raise_softirq(unsigned int nr)
{
  set_bit(nr, (unsigned long *) &softirq_bitmap);
}


void softirq_init(void)
{
  spin_lock_init(&softirq_pending_lock);
}
