/*
 * calibrate.c
 */


#include <calibrate.h>
#include <schedule.h>


/**
 * Set the global variable jiffy_usec with the number of loop for 1us
 */
void calibrate_delay(void){

	printk("%s: calibrating...", __func__);

	u64 __jiffies = jiffies;

	jiffies_ref = 0;
	while (jiffies == __jiffies) ;
	__jiffies = jiffies;

	while (jiffies == __jiffies) jiffies_ref++;

	printk("done. jiffies_ref = %llx\n", jiffies_ref);

}
