/*
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 *
 * Contributors:
 * - 2014-2017: Daniel Rossier
 * - June 2017: Xavier Ruppen
 *
 * This file contains some piece of code for various tests in SO3.
 */

#include <compiler.h>
#include <thread.h>
#include <common.h>
#include <heap.h>
#include <errno.h>

#include <asm/processor.h>

static unsigned int tid_current = 0;

static struct list_head threads;

/*
 * Find a thread (tcb_t) from its tid. The thread belongs to the process @pcb.
 *
 * Return NULL if no process as been found.
 */
tcb_t *find_thread_by_tid(pcb_t *pcb, uint32_t tid) {
	tcb_t *tcb;
	struct list_head *pos;

	list_for_each(pos, &threads)
	{
		tcb = list_entry(pos, tcb_t, list);
		if (tcb->tid == tid)
			return tcb;

	}

	/* Not found */
	return NULL;
}

/*
 * Thread stack management
 */
bool kernel_stack_slot[THREAD_MAX];

/*
 * Get the kernel stack (top), full descending - The kernel stack is divided into small stack areas
 * used for individual kernel *and* user threads (in SVC mode).
 *
 * The first stack area is the initial system stack and remains preserved so far.
 * The first thread stack slot ID #0 starts right under this area.
 */
uint32_t get_kernel_stack_top(uint32_t slotID) {
	return (uint32_t) ((void *) &__stack_top - THREAD_STACK_SIZE - slotID*THREAD_STACK_SIZE);
}

/*
 * Reserve a stack slot dedicated to a kernel thread.
 */
int get_kernel_stack_slot(void)
{
	unsigned int i;

	for (i = 0; i < THREAD_MAX; i++) {
		if (!kernel_stack_slot[i]) {
			kernel_stack_slot[i] = true;

			return i;
		}
	}

	/* No free stack slot */
	return -1;
}

/*
 * Reserve a stack slot dedicated to a user thread.
 * This function returns the stack base address.
 *
 * The general stack base address is retrieved from __svc_start__.
 * No need to reserve the first stack slot.
 */
void free_kernel_stack_slot(int slotID)
{
	kernel_stack_slot[slotID] = false;
}

/*
 * Thread exit routine
 */
void thread_exit(int *exit_status)
{
	queue_thread_t *cur;
	struct list_head *pos, *q;

	local_irq_disable();

	ASSERT(current()->state == THREAD_STATE_RUNNING);

	if (exit_status != NULL)
		current()->exit_status = *exit_status;

	/* Wake up possible waiting threads */
	list_for_each_safe(pos, q, &current()->joinQueue)
	{
		cur = list_entry(pos, queue_thread_t, list);

		ready(cur->tcb);

		/* The entry will be freed by a joining thread */
	}

	zombie();

	/* Should never reach this point... */
	BUG();
}


void thread_prologue(int(*th_fn)(void *arg), void *arg)
{
	int exit_status = 0;

	/* Check for pending IRQs */

	exit_status = th_fn(arg);

	thread_exit(&exit_status); /* Setting exit code */
}

/*
 * Thread idle
 */
int thread_idle(void *dummy)
{
	/* Endless loop */
	while (true)
		/* For the moment nothing... The call to schedule() is ensured right after an IRQ processing. */
		__asm("wfi");


	return 0;
}

void set_thread_registers(tcb_t *thread, cpu_regs_t *regs)
{
	memcpy(&thread->cpu_regs, regs, sizeof(cpu_regs_t));
}

/*
 * Thread creation routine
 *
 * @start_routine: function to be threaded; if NULL, it means we are along a fork() processing.
 * @name: name of the thread. Warning ! The caller must foresee a concatenation of the tid to the string.
 * @arg: pointer to possible args
 * @pcb: NULL means it is a pure kernel thread, otherwise is is a user thread.
 */
tcb_t *thread_create(int (*start_routine) (void *), const char *name, void *arg)
{
	tcb_t *tcb;
	uint32_t flags;

	flags = local_irq_save();

	tcb = (tcb_t *) malloc(sizeof(tcb_t));

	if (tcb == NULL) {
		printk("%s: failed to alloc memory.\n", __func__);
		kernel_panic();
	}

	tcb->tid = tid_current++;

	/* We append the tid to the thread name */
	sprintf(tcb->name, "%s_%d", name, tcb->tid);

	tcb->th_fn = start_routine;
	tcb->th_arg = arg;

	tcb->state = THREAD_STATE_NEW;

	/* Init the thread kernel stack (svc mode) for both kernel and user thread. */
	tcb->stack_slotID = get_kernel_stack_slot();

	if (tcb->stack_slotID < 0) {
		printk("No available stack for a new thread\n");
		kernel_panic();
	}

	/* Prepare registers for future restore in switch_context() */
	tcb->cpu_regs.r4 = (unsigned int) tcb->th_fn;
	tcb->cpu_regs.r5 = (unsigned int) tcb->th_arg; /* First argument */

	tcb->cpu_regs.sp = get_kernel_stack_top(tcb->stack_slotID);

	tcb->cpu_regs.lr = (unsigned int) __thread_prologue_kernel;

	/* Initialize the join queue associated to this thread */
	INIT_LIST_HEAD(&tcb->joinQueue);

	/* We do not expect to have the idle thread as a "ready" thread */
	if (start_routine != thread_idle)
		ready(tcb);

	local_irq_restore(flags);

	return tcb;
}

tcb_t *kernel_thread(int (*start_routine)(void *), const char *name, void *arg)
{
	return thread_create(start_routine, name, arg);
}

/*
 * @clean_thread is called when no joining threads a waiting, or an exec() is performed
 * so that the binary image is fully replaced.
 */
void clean_thread(tcb_t *tcb) {

	/* Definitively remove the thread */
	free_kernel_stack_slot(tcb->stack_slotID);

	ASSERT(list_empty(&tcb->joinQueue));

	/* Remove the thread from the zombie list */
	remove_zombie(tcb);

	/* Finally, free the tcb struct */
	free(tcb);
}

/*
 * Join a thread.
 * Perform synchronization and remove the tcb of the joined thread.
 *
 * The target thread which we are trying to join will be definitively removed
 * from the system when no other threads are joining it too.
 */
int thread_join(tcb_t *tcb)
{
	queue_thread_t *cur;
	int exit_status;
	tcb_t *_tcb;
	struct list_head *pos, *q;
	uint32_t flags;

	flags = local_irq_save();

	/* Check if the thread already finished */
	if (tcb->state != THREAD_STATE_ZOMBIE) {

		/* Register us in the join queue attached to the target thread */

		cur = (queue_thread_t *) malloc(sizeof(queue_thread_t));
		if (cur == NULL) {
			printk("%s: cannot allocate memory to register in join queue.\n", __func__);
			BUG();
		}

		cur->tcb = current();

		/* Append ourself in the queue */
		list_add_tail(&cur->list, &tcb->joinQueue);


		/* Waiting for the target thread; we suspend ourself... */
		waiting();

		/*
		 * At this point, we are awake; the target thread is over in *zombie* state.
		 * So, we leave the join queue belonging to this thread.
		 */
		list_for_each_safe(pos, q, &tcb->joinQueue)
		{
			cur = list_entry(pos, queue_thread_t, list);
			_tcb = cur->tcb;

			if (_tcb == current()) {

				list_del(pos);
				free(cur);
				break;
			}
		}
	}

	/* The joined thread *must* be in zombie */
	ASSERT(tcb->state == THREAD_STATE_ZOMBIE);

	exit_status = tcb->exit_status;

	/* Now, if we are the last which is woken up, we can proceed with the tcb removal. */
	if (list_empty(&tcb->joinQueue))
		clean_thread(tcb);

	local_irq_restore(flags);

	return exit_status;
}


void threads_init(void)
{
	int i;

	INIT_LIST_HEAD(&threads);

	for (i = 0; i < THREAD_MAX; i++)
		kernel_stack_slot[i] = false; /* Unallocated stack */
}
