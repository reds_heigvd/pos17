/*
 * File:		so3/kernel/timer.c
 *
 * Created by: 	Daniel Rossier
 * Created on:	18.09.2017
 *
 * Modified by: Lucas Elisei & David Truan
 * Modified on: 09.11.2017
 *
 * Deadlines handler for TIMER_SOFTIRQ.
 */

#include <device/timer.h>
#include <heap.h>
#include <list.h>
#include <softirq.h>
#include <spinlock.h>
#include <timer.h>
#include <types.h>
#include <heap.h>

// List head.
static deadline_t deadlines_list;

// List size.
static uint8_t size;

// For concurrency.
DEFINE_SPINLOCK(deadlines_handler_lock);

/**
 * Handles deadlines each time TIMER_SOFTIRQ is raised.
 */
static void deadlines_handler(void) {
	// Get current time from clocksource.
	uint64_t current_time = clocksource_timer.read();
	struct list_head *cursor, *tmp;
	deadline_t *deadline;

	// Iterate over each deadline and execute its corresponding handler if the
	// deadline was reached.
	list_for_each_safe(cursor, tmp, &(deadlines_list.list)) {
		deadline = list_entry(cursor, deadline_t, list);
		// Check if the deadline is expired.
		if (deadline->absolute_time <= current_time) {
			if (deadline->handler != NULL) {
				deadline->handler();
			}

<<<<<<< HEAD
			// Delete the deadline from the list.
			spin_lock(&deadlines_handler_lock);
			list_del(cursor);
			--size;
			spin_unlock(&deadlines_handler_lock);
=======
			//spin_lock(&deadline_handler_lock);
			list_del(&(deadline->list));
			//spin_unlock(&deadline_handler_lock);
>>>>>>> rpi3
			free(deadline);
		}
	}
}

/**
 * Adds deadline to the list. A deadline is defined by a relative time (in
 * nanoseconds) and handler to call when the deadline is reached.
 */
int add_deadline(uint64_t relative_time_ns, deadline_handler handler) {
	// Get current time from clocksource.
	uint64_t current_time = clocksource_timer.read();

	// Check size.
	spin_lock(&deadlines_handler_lock);
	if (size >= NB_MAX_DEADLINES) {
		spin_unlock(&deadlines_handler_lock);
		return -1;
	}
	spin_unlock(&deadlines_handler_lock);

	// Get enough memory.
	deadline_t *deadline_to_add = (deadline_t *)malloc(sizeof(deadline_t));
	if (deadline_to_add == NULL) {
		return -1;
	}

	// Initialize deadline struct.
	INIT_LIST_HEAD(&(deadline_to_add->list));
	deadline_to_add->absolute_time = current_time + relative_time_ns;
	deadline_to_add->handler = handler;

	// Add deadline to the list.
	spin_lock(&deadlines_handler_lock);
	list_add(&(deadline_to_add->list), &(deadlines_list.list));
	++size;
	spin_unlock(&deadlines_handler_lock);

<<<<<<< HEAD
	return 0;
}

/**
 * Initializes the deadlines handler.
 */
void deadlines_handler_init(void) {
	// Initialize list.
	INIT_LIST_HEAD(&(deadlines_list.list));
	size = 0;

	// Call `deadlines_handler` each time TIMER_SOFTIRQ is raised.
	register_softirq(TIMER_SOFTIRQ, deadlines_handler);
=======
	//spin_lock(&deadline_handler_lock);
	list_add(&(deadline_to_add.list), &(deadlines_list.list));
	//spin_unlock(&deadline_handler_lock);
>>>>>>> rpi3
}
