/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * 
 * The contents of this file is strictly under the property of Sootech SA and
 * must not be shared in any case.
 *
 * Contributors:
 *
 * - June 2017: Alexandre Malki
 *
 */

#ifndef _STAT_H_
#define _STAT_H_

#define FILENAME_SIZE 256

struct stat {
	char	st_name[FILENAME_SIZE];		/* Filename */
	unsigned long	st_size; 		/* Size of file */
	unsigned long	ctime;			/* Time of last modification in sec*/
	unsigned char	st_flags;		/* Regular file flag (not supported on fat) */
	unsigned char	st_mode;		/* Protection not used (not supported on fat) */
};


#endif /* _STAT_H_ */
