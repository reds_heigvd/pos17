#ifndef HEAP_H
#define HEAP_H

#include <stddef.h>
#include <sizes.h>

#if 0
#define TRACKING
#endif

#define HEAP_SIZE 	(SZ_1M)

/* there's a list for each size. The size changes over the process lifetime. */
struct mem_chunk {

    /* Pointer to the next memchunk of a different size */
    struct mem_chunk *next_list;

    /* Pointer to the next memchunk of this size. */
    struct mem_chunk *next_chunk;

    /* Refer to the head of the (2nd-dimension) list for this size */
    struct mem_chunk *head;

    size_t size; /* size in bytes of the user area (this does NOT include the size of mem_chunk) */

    /* Used in case of specific alignment on address - These bytes are lost */
    size_t padding_bytes;
};
typedef struct mem_chunk mem_chunk_t;


#ifndef TRACKING
void *malloc(size_t size);
void *memalign(size_t size, unsigned int alignment);
#else
void *malloc_log(size_t size, const char *filename, const char *fname, const int line);
void *memalign_log(size_t size, unsigned int alignment, const char *filename, const char *fname, const int line);

#define malloc(x) malloc_log(x, __FILE__, __func__, __LINE__)
#define memalign(x,y) memalign_log(x, y, __FILE__, __func__, __LINE__)

#endif

void free(void *ptr);
void printHeap(void);

void heap_init(void);
void dump_heap(const char *info);


#endif /* HEAP_H */
