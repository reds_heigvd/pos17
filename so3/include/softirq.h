
/*
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 *
 * Contributors:
 * - Daniel Rossier, May 2017
 *
 */

#ifndef SOFTIRQ_H
#define SOFTIRQ_H

/* Low-latency softirqs come first in the following list. */
enum {
	TIMER_SOFTIRQ = 0,
	SCHEDULE_SOFTIRQ,
	NR_COMMON_SOFTIRQS
};

#define NR_SOFTIRQS	NR_COMMON_SOFTIRQS

typedef void (*softirq_handler)(void);

void register_softirq(int nr, softirq_handler handler);
void raise_softirq(unsigned int nr);
void softirq_init(void);

#endif /* SOFTIRQ_H */

