/* string.h
 *
 * From http://clc-wiki.net/wiki/C_standard_library:string.h (public domain license)
 *      - removed _NC_RESTRICT
 *      - removed __np_size_t
 *      - commented out functions unused for now
 */

#ifndef STRING_H
#define STRING_H

#ifndef NULL
#define NULL 0
#endif

#include <stddef.h>

void *memchr(const void *, int, size_t);
int memcmp(const void *, const void *, size_t);
void *memcpy(void *, const void *, size_t);
void *memmove(void *, const void *, size_t);
void *memset(void *, int, size_t);

void downcase(char *str);

void uppercase(char *str, int len);

/*
 char   *strcat(char *, const char *);
 */
char *strchr(const char *, int);
int strcmp(const char *, const char *);
int strncmp(const char *, const char *, size_t);
size_t strnlen(const char * s, size_t count);

unsigned long simple_strtoul(const char *cp, char **endp, unsigned int base);
unsigned long long simple_strtoull(const char *cp, char **endp, unsigned int base);
char *strdup(const char *s);

int sprintf(char *buf, const char *fmt, ...);

/*
 int     strcoll(const char *, const char *);
 */
char *strcpy(char *, const char *);
/*
 size_t  strcspn(const char *, const char *);
 char   *strerror(int);
 */
size_t strlen(const char *);
/*
 char   *strncat(char *, const char *, size_t);
 */
char *strncpy(char *, const char *, size_t);
/*
 char   *strpbrk(const char *, const char *);
 char   *strrchr(const char *, int);
 size_t  strspn(const char *, const char *);
 char   *strstr(const char *, const char *);
 char   *strtok(char *, const char *);
 size_t  strxfrm(char *, const char *, size_t);
 */

#endif /* STRING_H */
