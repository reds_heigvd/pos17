/*
 * calibrate.h
 *
 */

#ifndef CALIBRATE_H_
#define CALIBRATE_H_

#include "calibrate.h"

/**
 * Set the global variable jiffy_usec with the number of loop for 1us
 */
void calibrate_delay(void);

//Don't use before calibrate
extern unsigned long jiffy_usec;

#endif /* CALIBRATE_H_ */
