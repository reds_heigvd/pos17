/*
 * File:		so3/include/timer.h
 *
 * Created by: 	Daniel Rossier
 * Created on:	18.09.2017
 *
 * Modified by: Lucas Elisei & David Truan
 * Modified on: 09.11.2017
 */

#ifndef INCLUDE_TIMER_H_
#define INCLUDE_TIMER_H_

#include <list.h>
#include <types.h>

// Maximum number of deadlines.
#define NB_MAX_DEADLINES 32

typedef void (*deadline_handler)(void);

// Structure that represents a deadline.
typedef struct deadline {
	struct list_head list;		// List entry
	uint64_t absolute_time; 	// Absolute time (nanoseconds)
	deadline_handler handler;	// Handler to call when the deadline is reached
} deadline_t;

void deadlines_handler_init(void);
int add_deadline(uint64_t relative_time_ns, deadline_handler handler);

#endif /* INCLUDE_TIMER_H_ */
