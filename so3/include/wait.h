#ifndef __WAIT_H__
#define __WAIT_H__

/* all taken from linux */
#define WNOHANG		0x00000001
#define WUNTRACED	0x00000002
#define WSTOPPED	WUNTRACED
#define WEXITED		0x00000004
#define WCONTINUED	0x00000008
#define WNOWAIT		0x01000000	/* Don't reap, just poll status.  */


#endif /* __WAIT_H__ */
