/* jitter.h
 *
 * Auteur : Sydney Hauke
 */

#include <types.h>

# define NR_MEASURES 3

typedef struct jitter_measure {
  uint64_t measures[NR_MEASURES];
  uint8_t cursor;
} jitter_measure_t;

void jitter_init(jitter_measure_t *measures);

void jitter_measure(jitter_measure_t *measures);

uint64_t jitter_get(jitter_measure_t *measures);

uint64_t jitter_latency(jitter_measure_t *measures);
