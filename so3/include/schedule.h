

/*
 * schedule.h
 */
#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <list.h>
#include <thread.h>

#define HZ      100


extern u64 jiffies;
extern u64 jiffies_ref;

struct tcb;

extern struct tcb *tcb_idle;

void scheduler_init(void);
void schedule(void);
void yield(void);

void ready(struct tcb *tcb);
void waiting(void);
struct tcb *pick_waiting_thread(struct tcb *tcb);
void zombie(void);

void remove_zombie(struct tcb *tcb);

void wake_up(struct tcb *tcb);

void dump_sched(void);

inline void set_current(struct tcb *tcb);
struct tcb *current(void);

void schedule_isr(void);

typedef struct queue_thread {
	struct list_head list;
	struct tcb *tcb;
} queue_thread_t;

#endif /* SCHEDULE_H */
