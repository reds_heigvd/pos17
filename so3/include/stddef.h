/* stddef.h
 * Based on POSIX specifications
 * http://pubs.opengroup.org/onlinepubs/9699919799/
 */

#ifndef _STDDEF_H
#define _STDDEF_H

/* mandatory macros */
#undef NULL
#define NULL ((void *) 0)


/* mandatory types */

typedef unsigned int size_t;

typedef signed int ptrdiff_t;

typedef unsigned int wchar_t;

#endif
