

#ifndef MEMORY_H
#define MEMORY_H

#include <types.h>

#include <asm/memory.h>
#include <generated/autoconf.h>

#define PAGE_SIZE		(1 << 12)
#define PAGE_MASK		(~(PAGE_SIZE - 1))

#define SECTION_SIZE	(1 << 20)
#define SECTION_MASK	(~(SECTION_SIZE - 1))

struct mem_info {
    uint32_t phys_base;
    uint32_t size;
};
typedef struct mem_info mem_info_t;

extern mem_info_t mem_info;

void clear_bss(void);
void memory_init(void);

/* Get memory informations from a device tree */
int get_mem_info(const void *fdt, mem_info_t *info);

uint32_t get_kernel_size(void);

void io_map_init(void);
uint32_t io_map(uint32_t phys_addr, uint32_t size);

#endif /* MEMORY_H */
