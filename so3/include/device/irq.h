/*
 * irq.h
 */

#ifndef IRQ_H
#define IRQ_H

#include <common.h>
#include <fdt.h>

/* Maximum physical interrupts than can be managed by SO3 */
#define NR_IRQS 255

enum irqreturn {
	IRQ_NONE		= (0 << 0),
	IRQ_HANDLED		= (1 << 0),
	IRQ_WAKE_THREAD		= (1 << 1),
};

typedef enum irqreturn irqreturn_t;
typedef irqreturn_t (*irq_handler_t)(int);

typedef enum  { IRQF_NO_REENABLE } irqflags_t;

typedef struct irqdesc {
	irq_handler_t action;
	irqflags_t irqflags;
} irqdesc_t;


extern int arch_irq_init(void);
extern void setup_arch(void);

/* IRQ controller */
typedef struct  {

    void (*irq_enable)(unsigned int irq);
    void (*irq_disable)(unsigned int irq);
    void (*irq_mask)(unsigned int irq);
    void (*irq_unmask)(unsigned int irq);
    void (*irq_handle)(void);

} irq_ops_t;

extern irq_ops_t irq_ops;

int irq_process(int irq);

void irq_init(void);

int irq_bind(int irq, irq_handler_t handler, int irqflags);
void irq_enable(int irq);

struct irq_desc *irq_to_desc(unsigned int irq);

void irq_enable(int irq);
void irq_disable(int irq);
void irq_mask(int irq);
void irq_unmask(int irq);

#endif /* IRQ_H */
