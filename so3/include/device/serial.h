
/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 *
 */

#ifndef SERIAL_H
#define SERIAL_H

typedef struct {
    int (*put_byte)(char c);
    char (*get_byte)(void);
} serial_ops_t;

extern serial_ops_t serial_ops;

int serial_putc(char c);
char serial_getc(void);

int serial_write(char *str, int max);
int serial_read(char *buf, int len);

extern void __ll_put_byte(char c);

void serial_init(void);

#endif /* SERIAL_H */
