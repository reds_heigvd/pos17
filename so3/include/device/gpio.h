#ifndef GPIO_H_
#define GPIO_H_

/* Maximum number of gpios supported by SO3 */
#define NR_GPIOS 128

typedef void (*gpio_int_handler_t)(unsigned int);

typedef struct gpio_desc {
	gpio_int_handler_t action;
} gpio_desc_t;

typedef enum gpio_dir_t {
	INPUT = 0,
	OUTPUT,
} gpio_dir_t;

typedef enum gpio_state_t {
	LOW = 0,
	HIGH,
} gpio_state_t;

typedef enum gpio_edge_dir_t {
	FALLING = 0,
	RISING,
} gpio_edge_dir_t;

/* Operations a gpio driver has to implement */
typedef struct {
	int (*is_valid)(unsigned int gpio);
	void (*config_dir)(unsigned int gpio, gpio_dir_t dir);
	gpio_state_t (*get)(unsigned int gpio);
	void (*set)(unsigned int gpio, gpio_state_t state);
	int (*enable_int)(unsigned int gpio, gpio_edge_dir_t edge);
	int (*disable_int)(unsigned int gpio);
} gpio_ops_t;

extern gpio_ops_t gpio_ops;
extern gpio_desc_t gpio_desc[NR_GPIOS];

/* Indicates if this gpio number is valid on the current
 * platform.
 *
 * @return : 1 if valid, 0 otherwise
 */
int gpio_is_valid(unsigned int gpio);

/* Sets the IO direction for the specified gpio. */
void gpio_config_dir(unsigned int gpio, gpio_dir_t dir);

/* Gets the current input state for the specified gpio.
 *
 * @return : input state
 */
gpio_state_t gpio_get(unsigned int gpio);

/* Sets the current output state for the specified gpio. */
void gpio_set(unsigned int gpio, gpio_state_t state);

/* Registers an action each time a gpio generates an interrupts.
 *
 * @return : -1 in case of an error, 0 otherwise
 */
int gpio_register_int(unsigned int gpio,
											gpio_edge_dir_t edge,
											gpio_int_handler_t action);

/* Unregisters the current action attached to the specified gpio
 *
 * @return : -1 in case of an error, 0 otherwise
 */
int gpio_unregister_int(unsigned int gpio);

/* Initializes the gpio subsystem */
void gpio_init(void);

#endif /* GPIO_H_ */
