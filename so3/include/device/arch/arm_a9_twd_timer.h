/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: arm_a9_twd_timer.h
 *
 * @brief   Here goes specification of ARM Cortex-A9 Watchdog Timer.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-07
 */
#ifndef SO3_ARM_A9_TWD_TIMER_H_
#define SO3_ARM_A9_TWD_TIMER_H_

#include <types.h>
#include <device/device.h>

/**
 * @brief  Structure declaring registers layout inside Watchdog Timer controller.
 * @see    DDI0407G_cortex_a9_mpcore_r3p0_trm, p64
 * @remark Columns in comments are:
 *             1. offset in TWD controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned
 */
struct arm_a9_twd_timer_regs
{
	uint32_t load;         /** @brief 0x00 - Load             [RW - 31:0] */
	uint32_t counter;      /** @brief 0x04 - Counter          [RW - 31:0] */
	uint32_t ctrl;         /** @brief 0x08 - Control          [RW - 15:8,3:0] */
	uint32_t intr_status;  /** @brief 0x0C - Interrupt Status [RW - 0] */
	uint32_t reset_status; /** @brief 0x10 - Reset Status     [RW - 0] */
	uint32_t disable;      /** @brief 0x14 - Disable          [WO - constants] */
};

#endif /* ARM_A9_TWD_TIMER_H_ */
