/*
 * Contributors:
 * December 2017 : Sydney Hauke
 */

#ifndef BCM283X_SYSTIMER_H
#define BCM283X_SYSTIMER_H

#include <types.h>

typedef struct bcm283x_systimer {
	volatile uint32_t cs; // control/status
	volatile uint32_t clo; // counter lower 32 bits
	volatile uint32_t chi; // counter higher 32 bits
	volatile uint32_t c0; // compare 0
	volatile uint32_t c1; // compare 1
	volatile uint32_t c2; // compare 2
	volatile uint32_t c3; // compare 3
} bcm283x_systimer_t;

/* CS register bits */
#define M0 0
#define M1 1
#define M2 2
#define M3 3

#endif
