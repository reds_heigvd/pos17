/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: zynq_swdt_timer.h
 *
 * @brief   Here goes specification of Zynq-7000 System Watchdog Timer.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-07
 */
#ifndef SO3_ZYNQ_SWDT_TIMER_H_
#define SO3_ZYNQ_SWDT_TIMER_H_

/**
 * @brief  Structure declaring registers layout inside Zynq-7000 System Watchdog Timer controller.
 * @see    ug585-Zynq-7000-TRM.pdf, p1750-1753
 * @remark Columns in comments are:
 *             1. offset in SWDT controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned
 */
struct zynq_swdt_timer_regs
{
	uint32_t wd_zero_mode; /** @brief 0x00 - WD zero mode         [WO - 23:12,RW -  8:7,2:0]  (0x1C0) */
	uint32_t counter_ctrl; /** @brief 0x04 - Counter Control      [WO - 25:14,RW - 13:2,1:0] (0x3FFC) */
	uint32_t restart_key;  /** @brief 0x08 - Restart key (no mem) [WO - 15:0]         */
	uint32_t status;       /** @brief 0x0C - Status               [RO -    0]         */
};

#endif /* SO3_ZYNQ_SWDT_TIMER_H_ */
