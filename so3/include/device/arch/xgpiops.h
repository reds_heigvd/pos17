/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: zynq_gpio.h
 *
 * @brief   Here goes specification of Zynq-7000 GPIO.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-07
 */
#ifndef SO3_XGPIOPS_H_
#define SO3_XGPIOPS_H_

/* -------------------------------------------------------------------------- *
 * ------------------------------ Memory layout ----------------------------- *
 * -------------------------------------------------------------------------- */

struct xgpiops_bank_cfg_regs
{
	uint32_t __reserved4[7];
	uint32_t direction_mode;
	uint32_t output_enable;
	uint32_t intr_mask_status;
	uint32_t intr_unmask;
	uint32_t intr_mask;
	uint32_t intr_status;
	uint32_t intr_type;
	uint32_t intr_polarity;
	uint32_t intr_edge;
};

/**
 * @brief  Structure declaring registers layout inside Zynq-7000 GPIO Controller.
 * @see    ug585-Zynq-7000-TRM.pdf, p1347-1375
 * @remark Columns in comments are:
 *             1. offset in GPIO Controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned
 */
struct xgpiops_regs
{
	uint32_t bank_mask_out_lsw[4][2]; /** @brief 0x00:0x1C - Maskable Output Data (GPIO Bank0-1 MIO, Bank2-3 EMIO, Lower + Upper 16bits) [M][M] (x,x,x,x,0,0,0,0) */

	uint32_t __reserved1[8];

	uint32_t bank_data_out[4];        /** @brief 0x40:0x4C - Output Data (GPIO Bank0-1 MIO, Bank2-3 EMIO) [RW] (x,x,0,0) */

	uint32_t __reserved2[4];

	uint32_t bank_data_in[4];         /** @brief 0x60:0x6C - Input  Data (GPIO Bank0-1 MIO, Bank2-3 EMIO) [RO] (x,x,0,0) */

	uint32_t __reserved3[94];

	struct xgpiops_bank_cfg_regs bank_cfg[4];
};

#endif /* SO3_XGPIOPS_H_ */
