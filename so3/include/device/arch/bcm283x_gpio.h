/*
 * bcm283x_gpio.h
 *
 * Auteur : Sydney Hauke
 */

#ifndef __BCM283X_GPIO_H
#define __BCM283X_GPIO_H

#include <types.h>

#define NB_GPIOS 54

/* There are 54 GPIOs. They are split in 2 banks of 32 bit
 * registers. GPIOs [0;31] are in bank 0, and GPIOs [32, 53]
 * are in bank 1.
 */
typedef struct bcm283x_gpio_t {
	volatile uint32_t gpfsel[6]; // Alternate function selectors
	volatile uint32_t _res0; // Reserved
	volatile uint32_t gpset[2]; // Pin output set 0/1
	volatile uint32_t _res1; // Reserved
	volatile uint32_t gpclr[2]; // Pin output clear 0/1
	volatile uint32_t _res2; // Reserved
	volatile uint32_t gplev[2]; // Pin level 0/1
	volatile uint32_t _res3; // Reserved
	volatile uint32_t gpeds[2]; // Pin event detect status 0/1
	volatile uint32_t _res4; // Reserved
	volatile uint32_t gpren[2]; // Pin rising edge detect enable 0/1
	volatile uint32_t _res5; // Reserved
	volatile uint32_t gpfen[2]; // Pin falling edge detect enable 0/1
	volatile uint32_t _res6; // Reserved
	volatile uint32_t gphen[2]; // Pin high detect enable 0/1
	volatile uint32_t _res7; // Reserved
	volatile uint32_t gplen[2]; // Pin low detect enable 0/1
	volatile uint32_t _res8; // Reserved
	volatile uint32_t gparen[2]; // Pin async. rising edge detect 0/1
	volatile uint32_t _res9; // Reserved
	volatile uint32_t gpafen[2]; // Pin async. falling edge detect 0/1
	volatile uint32_t _res10; // Reserved
	volatile uint32_t gppud; // Pin pull-up/down enable
	volatile uint32_t gppudclk[2]; // Pin pull-up/down enable clock 0/1
	volatile uint32_t _res11[4]; // Reserved
	volatile uint32_t test; // Test register

} bcm283x_gpio_t;

/* Alternate function selector field bits */
#define FSEL_INPUT 	0x0
#define FSEL_OUTPUT 0x1
#define FSEL_ALT0	0x4
#define FSEL_ALT1	0x5
#define FSEL_ALT2	0x6
#define FSEL_ALT3	0x7
#define FSEL_ALT4	0x3
#define FSEL_ALT5	0x2

#define FSEL_OFFSET(GPIO) ((GPIO % 10) * 3) // Field offset in fsel register

#endif
