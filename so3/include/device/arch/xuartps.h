/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: xuartps.h
 *
 * @brief   Here goes specification of Zynq-7000 UART interface.
 *          Following declarations provided here allow to initialize
 *          and handle UART communication.
 * @author  Luca   Sivillica [CREATED BY]
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 1.0
 * @since   2017-12-04
 */
#ifndef SO3_ZYNQ_UART_H_
#define SO3_ZYNQ_UART_H_

#include <types.h>

/* -------------------------------------------------------------------------- *
 * ------------------------------ Memory layout ----------------------------- *
 * -------------------------------------------------------------------------- */
/**
 * @brief  Structure declaring registers layout inside UART controller.
 * @see    ug585-Zynq-7000-TRM.pdf, p1775-1793
 * @remark Columns in comments are:
 *             1. offset in UART controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned, `(x)` means no reset
 */
struct xuartps_regs
{
	uint32_t ctrl;                /** @brief 0x00 - Control                      [RW  -   8:0] (0x128) */
	uint32_t mode;                /** @brief 0x04 - Mode                         [RW  -   9:0]         */
	uint32_t intr_enable;         /** @brief 0x08 - Interrupt Enable             [WO  -  12:0]         */
	uint32_t intr_disable;        /** @brief 0x0C - Interrupt Disable            [WO  -  12:0]         */
	uint32_t intr_mask;           /** @brief 0x10 - Interrupt Mask               [RO  -  12:0]         */
	uint32_t intr_channel_status; /** @brief 0x14 - Channel Interrupt Status     [WTC -  12:0]         */
	uint32_t baud_rate_gen;       /** @brief 0x18 - Baud Rate Generator          [RW  -  15:0] (0x28B) */
	uint32_t rx_timeout;          /** @brief 0x1C - Receiver Timeout             [RW  -   7:0]         */
	uint32_t rx_fifo_trig_lvl;    /** @brief 0x20 - Receiver FIFO Trigger Level  [RW  -   5:0]  (0x20) */
	uint32_t modem_ctrl;          /** @brief 0x24 - Modem Control                [RW  - 5,1:0]         */
	uint32_t modem_status;        /** @brief 0x28 - Modem Status   [RW - 8,RO - 7:4,WTC - 3:0]     (x) */
	uint32_t channel_status;      /** @brief 0x2c - Channel Status            [RO - 14:10,4:0]         */
	uint32_t tx_rx_fifo;          /** @brief 0x30 - FIFO                       [RW - 15:0,7:0]         */
	uint32_t baud_rate_div;       /** @brief 0x34 - Baud Rate Divider               [RW - 7:0]   (0xF) */
	uint32_t flow_delay;          /** @brief 0x38 - Flow Control Delay              [RW - 5:0]         */
	uint32_t ir_rx_pulse_width;   /** @brief 0x3c - IR Min Received Pulse Width                        */
	uint32_t ir_tx_pulse_width;   /** @brief 0x40 - IR Transmitted  Pulse Width                        */
	uint32_t tx_fifo_trig_lvl;    /** @brief 0x44 - Transmitter FIFO Trigger Level  [RW - 5:0]  (0x20) */
};

/* -------------------------------------------------------------------------- *
 * -------------------------------- Bitmasks -------------------------------- *
 * -------------------------------------------------------------------------- */
/*
 * Channel Status Register:
 * The channel status register (CSR) is provided to enable the control logic
 * to monitor the status of bits in the channel interrupt status register,
 * even if these are masked out by the interrupt mask register.
 */
#define XUARTPS_STATUS_RXTRIG       0x0001 /* RX trigger     */
#define XUARTPS_STATUS_RXEMPTY      0x0002 /* RX FIFO empty  */
#define XUARTPS_STATUS_TXEMPTY      0x0008 /* TX FIFO empty  */
#define XUARTPS_STATUS_TXFULL       0x0010 /* TX FIFO full   */
#define XUARTPS_STATUS_TXACTIVE     0x0800 /* TX active      */

/*
 * Control Register Bit Definitions
 */
#define XUARTPS_CTRL_RXRST          0x0001 /* RX logic reset */
#define XUARTPS_CTRL_TXRST          0x0002 /* TX logic reset */
#define XUARTPS_CTRL_RX_EN          0x0004 /* RX enabled     */
#define XUARTPS_CTRL_RX_DIS         0x0008 /* RX disabled    */
#define XUARTPS_CTRL_TX_EN          0x0010 /* TX enabled     */
#define XUARTPS_CTRL_TX_DIS         0x0020 /* TX disabled    */
#define XUARTPS_CTRL_RST_TO         0x0040 /* Restart Timeout Counter */
#define XUARTPS_CTRL_STOPBRK        0x0100 /* Stop TX break  */
#define XUARTPS_CTRL_STARTBRK       0x0080 /* Set TX break   */

/*
 * Mode Register:
 * The mode register (MR) defines the mode of transfer as well as the data
 * format. If this register is modified during transmission or reception,
 * data validity cannot be guaranteed.
 */
#define XUARTPS_MODE_CLKSEL         0x0001 /* Pre-scalar selection */
#define XUARTPS_MODE_CHMODE_NORM    0x0000 /* Normal mode          */
#define XUARTPS_MODE_CHMODE_L_LOOP  0x0200 /* Local loop back mode */

#define XUARTPS_MODE_STOPMODE_1_BIT 0x0000 /* 1 stop bit  */
#define XUARTPS_MODE_STOPMODE_2_BIT 0x0080 /* 2 stop bits */

#define XUARTPS_MODE_PARITY_EVEN    0x0000 /* Even  parity mode */
#define XUARTPS_MODE_PARITY_ODD     0x0008 /* Odd   parity mode */
#define XUARTPS_MODE_PARITY_SPACE   0x0010 /* Space parity mode */
#define XUARTPS_MODE_PARITY_MARK    0x0018 /* Mark  parity mode */
#define XUARTPS_MODE_PARITY_NONE    0x0020 /* No    parity mode */

#define XUARTPS_MODE_CHARLEN_6_BIT  0x0006 /* 6 bits data */
#define XUARTPS_MODE_CHARLEN_7_BIT  0x0004 /* 7 bits data */
#define XUARTPS_MODE_CHARLEN_8_BIT  0x0000 /* 8 bits data */

#endif /* SO3_ZYNQ_UART_H_ */
