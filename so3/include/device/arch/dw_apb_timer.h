/*
 * File      : dw_apb_timer.h
 *
 * Created by: Lucas Elisei & David Truan
 * Created on: 07.12.2017
 *
 * DesignWare APB timer
 */

#ifndef DW_APB_TIMER_H_
#define DW_APB_TIMER_H_

// Maximum value for the timer counter register.
#define MAX_COUNTER_VALUE	(uint32_t)(0xFFFFFFFF)

// Maximum value converted to nanoseconds.
#define MAX_TIME_NS			MICROSECS(MAX_COUNTER_VALUE)

#define TIMER_CTRL_ENABLE	(1 << 0)
#define TIMER_CTRL_FREE		(0 << 1)
#define TIMER_CTRL_PERIODIC	(1 << 1)
#define TIMER_CTRL_IE		(0 << 2)

typedef struct {
	/* 0x00: Load register */
	volatile uint32_t load;
	/* 0x04: Counter register */
	volatile uint32_t counter;
	/* 0x08: Control register */
	volatile uint32_t control;
	/* 0x0C: End-of-Interrupt register */
	volatile uint32_t eoi;
	/* 0x10: Interrupt status register */
	volatile uint32_t is;
} dw_apb_timer_t;

#endif /* DW_APB_TIMER_H_ */
