/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: xemacps.h
 *
 * @brief   Here goes specification of Zynq-7000 Gigabit Ethernet Controller.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-07
 */
#ifndef SO3_XEMACPS_H_
#define SO3_XEMACPS_H_

#include <types.h>

/* -------------------------------------------------------------------------- *
 * ------------------------------ Memory layout ----------------------------- *
 * -------------------------------------------------------------------------- */
/**
 * @brief  Structure declaring registers layout inside Zynq-7000 Gigabit Ethernet Controller.
 * @see    ug585-Zynq-7000-TRM.pdf, p1269-1346
 * @remark Columns in comments are:
 *             1. offset in Gigabit Ethernet Controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned, `(x)` means no reset
 */
struct xemacps_regs
{
	uint32_t net_ctrl;                     /** @brief 0x000       - Network Control                     [M]            */
	uint32_t net_cfg;                      /** @brief 0x004       - Network Configuration               [RW] (0x80000) */
	uint32_t net_status;                   /** @brief 0x008       - Network Status                      [RO]       (x) */

	uint32_t __reserved1;

	uint32_t dma_cfg;                      /** @brief 0x010       - DMA Configuration                   [M]  (0x20784) */
	uint32_t tx_status;                    /** @brief 0x014       - Transmit Status                     [M]  */
	uint32_t rx_buf_queue_addr;            /** @brief 0x018       - Receive  Buffer Queue Base Address  [M]  */
	uint32_t tx_buf_queue_addr;            /** @brief 0x01C       - Transmit Buffer Queue Base Address  [M]  */
	uint32_t rx_status;                    /** @brief 0x020       - Receive Status                      [M]  */
	uint32_t intr_status;                  /** @brief 0x024       - Interrupt Status                    [M]  */
	uint32_t intr_enable;                  /** @brief 0x028       - Interrupt Enable                    [WO] (x) */
	uint32_t intr_disable;                 /** @brief 0x02C       - Interrupt Disable                   [WO] (x) */
	uint32_t intr_mask_status;             /** @brief 0x030       - Interrupt Mask Status               [M]  (x) */
	uint32_t phy_maintain;                 /** @brief 0x034       - PHY Maintenance                     [RW] */
	uint32_t rx_pause_quantum;             /** @brief 0x038       - Received Pause Quantum              [RO] */
	uint32_t tx_pause_quantum;             /** @brief 0x03C       - Transmit Pause Quantum              [RW] (0xFFFF) */

	uint32_t __reserved2[16];

	uint32_t hash[2];                      /** @brief 0x080:0x084 - Hash Register        Bottom+Top     [RW - 31:0][RW - 63:32] */
	uint32_t specific_addr[4][2];          /** @brief 0x088:0x0A4 - Specific Address 1-4 Bottom+Top     [RW - 31:0][M  - 47:32] */
	uint32_t type_id_match[4];             /** @brief 0x0A8:0x0B4 - Type ID Match 1-4                   [M]  */
	uint32_t wake_on_lan;                  /** @brief 0x0B8       - Wake on LAN Register                [M]  */
	uint32_t ipg_stretch;                  /** @brief 0x0BC       - IPG stretch register                [M]  */
	uint32_t stacked_vlan;                 /** @brief 0x0C0       - Stacked VLAN Register               [M]  */
	uint32_t tx_pfc_pause;                 /** @brief 0x0C4       - Transmit PFC Pause Register         [M]  */
	uint32_t specific_mask1[2];            /** @brief 0x0C8:0x0CC - Specific Address Mask 1 Bottom+Top  [RW - 31:0][M  - 47:32] */

	uint32_t __reserved3[11];

	uint32_t module_id;                    /** @brief 0x0FC       - Module ID                           [RO] (0x20118) */
	uint32_t octets_tx[2];                 /** @brief 0x100:0x104 - Octets transmitted (in frames no error) [R0 - 31:0][RO - 47:32] */
	uint32_t frames_tx;                    /** @brief 0x108       - Frames Transmitted                  [RO] */
	uint32_t bcast_frames_tx;              /** @brief 0x10C       - Broadcast frames Tx                 [RO] */
	uint32_t mcast_frames_tx;              /** @brief 0x110       - Multicast frames Tx                 [RO] */
	uint32_t pause_frames_tx;              /** @brief 0x114       - Pause     frames Tx                 [RO] */
	uint32_t frames_tx[6];                 /** @brief 0x118:0x12C - Frames Tx,          64-byte length  [RO],
	                                                                Frames Tx,  65 to  127-byte length  [RO],
	                                                                Frames Tx, 128 to  255-byte length  [RO],
	                                                                Frames Tx, 256 to  511-byte length  [RO],
	                                                                Frames Tx, 512 to 1023-byte length  [RO],
	                                                                Frame  Tx,1024 to 1518-byte length  [RO] */

	uint32_t __reserved4;

	uint32_t tx_under_runs;                /** @brief 0x134       - Transmit under runs                 [RO] */
	uint32_t single_collision_frames;      /** @brief 0x138       - Single    Collision Frames          [RO] */
	uint32_t multiple_collision_frames;    /** @brief 0x13C       - Multiple  Collision Frames          [RO] */
	uint32_t excessive_collisions;         /** @brief 0x140       - Excessive Collisions                [RO] */
	uint32_t late_collisions;              /** @brief 0x144       - Late      Collisions                [RO] */
	uint32_t deferred_tx_frames;           /** @brief 0x148       - Deferred Transmission Frames        [RO] */
	uint32_t carrier_sense_err;            /** @brief 0x14C       - Carrier Sense Errors.               [RO] */
	uint32_t octets_rx[2];                 /** @brief 0x150:0x154 - Octets Received                     [R0 - 31:0][RO - 47:32] */
	uint32_t frames_rx;                    /** @brief 0x158       - Frames Received                     [RO] */
	uint32_t bcast_frames_rx;              /** @brief 0x15C       - Broadcast Frames Rx                 [RO] */
	uint32_t mcast_frames_rx;              /** @brief 0x160       - Multicast Frames Rx                 [RO] */
	uint32_t pause_frames_rx;              /** @brief 0x164       - Pause     Frames Rx                 [RO] */
	uint32_t frames_rx[6];                 /** @brief 0x168:0x17C - Frames Rx,          64-byte length  [RO],
	                                                                Frames Rx,  65 to  127-byte length  [RO],
	                                                                Frames Rx, 128 to  255-byte length  [RO],
	                                                                Frames Rx, 256 to  511-byte length  [RO],
	                                                                Frames Rx, 512 to 1023-byte length  [RO],
	                                                                Frame  Rx,1024 to 1518-byte length  [RO] */

	uint32_t __reserved5;

	uint32_t undersz_frames_rx;            /** @brief 0x184       - Undersize frames received           [RO] */
	uint32_t oversz_frames_rx;             /** @brief 0x188       - Oversize  frames received           [RO] */
	uint32_t jabbers_rx;                   /** @brief 0x18C       - Jabbers received                    [RO] */
	uint32_t frame_chk_seq_err;            /** @brief 0x190       - Frame check  sequence errors        [RO] */
	uint32_t len_field_frame_err;          /** @brief 0x194       - Length field frame    errors        [RO] */
	uint32_t rx_sym_err;                   /** @brief 0x198       - Receive      symbol   errors        [RO] */
	uint32_t align_err;                    /** @brief 0x19C       - Alignment             errors        [RO] */
	uint32_t rx_res_err;                   /** @brief 0x1A0       - Receive      resource errors        [RO] */
	uint32_t rx_overrun_err;               /** @brief 0x1A4       - Receive      overrun  errors        [RO] */
	uint32_t ip_h_chksum_err;              /** @brief 0x1A8       - IP header    checksum errors        [RO] */
	uint32_t tcp_chk_err;                  /** @brief 0x1AC       - TCP          checksum errors        [RO] */
	uint32_t udp_chk_err;                  /** @brief 0x1B0       - UDP          checksum error         [RO] */

	uint32_t __reserved6[8];

	uint32_t timer_sync_strobe_sec;        /** @brief 0x1C8       - 1588 timer sync strobe            seconds     [RW] */
	uint32_t timer_sync_strobe_nsec;       /** @brief 0x1CC       - 1588 timer sync strobe            nanoseconds [M]  */
	uint32_t timer_sec;                    /** @brief 0x1D0       - 1588 timer                        seconds     [RW] */
	uint32_t timer_nsec;                   /** @brief 0x1D4       - 1588 timer                        nanoseconds [M]  */
	uint32_t timer_adjust;                 /** @brief 0x1D8       - 1588 timer adjust                             [M]  */
	uint32_t timer_increment;              /** @brief 0x1DC       - 1588 timer increment                          [M]  */
	uint32_t ptp_event_frame_tx_sec;       /** @brief 0x1E0       - PTP event frame transmitted       seconds     [RO] */
	uint32_t ptp_event_frame_tx_nsec;      /** @brief 0x1E4       - PTP event frame transmitted       nanoseconds [RO] */
	uint32_t ptp_event_frame_rx_sec;       /** @brief 0x1E8       - PTP event frame received          seconds     [RO] */
	uint32_t ptp_event_frame_rx_nsec;      /** @brief 0x1EC       - PTP event frame received          nanoseconds [RO] */
	uint32_t ptp_peer_event_frame_tx_sec;  /** @brief 0x1F0       - PTP peer  event frame transmitted seconds     [RO] */
	uint32_t ptp_peer_event_frame_tx_nsec; /** @brief 0x1F4       - PTP peer  event frame transmitted nanoseconds [RO] */
	uint32_t ptp_peer_event_frame_rx_sec;  /** @brief 0x1F8       - PTP peer  event frame received    seconds     [RO] */
	uint32_t ptp_peer_event_frame_rx_nsec; /** @brief 0x1FC       - PTP peer  event frame received    nanoseconds [RO] */

	uint32_t __reserved7[33];

	uint32_t design_cfg[4];                /** @brief 0x284:0x290 - Design Configuration 2-5 [RO] (x,0,0,x) */
};

/* -------------------------------------------------------------------------- *
 * -------------------------------- Bitmasks -------------------------------- *
 * -------------------------------------------------------------------------- */

/* Configuration masks */
#define DIS_ALL_INT 0xFFFFFFFF
#define DIS_TX_RX   0x00000000

#endif /* SO3_XEMACPS_H_ */
