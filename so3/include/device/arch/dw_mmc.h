/*
 * dw_mmc.h
 *
 *  Created on: Jan 11; 2018
 *      Author: David Truan
 *
 *
 *   Remark: This is for the define part a copy paste from the u-boot file. It also
 *   		 declare the struct dw_mmc used to list the registers of the MMC controller
 */


#ifndef DW_MMC_H_
#define DW_MMC_H_

#include <device/mmc.h>

#define FIFO_DEPTH 0x400


/* Interrupt Mask register */
#define DWMCI_INTMSK_ALL	0xffffffff
#define DWMCI_INTMSK_RE		(1 << 1)
#define DWMCI_INTMSK_CDONE	(1 << 2)
#define DWMCI_INTMSK_DTO	(1 << 3)
#define DWMCI_INTMSK_TXDR	(1 << 4)
#define DWMCI_INTMSK_RXDR	(1 << 5)
#define DWMCI_INTMSK_DCRC	(1 << 7)
#define DWMCI_INTMSK_RTO	(1 << 8)
#define DWMCI_INTMSK_DRTO	(1 << 9)
#define DWMCI_INTMSK_HTO	(1 << 10)
#define DWMCI_INTMSK_FRUN	(1 << 11)
#define DWMCI_INTMSK_HLE	(1 << 12)
#define DWMCI_INTMSK_SBE	(1 << 13)
#define DWMCI_INTMSK_ACD	(1 << 14)
#define DWMCI_INTMSK_EBE	(1 << 15)

/* Raw interrupt Regsiter */
#define DWMCI_DATA_ERR	(DWMCI_INTMSK_EBE | DWMCI_INTMSK_SBE | DWMCI_INTMSK_HLE |\
			DWMCI_INTMSK_FRUN | DWMCI_INTMSK_EBE | DWMCI_INTMSK_DCRC)
#define DWMCI_DATA_TOUT	(DWMCI_INTMSK_HTO | DWMCI_INTMSK_DRTO)
/* CTRL register */
#define DWMCI_CTRL_RESET	(1 << 0)
#define DWMCI_CTRL_FIFO_RESET	(1 << 1)
#define DWMCI_CTRL_DMA_RESET	(1 << 2)
#define DWMCI_DMA_EN		(1 << 5)
#define DWMCI_CTRL_SEND_AS_CCSD	(1 << 10)
#define DWMCI_IDMAC_EN		(1 << 25)
#define DWMCI_RESET_ALL		(DWMCI_CTRL_RESET | DWMCI_CTRL_FIFO_RESET |\
				DWMCI_CTRL_DMA_RESET)

/* CMD register */
#define DWMCI_CMD_RESP_EXP	(1 << 6)
#define DWMCI_CMD_RESP_LENGTH	(1 << 7)
#define DWMCI_CMD_CHECK_CRC	(1 << 8)
#define DWMCI_CMD_DATA_EXP	(1 << 9)
#define DWMCI_CMD_RW		(1 << 10)
#define DWMCI_CMD_SEND_STOP	(1 << 12)
#define DWMCI_CMD_ABORT_STOP	(1 << 14)
#define DWMCI_CMD_PRV_DAT_WAIT	(1 << 13)
#define DWMCI_CMD_UPD_CLK	(1 << 21)
#define DWMCI_CMD_USE_HOLD_REG	(1 << 29)
#define DWMCI_CMD_START		(1 << 31)

/* CLKENA register */
#define DWMCI_CLKEN_ENABLE	(1 << 0)
#define DWMCI_CLKEN_LOW_PWR	(1 << 16)

/* Card-type registe */
#define DWMCI_CTYPE_1BIT	0
#define DWMCI_CTYPE_4BIT	(1 << 0)
#define DWMCI_CTYPE_8BIT	(1 << 16)

/* Status Register */
#define DWMCI_BUSY		(1 << 9)
#define DWMCI_FIFO_MASK		0x1fff
#define DWMCI_FIFO_SHIFT	17

/* FIFOTH Register */
#define MSIZE(x)		((x) << 28)
#define RX_WMARK(x)		((x) << 16)
#define TX_WMARK(x)		(x)
#define RX_WMARK_SHIFT		16
#define RX_WMARK_MASK		(0xfff << RX_WMARK_SHIFT)

#define DWMCI_IDMAC_OWN		(1 << 31)
#define DWMCI_IDMAC_CH		(1 << 4)
#define DWMCI_IDMAC_FS		(1 << 3)
#define DWMCI_IDMAC_LD		(1 << 2)

/*  Bus Mode Register */
#define DWMCI_BMOD_IDMAC_RESET	(1 << 0)
#define DWMCI_BMOD_IDMAC_FB	(1 << 1)
#define DWMCI_BMOD_IDMAC_EN	(1 << 7)

/* UHS register */
#define DWMCI_DDR_MODE	(1 << 16)

/* quirks */
#define DWMCI_QUIRK_DISABLE_SMU		(1 << 0)

// Cdetect register
#define DWMCI_CARD_DETECT (1 << 0)


typedef struct {
	/* 0x00: Control Register */
	volatile uint32_t ctrl;
	/* 0x04: Power Enable Register */
	volatile uint32_t pwren;
	/* 0x08: Clock Divider Register */
	volatile uint32_t clkdiv;
	/* 0x0C: SD Clock Source Register */
	volatile uint32_t clksrc;
	/* 0x10: Clock Enable Register */
	volatile uint32_t clkena;
	/* 0x14: Timeout Register */
	volatile uint32_t tmout;
	/* 0x18: Card Type Register */
	volatile uint32_t ctype;
	/* 0x1C: Block Size Register */
	volatile uint32_t blksiz;
	/* 0x20: Byte Count Register */
	volatile uint32_t bytcnt;
	/* 0x24: Interrupt Mask Register */
	volatile uint32_t intmask;
	/* 0x28: Command Argument Register */
	volatile uint32_t cmdarg;
	/* 0x2C: Command Register */
	volatile uint32_t cmd;
	/* 0x30: Response Register 0 */
	volatile uint32_t resp0;
	/* 0x34: Response Register 1 */
	volatile uint32_t resp1;
	/* 0x38: Response Register 2 */
	volatile uint32_t resp2;
	/* 0x3C: Response Register 3 */
	volatile uint32_t resp3;
	/* 0x40: Masked Interrupt Status Register */
	volatile uint32_t mintsts;
	/* 0x44: Raw Interrupt Status Register */
	volatile uint32_t rintsts;
	/* 0x48: Status Register */
	volatile uint32_t status;
	/* 0x4C: FIFO Threshold Watermark Register */
	volatile uint32_t fifioth;
	/* 0x50: Card Detect Register */
	volatile uint32_t cdetect;
	/* 0x54: Write Protect Register */
	volatile uint32_t wrtprt;
	/* 0x58: Reserved */
	volatile uint32_t reserved1;
	/* 0x5C: Transferred CIU Card Byte Count Register*/
	volatile uint32_t tcbcnt;
	/* 0x60: Transferred Host to BIU-FIFO Byte Count Register */
	volatile uint32_t tbbcnt;
	/* 0x64: Debounce Count Register */
	volatile uint32_t debnce;
	/* 0x68: User ID Register */
	volatile uint32_t usrid;
	/* 0x6C: Version ID Register */
	volatile uint32_t verid;
	/* 0x70: Hardware Configuration Register */
	volatile uint32_t hcon;
	/* 0x74: UHS-1 Register */
	volatile uint32_t uhs_reg;
	/* 0x78: Hardware Reset Register */
	volatile uint32_t rst_n;
	/* 0x7C: Reserved */
	volatile uint32_t reserved2;
	/* 0x80: Bus Mode Register */
	volatile uint32_t bmod;
	/* 0x84: Poll Demand Register */
	volatile uint32_t pldmnd;
	/* 0x88: Descriptor List Base Address Register */
	volatile uint32_t dbaddr;
	/* 0x8C: Internal DMAC Status Register */
	volatile uint32_t idsts;
	/* 0x90: Internal DMAC Interrupt Enable Register */
	volatile uint32_t idinten;
	/* 0x94: Current Host Descriptor Address Register */
	volatile uint32_t dscaddr;
	/* 0x98: Current Buffer Descriptor Address Register */
	volatile uint32_t bufaddr;
	/* 0x9C-0xFC: Reserved */
	volatile uint32_t reserved3[23];
	/* 0x100: Card Threshold Control Register */
	volatile uint32_t xardthrctl;
	/* 0x104: Back End Power Register */
	volatile uint32_t back_end_power;
	/* 0x108 - 1FC: Reserved */
	volatile uint32_t reserved4[62];
	/* 0x200: Data FIFO Access*/
	volatile uint32_t data;
} dw_mmc_t;

struct dwmci_idmac {
	u32 flags;
	u32 cnt;
	u32 addr;
	u32 next_addr;
} __aligned(ARCH_DMA_MINALIGN);


#endif /* DW_MMC_H_ */
