#ifndef BCM283X_MU_H
#define BCM283X_MU_H

/*
 * The mini UART is an "Auxiliary peripheral" which shares the same area of
 * other peripherals such as two master SPIs. All share the same interrupt line.
 */

/*
 * From BCM2835 ARM peripherals documentation.
 *
 * Mini UART register offsets.
 */
#define MU_IO_REG 0x00
#define MU_IER_REG 0x04
#define MU_IIR_REG 0x08
#define MU_LCR_REG 0x0C
#define MU_MCR_REG 0x10
#define MU_LSR_REG 0x14
#define MU_MSR_REG 0x18
#define MU_SCRATCH 0x1C
#define MU_CNTL_REG 0x20
#define MU_STAT_REG 0x24
#define MU_BAUD_REG 0x28

#define MU_IO_DATA (0xFF << 0)
#define MU_STAT_SP_AVAIL (1 << 0)
#define MU_STAT_RX_FIFO_FILL (0xF << 16)
#define MU_LSR_TX_EMPTY (1 << 5)
#define MU_LSR_DATA_READY (1 << 0)
// TODO : all other bit offsets

#endif
