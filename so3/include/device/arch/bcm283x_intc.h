/*
 * bcm283x_intc.h
 *
 * interrupt controller register declaration for the BCM283x SoC
 *
 *  Created on: Dec 11, 2017
 *      Author: camilo
 */

#ifndef INCLUDE_DEVICE_ARCH_BCM283X_INTC_H_
#define INCLUDE_DEVICE_ARCH_BCM283X_INTC_H_

#include <types.h>

#define INTC_BASE_PERIPHERALS_IRQ_MASK 0xff

typedef struct bcm283x_intc {
	uint32_t irq_basic_pending;
	uint32_t irq_pending_1;
	uint32_t irq_pending_2;
	uint32_t fiq_control;
	uint32_t enable_irqs_1;
	uint32_t enable_irqs_2;
	uint32_t enable_basic_irqs;
	uint32_t disable_irqs_1;
	uint32_t disable_irqs_2;
	uint32_t disable_basic_irqs;
} bcm283x_intc_t;


#endif /* INCLUDE_DEVICE_ARCH_BCM283X_INTC_H_ */
