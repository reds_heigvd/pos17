/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: arm_a9_private_timer.h
 *
 * @brief   Here goes specification of ARM Cortex-A9 Private Timers.
 * @author  Luca   Sivillica [CREATED BY]
 * @author  Julien Baeriswyl [MERGED  BY] <julien.baeriswyl@heig-vd.ch>
 * @version 1.0
 * @since   2017-12-07
 */
#ifndef SO3_ARM_A9_PRIVATE_TIMER_H_
#define SO3_ARM_A9_PRIVATE_TIMER_H_

#include <types.h>

/**
 * @brief  Structure declaring registers layout inside Private Timer controller.
 * @see    DDI0407G_cortex_a9_mpcore_r3p0_trm, p64
 * @remark Columns in comments are:
 *             1. offset in Private Timer controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned
 */
struct arm_a9_private_timer_regs
{
    uint32_t load;        /** @brief 0x00 - Load             [RW - 31:0] */
    uint32_t counter;     /** @brief 0x04 - Counter          [RW - 31:0] */
    uint32_t ctrl;        /** @brief 0x08 - Control          [RW - 15:8,2:0] */
    uint32_t intr_status; /** @brief 0x0C - Interrupt Status [RW - 0] */
};

#define ARM_A9_PRIV_TIMER_CTRL_ENABLE      0x1 /* to enable the timer */
#define ARM_A9_PRIV_TIMER_CTRL_AUTO_RELOAD 0x2 /* to configure the timer in auto reload */
#define ARM_A9_PRIV_TIMER_CTRL_IRQ_ENABLE  0x4 /* to enable the interrupt timer */
#define ARM_A9_PRIV_TIMER_IRQ_FLAG         0x1 /* to clear the interrupt flag */

#endif /* SO3_ARM_A9_PRIVATE_TIMER_H_ */
