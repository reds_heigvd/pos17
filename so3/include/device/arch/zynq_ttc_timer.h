/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: zynq_ttc_timer.h
 *
 * @brief   Here goes specification of Zynq-7000 Triple Timer Counter.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-07
 */
#ifndef SO3_ZYNQ_TTC_TIMER_H_
#define SO3_ZYNQ_TTC_TIMER_H_

#include <types.h>
#include <device/device.h>

/**
 * @brief  Structure declaring registers layout inside Zynq-7000 Triple Timer Counter controller.
 * @see    ug585-Zynq-7000-TRM.pdf, p1754-1774
 * @remark Columns in comments are:
 *             1. offset in TTC controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned
 */
struct zynq_ttc_timer_regs
{
	uint32_t clk_ctrl[3];    /** @brief 0x00:0x08 - Clock Control register                [ 7:0] */
	uint32_t op_mode_rst[3]; /** @brief 0x0C:0x14 - Operational mode and reset            [ 7:0] (0x21) */
	uint32_t counter_val[3]; /** @brief 0x18:0x20 - Current counter value                 [15:0] */
	uint32_t interval[3];    /** @brief 0x24:0x2C - Interval value                        [15:0] */
	uint32_t match[3][3];    /** @brief 0x30:0x50 - Match value                           [15:0] */
	uint32_t intrs[3];       /** @brief 0x54:0x5C - Interval, Match, Ovrf and Event intrs [ 5:0] */
	uint32_t intr_enable[3]; /** @brief 0x60:0x68 - ANDed with corresponding Interrupt    [ 5:0] */
	uint32_t event_ctrl[3];  /** @brief 0x6C:0x74 - Enable, pulse and overflow            [ 2:0] */
	uint32_t events[3];      /** @brief 0x78:0x80 - `pclk` cycle count for event          [15:0] */
};

#define ZYNQ_TTC_CNT_CTRL_DISABLE_MASK 0x1
#define ZYNQ_TTC_CLK_CTRL_CSRC_MASK    (1 << 5) /* clock source */
#define ZYNQ_TTC_CLK_CTRL_PSV_MASK     0x1e
#define ZYNQ_TTC_CLK_CTRL_PSV_SHIFT    0x1

#endif /* SO3_ZYNQ_TTC_TIMER_H_ */
