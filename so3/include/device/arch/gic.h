/*
 * FILE: gic.h
 *
 * @brief   Here goes specification of GICv2 interface for GIC-400.
 * @author  Julien Baeriswyl [MODIFIED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-10-26
 */
#ifndef SO3_GIC_H_
#define SO3_GIC_H_

/* References:
 * [1] Allwinner A20 User Manual: Section 1.11 GIC
 * [2] Migrating a software application from ARMv5 to ARMv7-A/R Application Note 425
 *     http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dai0425/ch03s06s03.html
 * [3] GIC-400 Generic Interrupt Controller Technical Reference Manual
 *     http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0471b/index.html
 * [4] ARM Generic Interrupt Controller Architecture Specification v2.0
 *     http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ihi0048b/index.html
 */

#include <types.h>
#include <device/device.h>

/* Total number of IRQ sources handled by the controller
 *   0- 15 are SGI interrupts: SGI = Software Generated Interrupts
 *  16- 31 are PPI interrupts: PPI = Private Peripheral Interrupts
 *  32-122 are SPI interrupts: SPI = Shared Peripheral Interrupts
 *  122-127 are unused but declared to fit in GIC multiple of 32 bits structures
 */

#define NR_IRQS          128
#define NR_SGI            16

/* -------------------------------------------------------------------------- *
 * ----------------------------- Memory layouts ----------------------------- *
 * -------------------------------------------------------------------------- */
/**
 * @brief Registers layout of GIC (v1, v2) distributor.
 */
struct gic_cpu_regs
{
    /* CPU Interface Registers: [3] Table 3.6. CPU interface register summary */
    uint32_t ctlr;        /* 0x0000 */
    uint32_t pmr;         /* 0x0004 */
    uint32_t bpr;         /* 0x0008 */
    uint32_t iar;         /* 0x000c */
    uint32_t eoir;        /* 0x0010 */
    uint32_t rpr;         /* 0x0014 */
    uint32_t hppir;       /* 0x0018 */
    uint32_t abpr;        /* 0x001c */
    uint32_t aiar;        /* 0x0020 */
    uint32_t aeoir;       /* 0x0024 */
    uint32_t ahppir;      /* 0x0028 */

    uint32_t _res3[41];   /* 0x002c-0x00cc */

    uint32_t apr0;        /* 0x00d0 */
    uint32_t _res4[3];    /* 0x00d4-0x00dc */
    uint32_t nsapr0;      /* 0x00e0 */
    uint32_t _res5[6];    /* 0x00e4-0x00f8 */
    uint32_t iidr;        /* 0x00fc */
};

/**
 * @brief Registers layout of GIC (v1, v2) CPU interface.
 */
struct gic_dist_regs
{
    /* Distributor Registers: [3] Table 3.2. Distributor register summary */
    uint32_t ctlr;            /* 0x0000 */
    uint32_t typer;           /* 0x0004 */
    uint32_t iidr;            /* 0x0008 */
    uint32_t _res0[29];       /* 0x000c-0x001c: reserved 5 */
                              /* 0x0020-0x003c: implementation defined 8 */
                              /* 0x0040-0x007c: reserved 16 */
    uint32_t igroupr[32];     /* 0x0080 1024 interrupts with 1 bit/interrupt */
    uint32_t isenabler[32];   /* 0x0100 */
    uint32_t icenabler[32];   /* 0x0180 */
    uint32_t ispendr[32];     /* 0x0200 */
    uint32_t icpendr[32];     /* 0x0280 */
    uint32_t isactiver[32];   /* 0x0300 */
    uint32_t icactiver[32];   /* 0x0380 */

    uint32_t ipriorityr[256]; /* 0x0400 1024 interrupts with 8 bits/interrupt */
    uint32_t itargetsr[256];  /* 0x0800 1024 interrupts with 8 bits/interrupt */
    uint32_t icfgr[64];       /* 0x0c00 1024 interrupts with 2 bits/interrupt */
    uint32_t ppisr;           /* 0x0d00 */
    uint32_t spisr[63];       /* 0x0d04-0x0dfc */
    uint32_t nsacrn[64];      /* 0x0e00 (optional) */
    uint32_t sgir;            /* 0x0f00 */
    uint32_t _res1[3];        /* 0x0f04-0x0f0c */
    uint32_t cpendsgirn[4];   /* 0x0f10-0x0f1c */
    uint32_t spendsgirn[4];   /* 0x0f20-0x0f2c */
    uint32_t _res2[40];       /* 0x0f30-0x0fcc */
    uint32_t pidr4;           /* 0x0fd0 */
    uint32_t pidr5;           /* 0x0fd4 */
    uint32_t pidr6;           /* 0x0fd8 */
    uint32_t pidr7;           /* 0x0fdc */
    uint32_t pidr0;           /* 0x0fe0 */
    uint32_t pidr1;           /* 0x0fe4 */
    uint32_t pidr2;           /* 0x0fe8 */
    uint32_t pidr3;           /* 0x0fec */
    uint32_t cidr0;           /* 0x0ff0 */
    uint32_t cidr1;           /* 0x0ff4 */
    uint32_t cidr2;           /* 0x0ff8 */
    uint32_t cidr3;           /* 0x0ffc */
};

/* -------------------------------------------------------------------------- *
 * -------------------------------- Bitmasks -------------------------------- *
 * -------------------------------------------------------------------------- */

/* constants to handle GIC CPU interface */
#define GICC_ENABLE				0x1
#define GICC_INT_PRI_THRESHOLD	0xf0
#define GICC_IAR_MASK			0x1fff
#define GICC_IAR_INT_ID_MASK	0x3ff
#define GICC_INT_SPURIOUS		1023
#define GICC_DIS_BYPASS_MASK	0x1e0

/* constants to handle GIC distributor */
#define GICD_ENABLE				0x1
#define GICD_DISABLE			0x0
#define GICD_INT_ACTLOW_LVLTRIG	0x0
#define GICD_INT_EN_CLR_X32		0xffffffff
#define GICD_INT_EN_SET_SGI		0x0000ffff
#define GICD_INT_EN_CLR_PPI		0xffff0000
#define GICD_INT_DEF_PRI		0xa0
#define GICD_INT_DEF_PRI_X4		((GICD_INT_DEF_PRI << 24) | \
								( GICD_INT_DEF_PRI << 16) | \
								( GICD_INT_DEF_PRI <<  8) | \
								  GICD_INT_DEF_PRI)
#define GICD_CPU_ENABLE(x)		((1 << (x)) | (1 << ((x) + 8)) | (1 << ((x) + 16)))

#define INTC_CPU_CTRL_REG0		0x28
#define INTC_DISABLE			(1 << 4)

#endif /* SO3_GIC_H_ */
