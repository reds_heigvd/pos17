/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: zynq_slcr.h
 *
 * @brief   Here goes specification of Zynq-7000 System Level Control Registers.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-13
 */
#ifndef SO3_ZYNQ_SLCR_H_
#define SO3_ZYNQ_SLCR_H_

/**
 * @brief  Structure declaring registers layout inside Zynq-7000 System Level Control Registers.
 * @see    ug585-Zynq-7000-TRM.pdf, p1572-1711
 * @remark Columns in comments are:
 *             1. offset in System Level Control Registers
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned
 */
struct zynq_slcr_regs /* TODO: complete layout */
{
	uint32_t __reserved1[112];
	uint32_t clk_621_true;      /** @brief 0x1c4 - CPU Clock Ratio Mode select [RW - 0] (0x1) */
	uint32_t __reserved2[620];
};

#define ZYNQ_CLK_MODE(ref)      (((struct zynq_slcr_regs* volatile)(ref))->clk_621_true & 1)
#define ZYNQ_SLCR_TICKS(ref,ns) (ZYNQ_CLK_MODE(ref) ? (ns) / 5 : 100 * (ns) / 666)

#endif /* SO3_ZYNQ_SLCR_H_ */
