/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: arm_a9_global_timer.h
 *
 * @brief   Here goes specification of ARM Cortex-A9 Global Timer.
 * @author  Luca   Sivillica [CREATED BY]
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 1.0
 * @since   2017-12-07
 */
#ifndef SO3_ARM_A9_GLOBAL_TIMER_H_
#define SO3_ARM_A9_GLOBAL_TIMER_H_

#include <types.h>

/**
 * @brief  Structure declaring registers layout inside Global Timer controller.
 * @see    DDI0407G_cortex_a9_mpcore_r3p0_trm, p70
 * @remark Columns in comments are:
 *             1. offset in Global Timer Controller
 *             2. name of register
 *             3. [access type - usedbits,...]
 *             4. (reset) == 0, when not mentionned
 */
struct arm_a9_global_timer_regs
{
	uint32_t counter[2];     /** @brief 0x00:0x04 - Counter 64bits   [RW - 31:0+31:0] */
	uint32_t ctrl;           /** @brief 0x08      - Control          [RW - 15:8, 3:0] */
	uint32_t intr_status;    /** @brief 0x0C      - Interrupt Status [RW - 0]         */
	uint32_t cmp_value[2];   /** @brief 0x10:0x14 - Comparator Value [RW - 31:0+31:0] */
	uint32_t auto_increment; /** @brief 0x18      - Auto-Increment   [RW - 31:0]      */
};

#define ARM_A9_GT_CTRL_TIMER_ENABLE      0x1
#define ARM_A9_GT_CTRL_COMP_ENABLE       0x2
#define ARM_A9_GT_CTRL_IRQ_ENABLE        0x4
#define ARM_A9_GT_CTRL_AUTO_INC          0x8
#define ARM_A9_GT_INTR_STATUS_EVENT_FLAG 0x1

#endif /* ARM_A9_GLOBAL_TIMER_H_ */
