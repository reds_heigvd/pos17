/*
 * Contributors :
 *
 * December 2017 : Sydney Hauke
 */

#ifndef BCM283X_SP804_H
#define BCM283X_SP804_H

#include <types.h>

typedef struct bcm283x_sp804_timer {
	volatile uint32_t load;
	volatile uint32_t value;
	volatile uint32_t control;
	volatile uint32_t irq_clr_ack;
	volatile uint32_t raw_irq;
	volatile uint32_t masked_irq;
	volatile uint32_t reload;
	volatile uint32_t pre_divider;
	volatile uint32_t fr_counter; // free-running counter
} bcm283x_sp804_timer_t;

/* control register bits */
#define TIMER_ONESHOT_MODE (0) // Not implemented on BCM283x
#define TIMER_32B_MODE (1) // 32 or 16 bit mode
#define TIMER_PRESCALE (2)
#define TIMER_IE (5) // Interrupt enable
#define TIMER_PERIODIC_MODE (6) // Not implemented on BCM283x
#define TIMER_EN (7) // Timer enable
#define TIMER_DEBUG (8) // Halt timer or not if in debug mode
#define TIMER_FR_EN (9) // Enable free-running counter (separate)
#define TIMER_FR_PRESCALE (16) // Free-running prescaler

#define TIMER_PRESCALE_DIV1 (0)
#define TIMER_PRESCALE_DIV16 (1)
#define TIMER_PRESCALE_DIV256 (2)

#endif
