/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: net.h
 *
 * @brief   Network generic interface for PHY layer.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2018-01-11
 */
#ifndef SO3_NET_H_
#define SO3_NET_H_

#include <common.h>
#include <fdt.h>

typedef struct
{
	unsigned char mac_src[6],
	              mac_dst[6];
	unsigned char* data, pad;
} net_frame_t;

typedef enum
{
	NET_OK = 0,
	NET_ERR,
	NET_ERRCHK,
	NET_DOWN,
	NET_UP,
	NET_WAIT,
	NET_PARITY
} net_status_t;

typedef struct
{
	net_status_t (*link_state)(void);
	net_status_t (*send)(const net_frame_t * frame);
	net_status_t (*recv)(      net_frame_t * frame);
} net_ops_t;

#endif /* SO3_NET_H_ */
