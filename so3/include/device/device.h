
/*
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 *
 * Contributors:
 * - Daniel Rossier (2015-2017)
 *
 * This file contains some piece of code for various tests in SO3.
 */

#ifndef DEVICE_H
#define DEVICE_H

#include <device/fdt.h>

typedef enum {
	STATUS_UNKNOWN,		/**/
	STATUS_DISABLED,
	STATUS_INIT_PENDING,
	STATUS_INITIALIZED,
} dev_status_t;

struct dev {
	char compatible[MAX_COMPAT_SIZE];
	char nodename[MAX_NODE_SIZE];
	uint32_t base;
	uint32_t size;
	int irq;
	dev_status_t status;
	int offset_dts;
	struct dev *parent;
	void *fdt;
};
typedef struct dev dev_t;

/*
 * Get device information from a device tree
 * This function will be in charge of allocating dev_inf struct;
 */
int get_dev_info(const void *fdt, int offset, const char *compat, dev_t *info);
int fdt_get_int(dev_t *dev, const char *name);

void devices_init(void);


#endif /* DEVICE_H */
