/*
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 *
 * Contributors:
 * - Daniel Rossier (2015-2017)
 *
 * This file contains some piece of code for various tests in SO3.
 */

#ifndef DRIVER_H
#define DRIVER_H

#include <linker.h>

#include <device/fdt.h>
#include <device/device.h>

/* driver registering */

struct driver_entry {
	char	*compatible; /* compatible Name */

	int (*init)(dev_t *dev);
};
typedef struct driver_entry	driver_entry_t;

#define REGISTER_DRIVER(_name, _compatible, _init) ll_entry_declare(struct driver_entry, _name, driver) = { \
		.compatible = _compatible,	\
		.init = _init, \
}

void init_driver_from_dtb(void);

#endif /* DRIVER_H */
