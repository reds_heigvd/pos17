
/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 *
 */


#ifndef TIME_H
#define TIME_H

#include <types.h>

#include <device/device.h>


/* Number of seconds */
#define MSEC	1000ull
#define USEC	1000000ull
#define NSEC	1000000000ull

/* Number of nano-seconds */
#define SECONDS(_s)     ((uint64_t)((_s)  * NSEC))
#define MILLISECS(_ms)  ((uint64_t)((_ms) * USEC))
#define MICROSECS(_us)  ((uint64_t)((_us) * MSEC))

/* Time conversion units */

typedef unsigned int time_t;

struct timespec {
	long		tv_sec;			/* seconds */
	long		tv_nsec;		/* nanoseconds */
};

/* All timing information below must be express in nanoseconds. The underlying hardware is responsible
 * to perform the necessary alignment on 64 bits. */

/* Structure for a periodic timer */
typedef struct {
	dev_t *dev; /* Associated hardware */
	uint64_t period; /* Period in ns of the periodic timer */
	void (*start)(void);
} periodic_timer_t;

/* Structure for a oneshot timer */
typedef struct {
	dev_t *dev; /* Associated hardware */
	int (*set_deadline)(uint64_t deadline);
} oneshot_timer_t;

/* Structure of a clocksource timer */
typedef struct {
	dev_t *dev; /* Associated hardware */
	uint64_t (*read)(void);
	uint32_t nb_wrap;
} clocksource_timer_t;

extern periodic_timer_t periodic_timer;
extern oneshot_timer_t oneshot_timer;
extern clocksource_timer_t clocksource_timer;

void timer_init(void);
void clocksource_wrap_handler(void);

#endif /* TIME_H */
