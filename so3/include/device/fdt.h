
/*
 * fdt.h
 */

#ifndef FDT_H
#define FDT_H

#include <memory.h>

#include <libfdt.h> 	/* libfdt API */

#define MAX_COMPAT_SIZE	128
#define MAX_NODE_SIZE	128
#define MAX_SUBNODE	4

extern unsigned int _fdt_addr;


#endif /* FDT_H */
