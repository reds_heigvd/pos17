#ifndef __DIRENT_H_
#define __DIRENT_H_

/* Similar to Linux */
struct dirent_so3 {
	/* In our case the size of */
	char d_name[256];
	int d_type;
	int d_size;
};

#endif /* __DIRENT_H_ */
