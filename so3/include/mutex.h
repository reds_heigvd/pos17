
/*
 * mutex.h
 *
*/

#ifndef MUTEX_H
#define MUTEX_H

#include <list.h>
#include <asm/atomic.h>
#include <spinlock.h>
#include <thread.h>

 struct mutex {
   /* 1: unlocked, 0: locked, negative: locked, possible waiters */
   atomic_t                count;
   tcb_t		   *owner;
   spinlock_t              wait_lock;

   /* Allow to manage recursive locking */
   uint32_t 		   recursive_count;

   struct list_head        wait_list;
};
typedef struct mutex mutex_t;

 /*
  * This is the control structure for tasks blocked on mutex,
  * which resides on the blocked task's kernel stack.
  */
struct mutex_waiter {
  struct list_head list;
  tcb_t *tcb;
};

 /**
  * mutex_is_locked - is the mutex locked
  * @lock: the mutex to be queried
  *
  * Returns 1 if the mutex is locked, 0 if unlocked.
  */
 static inline int mutex_is_locked(struct mutex *lock)
 {
 	return atomic_read(&lock->count) != 1;
 }

void mutex_lock(struct mutex *lock);
void mutex_unlock(struct mutex *lock);
void mutex_init(struct mutex *lock);

#endif /* MUTEX_H */

