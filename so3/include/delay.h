/*
 * delay.h
 *
 *  Created on: Dec 9, 2014
 *      Author: redsuser
 */

#ifndef DELAY_H_
#define DELAY_H_

#include <common.h>

/**
 * Active wait based on the jiffy_usec
 */
void udelay(u64 us);

#endif /* DELAY_H_ */
