/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - June 2017: Daniel Rossier
 *
 */

#ifndef INCLUDE_LINKAGE_H_
#define INCLUDE_LINKAGE_H_


#define ASM_NL           ;
#define __ALIGN .align 0
#define __ALIGN_STR ".align 0"

#define ALIGN __ALIGN

#ifndef END
#define END(name) \
	.size name, .-name
#endif


#ifndef ENTRY
#define ENTRY(name) \
	.globl name ASM_NL \
	ALIGN ASM_NL \
	name:
#endif


#define _AC(X,Y)        X
#define UL(x) _AC(x, UL)


#endif /* INCLUDE_LINKAGE_H_ */
