
/*
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016, 2017 Sootech SA
 *
 * Contributors:
 * - Daniel Rossier (2015-2017)
 * - Xavier Ruppen (May-June 2017)
 *
 * This file contains some piece of code for various tests in SO3.
 */

#ifndef ELF_H
#define ELF_H

#include <elf-struct.h>

#define ELF_MAXSIZE (128 * SZ_1K)

/* ELF Binary image related information */
struct elf_img_info {
	Elf32_Ehdr *header;
	Elf32_Shdr *sections;
	Elf32_Phdr *segments; /* program header */
	char **section_names;
	void *file_buffer;
	uint32_t segment_page_count;
};
typedef struct elf_img_info elf_img_info_t;

uint8_t *elf_load_buffer(const char *filename);

void elf_load_sections(elf_img_info_t *elf_img_info);
void elf_load_segments(elf_img_info_t *elf_img_info);

void elf_clean_image(elf_img_info_t *elf_img_info);

#endif /* ELF_H */
