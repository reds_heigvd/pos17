/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2017: Daniel Rossier
 *
 */

#include <spinlock.h>
#include <list.h>
#include <schedule.h>

struct waitqueue {
	spinlock_t lock;
	struct list_head tcb_list;
};
typedef struct waitqueue waitqueue_t;


/*
 * Simple completion structure to perform synchronized operations between threads.
 */
struct completion {
	uint32_t count;
	waitqueue_t wait;
};
typedef struct completion completion_t;

void wait_for_completion(completion_t *completion);
void complete(completion_t *completion);
void init_completion(completion_t *completion);

