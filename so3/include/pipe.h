#ifndef PIPE_H
#define PIPE_H

#include <memory.h>
#include <mutex.h>
#include <sync.h>

#define PIPE_READER	0
#define PIPE_WRITER	0

#define PIPE_SIZE	PAGE_SIZE

struct pipe_desc {

	/* Mutex to access critical parts */
	struct mutex lock;

	/* Main buffer of this pipe */
	void *pipe_buf;

	/* The two global FD for each extremity */
	uint32_t gfd[2];

	/* Consumer */
	uint32_t pos_read;

	/* Producer */
	uint32_t pos_write;

	/* Waiting queue for managing empty pipe */
	completion_t wait_for_writer;

	/* Waiting queue for managing full pipe */
  	completion_t wait_for_reader;
};
typedef struct pipe_desc pipe_desc_t;

int do_pipe(int pipefd[2]);

#endif /* PIPE_H */
