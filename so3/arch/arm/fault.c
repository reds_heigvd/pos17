
#include <common.h>

void __prefetch_abort(uint32_t ifar, uint32_t ifsr) {
	printk("### prefetch abort exception ifar: %x ifsr: %x ###\n", ifar, ifsr);
	kernel_panic();
}

void __data_abort(uint32_t far, uint32_t fsr) {
	printk("### abort exception far: %x fsr: %x ###\n", far, fsr);
	kernel_panic();
}
