/*
 * File:       mmu.c
 *
 * Created by: Lucas Elisei & David Truan
 * Created on: 09.12.2017
 */

#if 0
#define DEBUG
#endif

#include <common.h>
#include <memory.h>
#include <string.h>
#include <types.h>

#include <asm/memory.h>
#include <asm/mmu.h>

#include <device/serial.h>

#include <generated/autoconf.h>

#include <mach/uart.h>

void configure_level1_table(uint32_t table_addr, uint32_t fdt_addr) {
	DBG("MMU - Configuring first-level table\n");

	uint32_t i;
	uint32_t *table = (uint32_t *)table_addr;

	// Reset table to zeros.
	for (i = 0; i < 4096; ++i) {
		table[i] = 0;
	}

	// We must allocate a one-to-one section on the beginning of the RAM so that
	// the code can still be executed after the MMU is on.
	table[CONFIG_RAM_BASE >> L1TABLE_ADDR_SHIFT] = (CONFIG_RAM_BASE & L1_SECT_MASK) |
			L1_SECT_READ | L1_SECT_WRITE | L1_SECT_TYPE;

	// Map 10 MB for kernel (should be enough).
	for (i = 0; i < 10; ++i) {
		table[(CONFIG_KERNEL_VIRT_BASE >> L1TABLE_ADDR_SHIFT) + i] = (((CONFIG_RAM_BASE >> L1TABLE_ADDR_SHIFT) + i) << L1TABLE_ADDR_SHIFT) |
				L1_SECT_READ | L1_SECT_WRITE | L1_SECT_TYPE;
	}

	// Map virtual space for FDT.
	table[fdt_addr >> L1TABLE_ADDR_SHIFT] = (fdt_addr & L1_SECT_MASK) |
			L1_SECT_READ | L1_SECT_WRITE | L1_SECT_TYPE;

	// Map virtual space for UART.
	table[UART_BASE >> L1TABLE_ADDR_SHIFT] = (UART_BASE & L1_SECT_MASK) |
			L1_SECT_READ | L1_SECT_WRITE | L1_SECT_TYPE;

	DBG("MMU - Configuration done\n");
}

void create_l2_table(void) {
	uint32_t i;
	uint32_t *table = (uint32_t *)(__sys_l1pgtable + (0x4000 / 4));

	for (i = 0; i < 256; ++i) {
		table[i] = 0;
	}
}

void create_mapping(uint32_t virt_addr, uint32_t phys_addr, uint32_t size) {
	uint32_t allocated, i;
	uint32_t *l1_table, *l2_table;

	l1_table = (uint32_t *)__sys_l1pgtable;

	allocated = 0;

	// If the size is 1 Mo or more, allocate sections.
	if (size >= SECTION_SIZE) {
		for (i = 0; allocated < size; allocated += SECTION_SIZE, ++i) {
			l1_table[(virt_addr >> L1TABLE_ADDR_SHIFT) + i] = (((phys_addr >> L1TABLE_ADDR_SHIFT) + i) << L1TABLE_ADDR_SHIFT) | L1_SECT_READ | L1_SECT_WRITE | L1_SECT_TYPE;
		}
	} else {
		uint32_t l2_index, l2_table_mask;

		// Get L2 table address.
		l2_table = (uint32_t *)(l1_table + (0x4000 / 4));

		// If the corresponding L1 PTE is empty, then create the L2 table.
		if (l1_table[virt_addr >> L1TABLE_ADDR_SHIFT] == 0) {
			create_l2_table();

			l2_table_mask = (uint32_t)l2_table & L1_L2PTE_ADDR_MASK;

			l1_table[virt_addr >> L1TABLE_ADDR_SHIFT] = l2_table_mask | L1_L2PTE_TYPE;
		}

		for (i = 0; allocated < size; allocated += PAGE_SIZE, ++i) {
			l2_index = ((virt_addr & L2_INDEX_MASK) >> L2_ADDR_SHIFT) + i;

			l2_table[l2_index] = (((phys_addr >> L2_ADDR_SHIFT) + i) << L2_ADDR_SHIFT) | L2_SMALL_PAGE_READ | L2_SMALL_PAGE_WRITE | L2_SMALL_PAGE_TYPE;
		}
	}
}
