/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * @author Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @since  2017-12-04
 */
 
#define	UART_BASE	0xe0001000
