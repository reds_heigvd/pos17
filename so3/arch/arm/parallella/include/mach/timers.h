/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: timers.h
 *
 * @brief   Here goes utils of Zynq-7000 Global Timer controller.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-04
 */

#include <types.h>
#include <device/arch/zynq_slcr.h>

#define ARM_A9_GT_BASE 0xf8f00200
#define ZYNQ_SLCR_BASE 0xf8000000

#define GT_TICKS(ns) ZYNQ_SLCR_TICKS(ZYNQ_SLCR_BASE, periodic_timer.period)

#define FREQ_3x2x_621  200000000 /* frequency used in 621 mode */
#define FREQ_3x2x_421  150000000 /* frequency used in 421 mode */

u32 getTimerFreq(void)
{
	/* if the CPU clock ratio mode select register is set to 1,
	   the 621 mode is used */
	if (ZYNQ_CLK_MODE(ZYNQ_SLCR_BASE)) {
		return FREQ_3x2x_621;
	}

	/* else the 421 mode is used  */
	return FREQ_3x2x_421;
}
