/*
 * fpga.h
 *
 *  Created on: Nov 30, 2017
 *      Author: David Truan
 */

#ifndef FPGA_H_
#define FPGA_H_

#include <types.h>

extern uint32_t *__fpga_axi_bridge;

/**
 * Reading functions for different sizes
 */
uint8_t lwfpga_read_byte(uint32_t offset);
uint16_t lwfpga_read_hword(uint32_t offset);
uint32_t lwfpga_read_word(uint32_t offset);
uint64_t lwfpga_read_dword(uint32_t offset);

/**
 * Writing functions for different sizes
 */
void lwfpga_write_byte(uint32_t offset, uint8_t data);
void lwfpga_write_hword(uint32_t offset, uint16_t data);
void lwfpga_write_word(uint32_t offset, uint32_t data);
void lwfpga_write_dword(uint32_t offset, uint64_t data);


/**
 * Set bits functions for different sizes
 */
void lwfpga_set_bits_byte(uint32_t offset, uint8_t bitmask);
void lwfpga_set_bits_hword(uint32_t offset, uint16_t bitmask);
void lwfpga_set_bits_word(uint32_t offset, uint32_t bitmask);
void lwfpga_set_bits_dword(uint32_t offset, uint64_t bitmask);


/**
 * Set bits functions for different sizes
 */
void lwfpga_clear_bits_byte(uint32_t offset, uint8_t bitmask);
void lwfpga_clear_bits_hword(uint32_t offset, uint16_t bitmask);
void lwfpga_clear_bits_word(uint32_t offset, uint32_t bitmask);
void lwfpga_clear_bits_dword(uint32_t offset, uint64_t bitmask);


#endif /* FPGA_H_ */
