/*
 * timer.h
 *
 *  Created on: Dec 7, 2017
 *      Author: reds
 */

#ifndef ARCH_ALTERA_DE1_TIMER_H_
#define ARCH_ALTERA_DE1_TIMER_H_

// Timers frequencies (First two at 100MHz, last two at 25MHz)
#define APB_TIMER01_FREQ	100000000ull
#define APB_TIMER23_FREQ	25000000ull

// Timers addresses
#define APB_TIMER0_BASE	0xFFC08000
#define APB_TIMER1_BASE	0xFFC09000
#define APB_TIMER2_BASE	0xFFD00000
#define APB_TIMER3_BASE	0xFFD01000

#endif /* ARCH_ALTERA_DE1_TIMER_H_ */
