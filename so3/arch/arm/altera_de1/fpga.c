/*
 * fpga.c
 *
 *  Created on: Dec 6, 2017
 *  Authors: David Truan
 */

#include <mach/fpga.h>

uint32_t *__fpga_axi_bridge;

/**
 * Reading functions for different sizes
 */
uint8_t lwfpga_read_byte(uint32_t offset) {
	return  *((uint8_t *)(__fpga_axi_bridge + offset));
}

uint16_t lwfpga_read_hword(uint32_t offset) {
	return  *((uint16_t *)(__fpga_axi_bridge + offset));
}

uint32_t lwfpga_read_word(uint32_t offset) {
	return  *((uint32_t *)(__fpga_axi_bridge + offset));
}

uint64_t lwfpga_read_dword(uint32_t offset) {
	return  *((uint64_t *)(__fpga_axi_bridge + offset));
}


/**
 * Writing functions for different sizes
 */
void lwfpga_write_byte(uint32_t offset, uint8_t data) {
	*((uint8_t *)(__fpga_axi_bridge + offset)) = data;
}

void lwfpga_write_hword(uint32_t offset, uint16_t data) {
	*((uint16_t *)(__fpga_axi_bridge + offset)) = data;
}

void lwfpga_write_word(uint32_t offset, uint32_t data) {
	*((uint32_t *)(__fpga_axi_bridge + offset)) = data;
}

void lwfpga_write_dword(uint32_t offset, uint64_t data) {
	*((uint64_t *)(__fpga_axi_bridge + offset)) = data;
}


/**
 * Set bits functions for different sizes
 */
void lwfpga_set_bits_byte(uint32_t offset, uint8_t bitmask) {
	*((uint8_t *)(__fpga_axi_bridge + offset)) |= bitmask;
}

void lwfpga_set_bits_hword(uint32_t offset, uint16_t bitmask) {
	*((uint16_t *)(__fpga_axi_bridge + offset)) |= bitmask;
}

void lwfpga_set_bits_word(uint32_t offset, uint32_t bitmask) {
	*((uint32_t *)(__fpga_axi_bridge + offset)) |= bitmask;
}

void lwfpga_set_bits_dword(uint32_t offset, uint64_t bitmask) {
	*((uint64_t *)(__fpga_axi_bridge + offset)) |= bitmask;
}


/**
 * Set bits functions for different sizes
 */
void lwfpga_clear_bits_byte(uint32_t offset, uint8_t bitmask) {
	*((uint8_t *)(__fpga_axi_bridge + offset)) &= ~bitmask;
}

void lwfpga_clear_bits_hword(uint32_t offset, uint16_t bitmask) {
	*((uint16_t *)(__fpga_axi_bridge + offset)) &= ~bitmask;
}

void lwfpga_clear_bits_word(uint32_t offset, uint32_t bitmask) {
	*((uint32_t *)(__fpga_axi_bridge + offset)) &= ~bitmask;
}

void lwfpga_clear_bits_dword(uint32_t offset, uint64_t bitmask) {
	*((uint64_t *)(__fpga_axi_bridge + offset)) &= ~bitmask;
}

