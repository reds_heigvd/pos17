/*
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April-June 2017: Xavier Ruppen
 * - June 2017: Alexandre Malki
 * - June 2017: Daniel Rossier
 *
 */

#ifndef ASM_ARM_SYSCALL_H
#define ASM_ARM_SYSCALL_H

#include <errno.h>
#include <types.h>


#define SYSINFO_DUMP_HEAP	0
#define SYSINFO_DUMP_SCHED	1
#define SYSINFO_TEST_MALLOC	2

/*
 * Syscall number definition
 */

#define SYSCALL_EXIT		1
#define SYSCALL_EXEC 		2
#define SYSCALL_WAITPID		3
#define SYSCALL_READ 		4
#define SYSCALL_WRITE 		5
#define SYSCALL_FORK 		7
#define SYSCALL_READDIR		9
#define SYSCALL_OPEN 		14
#define SYSCALL_CLOSE		15
#define SYSCALL_THREAD_CREATE	16
#define SYSCALL_THREAD_JOIN	17
#define SYSCALL_THREAD_EXIT	18
#define SYSCALL_PIPE 		19
#define SYSCALL_DUP		22
#define SYSCALL_DUP2		23

#define SYSCALL_STAT		34

#define SYSCALL_SYSINFO		99

int syscall_handle(uint32_t, uint32_t, uint32_t, uint32_t);

void set_errno(uint32_t val);

#endif /* ASM_ARM_SYSCALL_H */
