/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2017: Daniel Rossier
 *
 */

#ifndef CACHE_H
#define CACHE_H

#define L1_CACHE_SHIFT		6
#define L1_CACHE_BYTES		(1 << L1_CACHE_SHIFT)

#define __read_mostly __attribute__((__section__(".data.read_mostly")))

#define ____cacheline_aligned __attribute__((__aligned__(L1_CACHE_BYTES)))
#define __cacheline_aligned	____cacheline_aligned

#endif /* CACHE_H */
