/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2017: Daniel Rossier
 *
 */

#ifndef CACHEFLUSH_H
#define CACHEFLUSH_H

#include <asm/processor.h>

void cache_clean_flush(void);
void dcache_flush(uint32_t *pte);

#endif /* CACHEFLUSH_H */
