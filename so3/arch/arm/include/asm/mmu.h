/*
 * File: mmu.h
 *
 * Created by: Lucas Elisei
 * Created on: 09.12.2017
 */

#ifndef ASM_MMU_H_
#define ASM_MMU_H_

// Align L1 table on 16KB.
#define MMU_LEVEL1_ALIGN		0x4000

// Bits to shift to translate physical section to virtual section.
#define L1TABLE_ADDR_SHIFT		20

// Mask to easily get section number.
#define L1_SECT_MASK			0xFFF00000

// Level 1 section descriptors.
#define L1_SECT_WRITE			(1 << 10)
#define L1_SECT_READ			(1 << 11)
#define L1_SECT_TYPE			0x2

#define L1_L2PTE_TYPE			(1 << 0)

#define L1_L2PTE_ADDR_SHIFT		10
#define L1_L2PTE_ADDR_OFFSET	(1 << L1_L2PTE_ADDR_SHIFT)
#define L1_L2PTE_ADDR_MASK		(~(L1_L2PTE_ADDR_OFFSET - 1))

#define L2_INDEX_MASK			0x000FF000

#define L2_SMALL_PAGE_WRITE		(1 << 4)
#define L2_SMALL_PAGE_READ		(1 << 5)
#define L2_SMALL_PAGE_APX		(1 << 9)
#define L2_SMALL_PAGE_TYPE		(1 << 1)

#define L2_ADDR_SHIFT			12
#define L2_ADDR_MASK			(~((1 << L2_ADDR_SHIFT) - 1))

#ifndef __ASSEMBLY__

#ifdef CONFIG_MMU
void create_mapping(uint32_t virt_addr, uint32_t phys_addr, uint32_t size);
#endif // CONFIG_MMU

#endif // not __ASSEMBLY__

#endif /* ASM_MMU_H_ */
