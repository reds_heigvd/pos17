/*
 * Memory asm-dependent declarations
 * 
*/

#ifndef ASM_MEMORY_H
#define ASM_MEMORY_H

#include <generated/autoconf.h>

#define IO_MAPPING_BASE		0xe0000000

#ifndef __ASSEMBLY__
#include <types.h>

extern uint32_t *__sys_l1pgtable;

#endif /* __ASSEMBLY__ */

#endif /* ASM_MEMORY_H */
