/**
 * This file contains the body of the getTimerFreq which allows to get the
 * frequency of the timers.
 *
 * @author  Luca Sivillica
 * @date    13.12.2017
 *
 * @version 1.0
 */

#include <mach/timers.h>
#include <asm/io.h>

/**
 * This function returns the current frequency used by the timers.
 *
 * @return the current frequency used by the timers
 */
u32 getTimerFreq(void) {
	/* if the CPU clock ratio mode select register is set to 1,
	   the 621 mode is used */
	if (ioread32(SLCR_REG + SLCR_CLK_621_TRUE)) {
		return FREQ_3x2x_621;
	}

	/* else the 421 mode is used  */
	return FREQ_3x2x_421;
}
