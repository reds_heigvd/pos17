/**
 * This file contains the body of all functions which allow to initialize the
 * UART and handle the communication with it.
 *
 * @authors Luca Sivillica
 * @date    29.11.2017
 *
 * @version 1.0
 */

#define	UART_BASE 0xe0001000
