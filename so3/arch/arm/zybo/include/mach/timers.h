/**
 * This file contains a function and constants which allow to get the
 * frequency of the timers.
 *
 * @author  Luca Sivillica
 * @date    13.12.2017
 *
 * @version 1.0
 */

#ifndef TIMERS_H
#define TIMERS_H

#include <types.h>
#include <device/arch/zynq_slcr.h>

#define ARM_A9_GT_BASE 0xf8f00200
#define ZYNQ_SLCR_BASE 0xf8000000

#define GT_TICKS(ns) ZYNQ_SLCR_TICKS(ZYNQ_SLCR_BASE, periodic_timer.period)

#define SLCR_REG		  0xF8000000 /* base address of the system level control register */
#define SLCR_CLK_621_TRUE 0x000001C4 /* CPU clock ratio mode select */

#define FREQ_3x2x_621	  200000000 /* frequency used in 621 mode */
#define FREQ_3x2x_421	  150000000 /* frequency used in 421 mode */

/**
 * This function returns the current frequency used by the timers.
 *
 * @return the current frequency used by the timers
 */
u32 getTimerFreq(void);

#endif // TIMERS_H
