
/* setup.c
 *
 * Low-level ARM-specific setup
 */

#include <tinyprintf.h>
#include <memory.h>

#include <device/driver.h>
#include <device/irq.h>

#include <asm/mmu.h>
#include <asm/processor.h>
#include <asm/setup.h>


extern unsigned char __irq_stack_start[];

unsigned int processor_id;

/*
 * Only 3 32-bit fields are sufficient (see exception.S)
 */
struct stack {
	u32 irq[3];
	u32 abt[3];
	u32 und[3];
	u32 fiq[3];
} ____cacheline_aligned;

struct stack stacks;

/*
 * Setup exceptions stacks for all modes except SVC and USR
 */
void setup_exception_stacks(void) {
	struct stack *stk = &stacks;

	/* Need to set the CPU in the different modes and back to SVC at the end */
	__asm__ (
		"msr	cpsr_c, %1\n\t"
		"add	r14, %0, %2\n\t"
		"mov	sp, r14\n\t"
		"msr	cpsr_c, %3\n\t"
		"add	r14, %0, %4\n\t"
		"mov	sp, r14\n\t"
		"msr	cpsr_c, %5\n\t"
		"add	r14, %0, %6\n\t"
		"mov	sp, r14\n\t"
		"msr	cpsr_c, %7\n\t"
		"add	r14, %0, %8\n\t"
		"mov	sp, r14\n\t"
		"msr	cpsr_c, %9"
		    :
		    : "r" (stk),
		      "I" (PSR_F_BIT | PSR_I_BIT | IRQ_MODE), "I" (offsetof(struct stack, irq[0])),
		      "I" (PSR_F_BIT | PSR_I_BIT | ABT_MODE), "I" (offsetof(struct stack, abt[0])),
		      "I" (PSR_F_BIT | PSR_I_BIT | UND_MODE), "I" (offsetof(struct stack, und[0])),
		      "I" (PSR_F_BIT | PSR_I_BIT | FIQ_MODE), "I" (offsetof(struct stack, fiq[0])),
		      "I" (PSR_F_BIT | PSR_I_BIT | SVC_MODE)
		    : "r14");

}

/*
 * Low-level initialization before the main boostrap process.
 */
void setup_arch(void) {

	/* Clear BSS */
	clear_bss();

#ifdef CONFIG_MMU
	__sys_l1pgtable = (uint32_t *)(CONFIG_RAM_BASE + MMU_LEVEL1_ALIGN);
#endif

	/* Set up the different stacks according to CPU mode */
	setup_exception_stacks();

}
