/*
 * designware.c
 *
 *  Created on: Dec 9, 2017
 *  Author: David Truan
 */

#include <device/arch/designware.h>
#include <device/driver.h>
#include <types.h>
#include <common.h>



static int designware_init(dev_t *dev) {

}

REGISTER_DRIVER(designware, "altr,socfpga-stmmac", designware_init);
