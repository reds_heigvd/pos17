/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: xemacps.c
 *
 * @brief   Here goes driver of Zynq-7000 Gigabit Ethernet Controller.
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 0.1
 * @since   2017-12-12
 */
#include <types.h>

#include <device/driver.h>
#include <device/net.h>
#include <device/arch/xemacps.h>

/**
 * @brief  Reference to memory area of Gigabit Ethernet controller.
 * @remark Value is written when DTB is parsed.
 */
static struct xemacps_regs* volatile gem_ref;

static int xemacps_gem_init(void)
{
	/* disable all interrupts */
	gem_ref->intr_disable = DIS_ALL_INT;

	/* disable the receiver and the transmitter */
	gem_ref->net_ctrl     = 0;
	gem_ref->tx_status    = 0;
	gem_ref->rx_status    = 0;
	gem_ref->phy_maintain = 0;
}

/**
 * @brief Initialize Xilinx Zynq-7000 Gigabit Ethernet controller.
 * @param dev [IN] device tree node of Gigabit Ethernet controller.
 * @return 0
 */
static int xemacps_init(dev_t* dev)
{
    gem_ref = (struct xemacps_regs*) dev->base;



    return 0;
}

REGISTER_DRIVER(xemacps, "cdns,gem", xemacps_init);
