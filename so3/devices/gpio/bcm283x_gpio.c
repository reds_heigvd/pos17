/*
 * bcm283x_gpio.c
 *
 * Auteur : Sydney Hauke
 */

#include <device/driver.h>
#include <device/gpio.h>
#include <device/irq.h>

#include <device/arch/bcm283x_gpio.h>

static bcm283x_gpio_t *bcm283x_gpio_regs;

static irqreturn_t
bcm283x_gpio_isr(int irq)
{
	unsigned int gpio_bank = 0;
	for(; gpio_bank <= NR_GPIOS / 32; gpio_bank++) {
		uint32_t event_status = bcm283x_gpio_regs->gpeds[gpio_bank];

		unsigned int gpio_bit = 0;
		for(; gpio_bit < 32; gpio_bit++) {
			if(event_status & (1 << gpio_bit)) {
				/* Clear event on current gpio */
				bcm283x_gpio_regs->gpeds[gpio_bank] |= (1 << gpio_bit);

				/* Call callback function associated to the current gpio */
				unsigned int current_gpio = gpio_bank * 32 + gpio_bit;
				if(gpio_desc[current_gpio].action != NULL) {
					gpio_desc[current_gpio].action(current_gpio);
				}
			}
		}
	}

	return IRQ_HANDLED;
}

static int
is_valid(unsigned int gpio)
{
	return gpio < NB_GPIOS;
}

static void
bcm283x_gpio_config_dir(unsigned int gpio, gpio_dir_t dir)
{
	/* Don't treat incorrect gpio number */
	if(!is_valid(gpio)) return;

	uint32_t gpfsel = bcm283x_gpio_regs->gpfsel[gpio / 10];

	gpfsel &= ~(0x7 << FSEL_OFFSET(gpio)); // Clear alternate function field of gpio

	if(dir == INPUT) {
		gpfsel |= (FSEL_INPUT << FSEL_OFFSET(gpio));
	}
	else if(dir == OUTPUT) {
		gpfsel |= (FSEL_OUTPUT << FSEL_OFFSET(gpio));
	}

	bcm283x_gpio_regs->gpfsel[gpio / 10] = gpfsel;
}

static gpio_state_t
bcm283x_gpio_get(unsigned int gpio)
{
	if(!is_valid(gpio)) return LOW;

	uint32_t bank_state = bcm283x_gpio_regs->gplev[gpio / 32];
	gpio_state_t gpio_state = !!(bank_state & (1 << (gpio % 32)));

	return gpio_state;
}

static void
bcm283x_gpio_set(unsigned int gpio, gpio_state_t state)
{
	/* Don't treat incorrect gpio number */
	if(!is_valid(gpio)) return;

	if(state == LOW) {
		bcm283x_gpio_regs->gpclr[gpio / 32] = (1 << (gpio % 32));
	}
	else if(state == HIGH) {
		bcm283x_gpio_regs->gpset[gpio / 32] = (1 << (gpio % 32));
	}
}

static int
bcm283x_gpio_enable_int(unsigned int gpio, gpio_edge_dir_t edge)
{
	/* Don't treat incorrect gpio number */
	if(!is_valid(gpio)) return -1;

	if(edge == FALLING) {
		bcm283x_gpio_regs->gpfen[gpio / 32] = (1 << (gpio % 32));
	}
	else if(edge == RISING) {
		bcm283x_gpio_regs->gpren[gpio / 32] = (1 << (gpio % 32));
	}

	return 0;
}

static int
bcm283x_gpio_disable_int(unsigned int gpio)
{
	if(!is_valid(gpio)) return -1;

	bcm283x_gpio_regs->gpfen[gpio / 32] &= ~(1 << (gpio % 32));
	bcm283x_gpio_regs->gpren[gpio / 32] &= ~(1 << (gpio % 32));

	return 0;
}

static int
bcm283x_gpio_init(dev_t *dev)
{
	/* Initialize base address of bcm283x_gpio register set */
	bcm283x_gpio_regs = (bcm283x_gpio_t*)dev->base;

	/* Register gpio ops functions of our driver */
	gpio_ops.is_valid = is_valid;
	gpio_ops.config_dir = bcm283x_gpio_config_dir;
	gpio_ops.get = bcm283x_gpio_get;
	gpio_ops.set = bcm283x_gpio_set;
	gpio_ops.enable_int = bcm283x_gpio_enable_int;
	gpio_ops.disable_int = bcm283x_gpio_disable_int;

	irq_bind(dev->irq, bcm283x_gpio_isr, 0);
	irq_enable(dev->irq);
	irq_unmask(dev->irq);

	return 0;
}

REGISTER_DRIVER(bcm283x_gpio, "bcm,bcm283x_gpio", bcm283x_gpio_init);
