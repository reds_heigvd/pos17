/*
 * dw_mmc.c
 *
 *  Created on: Jan 11, 2018
 *      Author: David Truan
 *
 *  Description: Driver register for the Designware MMC controller and low level
 *  			 functions with binding to the mmc_ops.
 *  			 It is strongly inspired from the U-Boot file. It bypass some of
 *  			 the abstraction layer such as the mmc_host and the mmc struct which
 *  			 contain some informations about the controller and the current SD card
 *  			 plugged such as the blocksize, the size, etc. Therefore, we need some
 *  			 additional define to specify these values.
 */

#if 0
#define DEBUG
#endif

#include <device/driver.h>
#include <device/arch/dw_mmc.h>

#include <common.h>
#include <delay.h>

#include <types.h>
#include <errno.h>

#include <device/timer.h>

#include <bouncebuf.h>
#include <asm/cacheflush.h>


static dw_mmc_t *dw_mmc;

static int dw_wait_reset(uint32_t value) {
	unsigned long timeout = 1000;
	uint32_t ctrl;

	dw_mmc->ctrl = value;


	while (timeout--) {
		ctrl = dw_mmc->ctrl;
		if (!(ctrl & DWMCI_RESET_ALL))
			return 1;
	}
	return 0;
}

static void dw_set_idma_desc(struct dwmci_idmac *idmac,
		u32 desc0, u32 desc1, u32 desc2)
{
	struct dwmci_idmac *desc = idmac;

	desc->flags = desc0;
	desc->cnt = desc1;
	desc->addr = desc2;
	desc->next_addr = (ulong)desc + sizeof(struct dwmci_idmac);
}

// DMA transfers not handled for now
static int dw_prepare_data(struct mmc_data *data,
	       struct dwmci_idmac *cur_idmac,
	       void *bounce_buffer) {
	unsigned long ctrl;
	unsigned int i = 0, flags, cnt, blk_cnt;
	ulong data_start, data_end;


	blk_cnt = data->blocks;

	dw_wait_reset(DWMCI_CTRL_FIFO_RESET);


	data_start = (ulong)cur_idmac;
	dw_mmc->dbaddr = (uint32_t)cur_idmac;


	do {
		flags = DWMCI_IDMAC_OWN | DWMCI_IDMAC_CH ;
		flags |= (i == 0) ? DWMCI_IDMAC_FS : 0;
		if (blk_cnt <= 8) {
			flags |= DWMCI_IDMAC_LD;
			cnt = data->blocksize * blk_cnt;
		} else
			cnt = data->blocksize * 8;

		// TODO: replace 4096 by a define PAGE_SIZE
		dw_set_idma_desc(cur_idmac, flags, cnt,
					(ulong)bounce_buffer + (i * 4096));

		if (blk_cnt <= 8)
			break;
		blk_cnt -= 8;
		cur_idmac++;
		i++;
	} while(1);

	data_end = (ulong)cur_idmac;

	cache_clean_flush();
	//flush_dcache_range(data_start, data_end + ARCH_DMA_MINALIGN);


	ctrl = dw_mmc->ctrl;
	ctrl |= DWMCI_IDMAC_EN | DWMCI_DMA_EN;
	dw_mmc->ctrl = ctrl;

	ctrl = dw_mmc->bmod;
	ctrl |= DWMCI_BMOD_IDMAC_FB | DWMCI_BMOD_IDMAC_EN;
	dw_mmc->bmod = ctrl;

	dw_mmc->blksiz = data->blocksize;
	dw_mmc->bytcnt = data->blocksize * data->blocks;

}

static int dw_data_transfer(struct mmc_data *data)
{
	//DBG("%s: %s\n", "dw_mmc", __func__);
	int ret = 0;
	u32 timeout = 240000;
	u32 mask, size, i, len = 0;
	u32 *buf = NULL;
	uint64_t start = clocksource_timer.read();
	u32 fifo_depth = (((dw_mmc->fifioth & RX_WMARK_MASK) >>
			    RX_WMARK_SHIFT) + 1) * 2;

	size = data->blocksize * data->blocks / 4;

	if (data->flags == MMC_DATA_READ) {
		//DBG("READ\n");
		buf = (unsigned int *)data->dest;
	} else {
		//DBG("WRITE\n");
		buf = (unsigned int *)data->src;
	}

	for (;;) {
		mask = dw_mmc->rintsts;
		/* Error during data transfer. */
		if (mask & (DWMCI_DATA_ERR | DWMCI_DATA_TOUT)) {
			debug("%s: DATA ERROR!\n", __func__);
			ret = -EINVAL;
			break;
		}

		// WE DO NOT HANDLE THE FIFO MODE SO THE CODE BELLOW IS NOT IMPLEMENTED
		/*
		if (size) {
			len = 0;
			if (data->flags == MMC_DATA_READ) {
				if ((dw_mmc->rintsts & DWMCI_INTMSK_RXDR)) {
					len = dw_mmc->status;
					len = (len >> DWMCI_FIFO_SHIFT) &
						    DWMCI_FIFO_MASK;
					len = min(size, len);
					for (i = 0; i < len; i++) {
						*buf++ = dw_mmc->data;
					}
					dw_mmc->rintsts = DWMCI_INTMSK_RXDR;
				}
			} else {
				if ((dw_mmc->rintsts & DWMCI_INTMSK_TXDR)) {
					len = dw_mmc->status;
					len = fifo_depth - ((len >> DWMCI_FIFO_SHIFT) & DWMCI_FIFO_MASK);
					len = min(size, len);
					for (i = 0; i < len; i++) {
						dw_mmc->data = *buf++;
						//DBG("buf[%u] = 0x%X\n", i, *buf);
					}
					dw_mmc->rintsts = DWMCI_INTMSK_TXDR;
				}
			}
			size = size > len ? (size - len) : 0;
		}*/

		/* Data arrived correctly. */
		if (mask & DWMCI_INTMSK_DTO) {
			ret = 0;
			break;
		}

		/* Check for timeout. */
		if (clocksource_timer.read()  > start + timeout * 1000000) {
			DBG("%s: Timeout waiting for data!\n",
			      __func__);
			ret = -ETIMEDOUT;
			//ret = 0;
			break;
		}
	}

	dw_mmc->rintsts = mask;

	return ret;
}

static int dw_set_transfer_mode(struct mmc_data *data)
{
	unsigned long mode;

	mode = DWMCI_CMD_DATA_EXP;
	if (data->flags & MMC_DATA_WRITE)
		mode |= DWMCI_CMD_RW;

	return mode;
}

static int dw_mmc_send_cmd(struct mmc_cmd *cmd, struct mmc_data *data) {


	ALLOC_CACHE_ALIGN_BUFFER(struct dwmci_idmac, cur_idmac,
			 data ? DIV_ROUND_UP(data->blocks, 8) : 0);

	struct bounce_buffer bbstate;
	unsigned long timeout = 500000000;
	uint32_t flags = 0, i;
	int ret;
	uint32_t retry = 100000;
	uint32_t mask , ctrl;
	unsigned long start = clocksource_timer.read();


	// Wait for the controller to be available
	while (dw_mmc->status & DWMCI_BUSY) {
		if (clocksource_timer.read() == (start + timeout)) {
			DBG("%s: Timout on data busy\n", __func__);
			return -ETIMEDOUT;
		}
	}
	// Mask interrupts
	dw_mmc->rintsts = DWMCI_INTMSK_ALL;

	if (data) {

		// fifo mode not implemented
		/*dw_mmc->blksiz = data->blocksize;
		dw_mmc->bytcnt = data->blocks * data->blocksize;
		dw_wait_reset(DWMCI_CTRL_FIFO_RESET);*/

		if (data->flags == MMC_DATA_READ) {
			bounce_buffer_start(&bbstate, (void*)data->dest,
					data->blocksize *
					data->blocks, GEN_BB_WRITE);
		} else {
			bounce_buffer_start(&bbstate, (void*)data->src,
					data->blocksize *
					data->blocks, GEN_BB_READ);
		}
		dw_prepare_data(data, cur_idmac,
				   bbstate.bounce_buffer);
	}

	dw_mmc->cmdarg = cmd->cmdarg;

	if (data)
		flags = dw_set_transfer_mode(data);

	if ((cmd->resp_type & MMC_RSP_136) && (cmd->resp_type & MMC_RSP_BUSY))
		return -1;

	if (cmd->cmdidx == MMC_CMD_STOP_TRANSMISSION)
		flags |= DWMCI_CMD_ABORT_STOP;
	else
		flags |= DWMCI_CMD_PRV_DAT_WAIT;

	if (cmd->resp_type & MMC_RSP_PRESENT) {
		flags |= DWMCI_CMD_RESP_EXP;
		if (cmd->resp_type & MMC_RSP_136)
			flags |= DWMCI_CMD_RESP_LENGTH;
	}

	if (cmd->resp_type & MMC_RSP_CRC)
		flags |= DWMCI_CMD_CHECK_CRC;

	flags |= (cmd->cmdidx | DWMCI_CMD_START | DWMCI_CMD_USE_HOLD_REG);


	//DBG("Sending CMD%u\n", cmd->cmdidx);

	dw_mmc->cmd = flags;

	for (i = 0; i < retry; i++) {
		mask = dw_mmc->rintsts;
		if (mask & DWMCI_INTMSK_CDONE) {
			if (!data)
				dw_mmc->rintsts = mask;
			break;
		}
	}

	if (i == retry) {
		DBG("%s: Timeout.\n", __func__);
		return -ETIMEDOUT;
	}

	if (mask & DWMCI_INTMSK_RTO) {

		//DBG("%s: Response Timeout.\n", __func__);
		return -ETIMEDOUT;
	} else if (mask & DWMCI_INTMSK_RE) {
		DBG("%s: Response Error.\n", __func__);
		return -EIO;
	}

	if (cmd->resp_type & MMC_RSP_PRESENT) {
		if (cmd->resp_type & MMC_RSP_136) {
			cmd->response[0] = dw_mmc->resp3;
			cmd->response[1] = dw_mmc->resp2;
			cmd->response[2] = dw_mmc->resp1;
			cmd->response[3] = dw_mmc->resp0;
		} else {
			cmd->response[0] = dw_mmc->resp0;
		}
	}

	if (data) {
		ret = dw_data_transfer(data);
		ctrl = dw_mmc->ctrl;
		ctrl &= ~(DWMCI_DMA_EN);
		dw_mmc->ctrl = ctrl;
		bounce_buffer_stop(&bbstate);
	}

	udelay(100);


	return ret;
}

static int dw_mmc_card_present(void) {
	return dw_mmc->cdetect & DWMCI_CARD_DETECT == 0 ? 1 : 0;
}


static int dw_mmc_init(dev_t *dev) {
	DBG("Designware MMC (0x%08X) initialization\n", dev->base);

	// Retrive the base address of the controller
	dw_mmc = (dw_mmc_t *) dev->base;
	// Bind the send_cmd of the generic abstraction with our low level function
	mmc_ops.send_cmd = dw_mmc_send_cmd;
	mmc_ops.card_present = dw_mmc_card_present;

	return 0;
}

REGISTER_DRIVER(dw_mmc, "altr,socfpga-dw-mshc", dw_mmc_init);
