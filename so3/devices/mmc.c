/*
 * mmc.c
 *
 *  Created on: Jan 23, 2018
 *      Author: David Truan
 *
 *    	Description: Basic MMC sub-system which allow to read/write blocks.
 *    				 Strongly inspired from the U-Boot file.
 *
 *    	Remarks: This sub-system is minimal and can be greatly improved by adding
 *    			 some abstraction layers of mmc. It should contains all the infos about
 *    			 the mmc (blocks size, size, high capacity, etc..) and is doable by
 *    			 adding a struct containing all the infos and by parsing the CSD.
 *    			 I didn't do that because of the lack of time.
 */

#if 1
#define DEBUG 1
#endif

#include <device/mmc.h>
#include <stddef.h>
#include <errno.h>
#include <common.h>
#include <delay.h>

mmc_ops_t mmc_ops;

static int mmc_send_cmd(struct mmc_cmd *cmd, struct mmc_data *data) {
	return mmc_ops.send_cmd(cmd, data);
}

int mmc_card_present(void) {
	return mmc_ops.card_present();
}

int mmc_send_status(int timeout) {
	struct mmc_cmd cmd;
	int err, retries = 5;

	cmd.cmdidx = MMC_CMD_SEND_STATUS;
	cmd.resp_type = MMC_RSP_R1;

	while (1) {
		err = mmc_send_cmd(&cmd, NULL);
		if (!err) {
			if ((cmd.response[0] & MMC_STATUS_RDY_FOR_DATA) &&
			    (cmd.response[0] & MMC_STATUS_CURR_STATE) != MMC_STATE_PRG)
				break;
			else if (cmd.response[0] & MMC_STATUS_MASK) {
#if !defined(CONFIG_SPL_BUILD) || defined(CONFIG_SPL_LIBCOMMON_SUPPORT)
				DBG("Status Error: 0x%08X\n", cmd.response[0]);
#endif
				return -ECOMM;
			}
		} else if (--retries < 0)
			return err;

		if (timeout-- <= 0)
			break;

		udelay(1000);
	}

	if (timeout <= 0) {
#if !defined(CONFIG_SPL_BUILD) || defined(CONFIG_SPL_LIBCOMMON_SUPPORT)
		DBG("Timeout waiting card ready\n");
#endif
		return -ETIMEDOUT;
	}

	return 0;
}


int mmc_read_blocks(void *dst, unsigned long start_addr, unsigned long blkcnt) {
	struct mmc_cmd cmd;
	struct mmc_data data;

	if (blkcnt > 1)
		cmd.cmdidx = MMC_CMD_READ_MULTIPLE_BLOCK;
	else
		cmd.cmdidx = MMC_CMD_READ_SINGLE_BLOCK;

	// We do not check for the high capacity or not now because there is no way yet
	// to do that (refer to the remark at the top for more infos)
	//if (mmc->high_capacity)
		cmd.cmdarg = start_addr;
	/*else
		cmd.cmdarg = start_addr * 512;*/

	cmd.resp_type = MMC_RSP_R1;

	data.dest = dst;
	data.blocks = blkcnt;
	data.blocksize = BLOCK_SIZE;
	data.flags = MMC_DATA_READ;

	if (mmc_send_cmd(&cmd, &data))
		return 0;

	if (blkcnt > 1) {
		cmd.cmdidx = MMC_CMD_STOP_TRANSMISSION;
		cmd.cmdarg = 0;
		cmd.resp_type = MMC_RSP_R1b;
		if (mmc_send_cmd(&cmd, NULL)) {
#if !defined(CONFIG_SPL_BUILD) || defined(CONFIG_SPL_LIBCOMMON_SUPPORT)
		DBG("mmc fail to send stop cmd\n");
#endif
			return 0;
		}
	}
	return blkcnt;
}


unsigned long mmc_write_blocks(unsigned long start, unsigned long blkcnt, const void *src)
{
	struct mmc_cmd cmd;
	struct mmc_data data;
	int timeout = 1000;

	if (blkcnt == 0)
		return 0;
	else if (blkcnt == 1)
		cmd.cmdidx = MMC_CMD_WRITE_SINGLE_BLOCK;
	else
		cmd.cmdidx = MMC_CMD_WRITE_MULTIPLE_BLOCK;

	// We do not check for the high capacity for now but it is doable
	//if (mmc->high_capacity)
	cmd.cmdarg = start;
	//else
	//	cmd.cmdarg = start * mmc->write_bl_len;

	cmd.resp_type = MMC_RSP_R1;

	data.src = src;
	data.blocks = blkcnt;
	data.blocksize = BLOCK_SIZE;
	data.flags = MMC_DATA_WRITE;

	if (mmc_send_cmd(&cmd, &data)) {
		DBG("mmc write failed\n");
		return 0;
	}

	if (blkcnt > 1) {
		cmd.cmdidx = MMC_CMD_STOP_TRANSMISSION;
		cmd.cmdarg = 0;
		cmd.resp_type = MMC_RSP_R1b;
		if (mmc_send_cmd(&cmd, NULL)) {
			DBG("mmc fail to send stop cmd\n");
			return 0;
		}
	}

	if (mmc_send_status(timeout))
		return 0;

	return blkcnt;
}

