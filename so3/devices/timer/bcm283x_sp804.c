/*
 * bcm283x_sp804 timer driver.
 *
 * The bcm283x_sp804 device is used as a periodic timer
 *
 * Contributors :
 * December 2017 : Sydney Hauke
 */

#if 0
#define DEBUG
#endif

#include <device/arch/bcm283x_sp804.h>
#include <device/timer.h>
#include <device/irq.h>
#include <device/driver.h>
#include <softirq.h>
#include <schedule.h>

static bcm283x_sp804_timer_t *bcm283x_sp804_timer;
static dev_t bcm283x_sp804_dev;

#define BCM2835_GPIO_BASE 0x3f200000
#define GPSET0 	((volatile unsigned int*)(BCM2835_GPIO_BASE + 0x0000001cUL)) // Bank 0 set
#define GPCLR0 	((volatile unsigned int*)(BCM2835_GPIO_BASE + 0x00000028UL)) // Bank 0 clear
#define GPFSEL2 ((volatile unsigned int*)(BCM2835_GPIO_BASE + 0x00000008UL)) // function select register 2 for gpio26
#define GPIO26_MASK (0x1 << 26) // Bit position for gpio 26
#define FSEL2_OFFSET (18) // Bits 20-18 are used to set the function selection of gpio26

static irqreturn_t bcm283x_sp804_isr(int irq) {
	bcm283x_sp804_timer->irq_clr_ack = 1; // Clear irq

	raise_softirq(SCHEDULE_SOFTIRQ);
	jiffies++;

#ifdef DEBUG
	static int set = 0;

    if(!set) {
    	*GPSET0 = GPIO26_MASK;
    	set = 1;
    }
    else {
        *GPCLR0 = GPIO26_MASK;
        set = 0;
    }
#endif

	return IRQ_HANDLED;
}

static void bcm283x_sp804_periodic_start(void) {
	// TODO : check bcm283x_sp804 timer freq.
	bcm283x_sp804_timer->load = (periodic_timer.period / 1000); // timer clock supposed to be 1 Mhz
	bcm283x_sp804_timer->control |= (1 << TIMER_EN);
}

static int bcm283x_sp804_init(dev_t *dev) {
	DBG("bcm283x_sp804_init\n");

	memcpy(&bcm283x_sp804_dev, dev, sizeof(dev_t));

	irq_bind(dev->irq, bcm283x_sp804_isr, 0);
	irq_ops.irq_enable(dev->irq);
	irq_ops.irq_unmask(dev->irq);

	DBG("Configure periodic timer\n");

	periodic_timer.dev = &bcm283x_sp804_dev;
	periodic_timer.start = bcm283x_sp804_periodic_start;

	DBG("Configure device bcm283x_sp804\n");

	bcm283x_sp804_timer = (bcm283x_sp804_timer_t *)(bcm283x_sp804_dev.base);

	bcm283x_sp804_timer->control = 0;
	bcm283x_sp804_timer->control |= (1 << TIMER_32B_MODE);
	bcm283x_sp804_timer->control |= (TIMER_PRESCALE_DIV1 << TIMER_PRESCALE);
	bcm283x_sp804_timer->control |= (1 << TIMER_IE);

	// Temp
    *GPFSEL2 |= (0x1 << FSEL2_OFFSET); // Set gpio26 as output

	return 0;
}

REGISTER_DRIVER(bcm283x_sp804_timer, "arm,bcm283x_sp804", bcm283x_sp804_init);
