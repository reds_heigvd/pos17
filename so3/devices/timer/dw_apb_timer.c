/*
 * File: dw_apb_timer.c
 *
 * Created by: Lucas Elisei & David Truan
 * Created on: 07.12.2017
 */

#if 1
#define DEBUG
#endif

#include <common.h>
#include <schedule.h>
#include <softirq.h>
#include <types.h>

#include <asm/io.h>

#include <device/driver.h>
#include <device/irq.h>
#include <device/timer.h>
#include <device/arch/dw_apb_timer.h>

#include <mach/timer.h>

static dw_apb_timer_t *clocksource_dev;
static dw_apb_timer_t *periodic_dev;

static void dw_apb_periodic_start(void) {
	// Convert nanoseconds to (seconds * TIMER_FREQ).
	uint32_t period = (periodic_timer.period * APB_TIMER23_FREQ) / NSEC;

	DBG("periodic timer period set to: %u\n", period);

	// Load period into register.
	periodic_dev->load = period;

	// Enable timer.
	periodic_dev->control |= TIMER_CTRL_ENABLE;
}

static irqreturn_t dw_apb_periodic_isr(int irq) {
	// Clear interrupt.
	ioread32(&periodic_dev->eoi);

	++jiffies;

	raise_softirq(SCHEDULE_SOFTIRQ);
	raise_softirq(TIMER_SOFTIRQ);

	return IRQ_HANDLED;
}

static int dw_apb_periodic_init(dev_t *dev) {
	DBG("Periodic timer (0x%08X) initialization\n", dev->base);

	periodic_dev = (dw_apb_timer_t *)dev->base;

	// Make sure timer is disabled.
	periodic_dev->control &= ~TIMER_CTRL_ENABLE;

	// Set periodic mode and enable interrupt.
	periodic_dev->control = TIMER_CTRL_PERIODIC | TIMER_CTRL_IE;

	// Configure interrupt routine.
	irq_bind(dev->irq, dw_apb_periodic_isr, 0);
	irq_enable(dev->irq);

	// Configure generic periodic timer.
	periodic_timer.dev = dev;
	periodic_timer.start = dw_apb_periodic_start;

	return 0;
}

static uint64_t dw_apb_clocksource_read(void) {
	// Convert value to nanoseconds and add wrap value.
	return (SECONDS(~(clocksource_dev->counter)) +
			(uint64_t)(clocksource_timer.nb_wrap * (uint64_t)(MAX_COUNTER_VALUE + 1))) / APB_TIMER23_FREQ;
}

static irqreturn_t dw_apb_clocksource_isr(int irq) {
	// Clear interrupt.
	ioread32(&clocksource_dev->eoi);

	++(clocksource_timer.nb_wrap);

	return IRQ_HANDLED;
}

static int dw_apb_clocksource_init(dev_t *dev) {
	DBG("Clocksource timer (0x%08X) initialization\n", dev->base);

	clocksource_dev = (dw_apb_timer_t *)dev->base;

	// Make sure timer is disabled.
	clocksource_dev->control &= ~TIMER_CTRL_ENABLE;

	// Set free-running mode and enable interrupt.
	clocksource_dev->control = TIMER_CTRL_FREE | TIMER_CTRL_IE;

	// Load value to decrement from.
	clocksource_dev->load = MAX_COUNTER_VALUE;

	// Re-enable timer.
	clocksource_dev->control |= TIMER_CTRL_ENABLE;

	// Configure interrupt routine.
	irq_bind(dev->irq, dw_apb_clocksource_isr, 0);
	irq_enable(dev->irq);

	clocksource_timer.dev = dev;
	clocksource_timer.nb_wrap = 0;
	clocksource_timer.read = dw_apb_clocksource_read;

	return 0;
}

/*
 * Initialize DesignWare APB timers as periodic and clocksource timers.
 *
 * The Altera DE1 board offers 4 timers. We decided to use the last two since
 * they have a frequency of 25MHz (better for our uses) than the first two
 * timers which have a frequency of 100MHz. So that's why we look at the
 * addresses and not at the number of initializations already done.
 */
static int dw_apb_timer_init(dev_t *dev) {
	static char count = 0;
	int status;

	// Initialize periodic timer.
	if (count == 2) {
		status = dw_apb_periodic_init(dev);
	}
	// Initialize clocksource timer.
	else if (count == 3) {
		status = dw_apb_clocksource_init(dev);
	}
	else {
		DBG("Not initializing timer at 0x%08X\n", dev->base);
		status = 0;
	}

	++count;

	return status;
}

REGISTER_DRIVER(dw_apb_timer, "snps,dw-apb-timer", dw_apb_timer_init);
