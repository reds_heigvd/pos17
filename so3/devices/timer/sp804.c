/*
 * File:		sp804.c
 *
 * Created by: 	Lucas Elisei & David Truan
 * Created on: 	29.10.2017
 *
 * SP804 Dual-Timer driver.
 *
 * There are two SP804 modules. We organized as follow:
 *	- sp804_00 : clocksource (altera_de1)
 *	- sp804_01 : oneshot (rpi3 and pine64)
 *	- sp804_10 : periodic (lime and reptar)
 *	- sp804_11 : not used
 */

// Enable debug.
#if 0
#define DEBUG
#endif

// Enable tests.
#if 0
#define DO_TESTS
#endif

#include <asm/io.h>
#include <common.h>
#include <device/arch/sp804.h>
#include <device/driver.h>
#include <device/irq.h>
#include <device/timer.h>
#include <errno.h>
#include <schedule.h>
#include <softirq.h>
#include <types.h>

// Entry for each device so we can easily access registers.
static sp804_timer_t *clocksource_dev;
static sp804_timer_t *periodic_dev;
static sp804_timer_t *oneshot_dev;

/**
 * Interrupt routine for periodic timer.
 */
static irqreturn_t periodic_isr(int irq) {
	// Clear interrupt.
	periodic_dev->timerintclr = 0x1;

	++jiffies;

	raise_softirq(SCHEDULE_SOFTIRQ);
	raise_softirq(TIMER_SOFTIRQ);

	return IRQ_HANDLED;
}

/**
 * Interrupt routine for clocksource timer.
 */
static irqreturn_t clocksource_isr(int irq) {
	// Clear interrupt.
	clocksource_dev->timerintclr = 0x1;

	// Increment wrap.
	++(clocksource_timer.nb_wrap);
}

static void handle_oneshot() {
	oneshot_dev->timerintclr = 0x1; // Clear interrupt

#ifdef DEBUG
	printk("Oneshot fired an interrupt!\n");
#endif
}

/**
 * ISR for the SP804_01 timer
 * The device has 2 timers (0 and 1), but we only use one interrupt line (34)
 * This ISR will handle an interruption from the first timer (clock source)
 * then the interruption from the second timer (onshot).
 */
static irqreturn_t sp804_01_isr(int irq) {
	if(clocksource_dev->timermis == 0x1) {
		handle_clocksource();
	}

	if(oneshot_dev->timermis == 0x1) {
		handle_oneshot();
	}

	return IRQ_HANDLED;
}

/**
 * Interrupt routine for oneshot timer.
 */
static irqreturn_t oneshot_isr(int irq) {
	// Clear interrupt.
	oneshot_dev->timerintclr = 0x1;

	DBG("Oneshot fired and interrupt\n");

	return IRQ_HANDLED;
}

/**
 * Interrupt routine for the first SP804 module.
 */
static irqreturn_t sp804_timer01_isr(int irq) {
	// Check which device rose the interrupt by reading its MIS register.
	if (clocksource_dev->timermis) {
		return clocksource_isr(irq);
	} else if (oneshot_dev->timermis) {
		return oneshot_isr(irq);
	} else {
		return IRQ_NONE;
	}
}

static irqreturn_t sp804_timer23_isr(int irq) {
	// Check which device rose the interrupt by reading its MIS register.
	if (periodic_dev->timermis) {
		return periodic_isr(irq);
	} else {
		return IRQ_NONE;
	}
}

/**
 * Returns clocksource value in nanoseconds.
 */
static uint64_t sp804_read_clocksource_value(void) {
	// Convert value to nanoseconds and add wrap value.
	return MICROSECS(~clocksource_dev->timervalue) + (uint64_t)(clocksource_timer.nb_wrap * (MAX_TIME_NS_32 + 1));
}

/**
 * Starts periodic timer with a period of `periodic_timer.period`.
 */
static void sp804_periodic_start(void) {
	// Convert period to microseconds.
	uint32_t period_us = (periodic_timer.period / MSEC);

	// and load it into the timer registers.
	periodic_dev->timerload = period_us;
	periodic_dev->timerbgload = period_us;

	// Enable timer.
	periodic_dev->timercontrol |= TIMER_CTRL_ENABLE;
}

/**
 * Sets deadline for the oneshot timer.
 */
static int sp804_oneshot_set_deadline(uint64_t deadline) {
	// Convert nanoseconds to microseconds.
	uint32_t deadline_us = deadline / MSEC;

	// If deadline > 0xFFFFFFFF.
	if(deadline_us > (~(uint32_t)(0))) {
		return EINVAL;
	}

	// Triggers oneshot countdown.
	oneshot_dev->timerload = deadline_us;

	return 0;
}

/**
 * Inits oneshot timer on the first SP804 module.
 */
static int sp804_oneshot_init(dev_t *dev) {
	// Retrieve timer struct.
	oneshot_dev = (sp804_timer_t *)(dev->base + TIMER2_OFFSET);

	// Configure oneshot timer of so3.
	oneshot_timer.dev = dev;
	oneshot_timer.set_deadline = sp804_oneshot_set_deadline;

	// Make sure timer is disabled.
	// TIMER_CTRL_REG(dev, TIMER2_OFFSET) &= ~TIMER_CTRL_ENABLE;
	oneshot_dev->timercontrol &= ~TIMER_CTRL_ENABLE;

	// Configure control register.
	oneshot_dev->timercontrol = TIMER_CTRL_32BIT | TIMER_CTRL_DIV1 | TIMER_CTRL_ONESHOT;

	// Enable timer.
	oneshot_dev->timercontrol |= TIMER_CTRL_ENABLE;

	return 0;
}

/**
 * Inits clocksource timer on the first SP804 module.
 */
static int sp804_clocksource_init(dev_t *dev) {
	DBG("dev->base = 0x%08X\n", dev->base);

	// Retrieve timer struct.
	clocksource_dev = (sp804_timer_t *)(dev->base + TIMER1_OFFSET);

	// Configure clocksource.
	clocksource_timer.dev = dev;
	clocksource_timer.read = sp804_read_clocksource_value;
	clocksource_timer.nb_wrap = 0;

	// Make sure timer is disabled.
	clocksource_dev->timercontrol &= ~TIMER_CTRL_ENABLE;

	// Configure timer.
	clocksource_dev->timercontrol = TIMER_CTRL_32BIT | TIMER_CTRL_DIV1 | TIMER_CTRL_IE;
	clocksource_dev->timerload = MAX_32BITS_VALUE;

	// Enable timer.
	clocksource_dev->timercontrol |= TIMER_CTRL_ENABLE;

/*
 * Tests that 5 seconds for the clocksource are (more or less) 5 actual seconds.
 * It needs a human and a real-life clock to be verified.
 */
#ifdef DO_TESTS
	printk("CLOCKSOURCE TEST: Waiting 5 seconds...");
	while (clocksource_timer.read() < 5 * NSEC);
	printk(" done.\n");
#endif

	return 0;
}

/**
 * Inits the periodic timer on the second SP804 module.
 */
static int sp804_periodic_init(dev_t *dev) {
	// Retrieve timer struct.
	periodic_dev = (sp804_timer_t *)(dev->base + TIMER1_OFFSET);

	// Configure periodic timer of so3.
	periodic_timer.dev = dev;
	periodic_timer.start = sp804_periodic_start;

	// Make sure timer is disabled.
	periodic_dev->timercontrol &= ~TIMER_CTRL_ENABLE;

	// Configure control register.
	periodic_dev->timercontrol = TIMER_CTRL_32BIT | TIMER_CTRL_DIV1 | TIMER_CTRL_PERIODIC | TIMER_CTRL_IE;

	return 0;
}

/**
 * Initializes the SP804 modules. Called when a `arm,sp804` entry is parsed from
 * the device tree.
 */
static int sp804_init(dev_t *dev) {
	static char count = 0;

	// First dual-timer initialization.
	if (count == 0) {
		DBG("Clocksource and one-shot timers initialization...\n");

		sp804_clocksource_init(dev);
		sp804_oneshot_init(dev);

		// Bind and enable corresponding interrupt.
		irq_bind(dev->irq, sp804_timer01_isr, 0);
		irq_ops.irq_enable(dev->irq);
	}
	// Second dual-timer initialization.
	else if (count == 1) {
		DBG("Periodic timer initialization...\n");

		sp804_periodic_init(dev);

		// Bind and enable corresponding interrupt.
		irq_bind(dev->irq, sp804_timer23_isr, 0);
		irq_ops.irq_enable(dev->irq);
	}

	++count;

	return 0;
}

REGISTER_DRIVER(sp804_timer, "arm,sp804", sp804_init);
