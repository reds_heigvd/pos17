/**
 * This file contains the body of all functions which allow to handle the
 * periodic timer.
 * To do this we use in this case the CPU private timer.
 *
 * @author  Luca   Sivillica [CREATED BY]
 * @author  Julien Baeriswyl [MERGED  BY] <julien.baeriswyl@heig-vd.ch>
 * @date    13.12.2017
 * @version 1.0
 */

#include <common.h>
#include <schedule.h>
#include <softirq.h>

#include <device/driver.h>
#include <device/irq.h>
#include <device/timer.h>
#include <device/arch/arm_a9_private_timer.h>

#include <mach/timers.h>

#if 0
#define DEBUG 0
#endif

/**
 * @brief Private timer structure mapping its registers.
 */
static struct arm_a9_private_timer_regs* volatile timer_ref;

/**
 * @brief Timer frequency, we use it to calculate the load value of the timer.
 */
static u32 timer_freq;

/**
 * @brief  Interrupt routine service of the periodic timer.
 *         It launch a soft interrupt that call the scheduler.
 *         It used to regulate the scheduling.
 * @param  irq  the interrupt number which has been triggered
 * @return IRQ_HANDLED indicating that the interrupt has been handled
 */
static irqreturn_t periodic_timer_isr(int irq)
{
	DBG("ISR private timer!\n");

	/* clear the interrupt */
	timer_ref->intr_status = ARM_A9_PRIV_TIMER_IRQ_FLAG;

	++jiffies;

	/* to scheduling */
	raise_softirq(SCHEDULE_SOFTIRQ);

	return IRQ_HANDLED;
}

/**
 * @brief This function allows to start the periodic timer.
 */
static void arm_a9_private_timer_periodic_start(void)
{
	u32 timeTick = NSEC / timer_freq;                /* time of one tick of the timer */
	u32 counter  = periodic_timer.period / timeTick; /* counter value to load */

	DBG("Start private timer!\n");

	/* make sure that timer is disabled in order to load the counter value */
	timer_ref->ctrl &= ~ARM_A9_PRIV_TIMER_CTRL_ENABLE;
	timer_ref->load  = counter;

	/*
	 * Enable the timer and configure it to auto reload counter value
	 * and enable interrupt.
	 */
	timer_ref->ctrl = ARM_A9_PRIV_TIMER_CTRL_ENABLE
	                | ARM_A9_PRIV_TIMER_CTRL_AUTO_RELOAD
	                | ARM_A9_PRIV_TIMER_CTRL_IRQ_ENABLE;
}

/**
 * @brief  Initialization function of the private timer called
 *         during the parsing of the DTB.
 * @param  dev  device node parsed from the DTB
 * @return 0
 */
static int arm_a9_private_timer_init(dev_t *dev)
{
	DBG("Private timer init...\n");

	timer_ref = (struct arm_a9_private_timer_regs*) dev->base;

	/* configure generic periodic timer of so3 */
	periodic_timer = (periodic_timer_t) {
		.start = arm_a9_private_timer_periodic_start
	};

	/* by default the timer is disabled */
	timer_ref->ctrl &= ~ARM_A9_PRIV_TIMER_CTRL_ENABLE;

	/* Register the isr corresponding to the timer interrupt */
	irq_bind(dev->irq, periodic_timer_isr, 0);
	irq_enable(dev->irq);

	/*
	 * Get the timer frequency in order to calculate
	 * later the load value of the timer.
	 */
	timer_freq = getTimerFreq();

	return 0;
}

REGISTER_DRIVER(arm_a9_priv_timer, "arm,cortex-a9-twd-timer", arm_a9_private_timer_init);
