/*
 * BCM283x system timer driver
 *
 * The BCM283x system timer is used for the oneshot and clocksource timers.
 *
 * Contributors :
 * December 2017 : Sydney Hauke
 */

#if 0
#define DEBUG
#endif

#include <device/irq.h>
#include <device/driver.h>
#include <device/timer.h>
#include <softirq.h>

#include <device/arch/bcm283x_systimer.h>

static bcm283x_systimer_t *bcm283x_systimer;
static dev_t bcm283x_systimer_dev;

static irqreturn_t bcm283x_systimer1_isr(int irq) {
	//DBG("Entering systimer1 isr\n");
	irq_ops.irq_disable(irq + 1);

	bcm283x_systimer->cs |= (1 << M1); // Acknowledge timer 1 compare match

	raise_softirq(TIMER_SOFTIRQ);

	return IRQ_HANDLED;
}

static irqreturn_t bcm283x_systimer3_isr(int irq) {
	DBG("Entering systimer3 isr\n");

	clocksource_timer.nb_wrap++; // TODO : mutual exclusion
	bcm283x_systimer->cs |= (1 << M3); // Acknowledge timer 3 compare match

	return IRQ_HANDLED;
}

/*
 * NOTE : deadline above 2^32 - 1 unsupported !!!
 */
static int bcm283x_systimer_set_deadline(uint64_t deadline) {
	uint32_t deadline32 = (uint32_t)(deadline / 1000); // Systimer freq. at 1Mhz.
	uint32_t current_time = bcm283x_systimer->clo;

	/* If current_time + deadline overflows */
	if(((~((uint32_t)0)) - deadline32) < current_time) {
		bcm283x_systimer->c1 = deadline32 - (~((uint32_t)0) - current_time);
	}
	else {
		bcm283x_systimer->c1 = current_time + deadline32;
	}

	irq_ops.irq_enable(bcm283x_systimer_dev.irq + 1);

	return 0;
}

static uint64_t bcm283x_systimer_read_clocksource(void) {
	return (((uint64_t)bcm283x_systimer->clo) |
				 (((uint64_t)bcm283x_systimer->chi) << 32)) * 1000; // 1 Mhz clock
}

static int bcm283x_systimer_init(dev_t *dev) {
	memcpy(&bcm283x_systimer_dev, dev, sizeof(dev_t));

	/* Register systimer as oneshot capable and
	 * also as a clocksource capable device.
	 */
	oneshot_timer.dev = &bcm283x_systimer_dev;
	oneshot_timer.set_deadline = bcm283x_systimer_set_deadline;

	clocksource_timer.dev = &bcm283x_systimer_dev;
	clocksource_timer.read = bcm283x_systimer_read_clocksource;
	bcm283x_systimer->c3 = 0; // Use timer 3 as clocksource

	irq_bind(dev->irq + 1, bcm283x_systimer1_isr, 0); // bind timer 1 to irq base + 1
	irq_bind(dev->irq + 3, bcm283x_systimer3_isr, 0); // bind timer 3 to irq base + 3

	bcm283x_systimer = (bcm283x_systimer_t *)(bcm283x_systimer_dev.base);

	return 0;
}

REGISTER_DRIVER(bcm283x_systimer, "arm,bcm283x_systimer", bcm283x_systimer_init);
