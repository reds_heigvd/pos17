/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2017 REDS Institute, HEIG-VD
 *
 * FILE: arm_a9_global_timer.c
 *
 * @brief   Here goes driver of ARM Cortex-A9 Global Timer.
 *          This driver map and handle Global Timer as periodic timer of SO3.
 * @author  Luca   Sivillica [CREATED BY]
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 1.0
 * @since   2017-12-07
 */
#include <common.h>
#include <schedule.h>
#include <softirq.h>
#include <types.h>

#include <device/driver.h>
#include <device/irq.h>
#include <device/timer.h>
#include <device/arch/arm_a9_global_timer.h>

#include <mach/timers.h>

#if 0
	#define ARM_A9_GT_TEST_IRQ
	#define ARM_A9_GT_TEST_CLOCK_SRC
#endif

/**
 * @brief  Reference to memory area of Global Timer.
 * @remark Value is written twice:
 *          - Once at compile time.
 *          - Once when DTB is parsed.
 *         This technique allow to use UART very quickly, before drivers initialization.
 */
static struct arm_a9_global_timer_regs* volatile gt_ref = (struct arm_a9_global_timer_regs*) ARM_A9_GT_BASE;

/**
 * @brief  Routine called when private IRQ issues.
 * @remark It launch a soft interrupt that call the scheduler.
 * @param  irq [IN] number ID
 * @return status of handling (here, HANDLED in any case)
 */
static irqreturn_t periodic_timer_isr(int irq)
{
	DBG("ISR global timer!\n");

	/* Acknoledge interrupt at timer level */
	gt_ref->intr_status |= ARM_A9_GT_INTR_STATUS_EVENT_FLAG;

	jiffies++;

#ifdef ARM_A9_GT_TEST_IRQ
	printk("TIMER: irq = %d\n", irq);
#endif

	/*
	 * Launch soft IRQ to schedule tasks
	 */
	raise_softirq(SCHEDULE_SOFTIRQ);

	return IRQ_HANDLED;
}

/**
 * @brief  Read timer value.
 * @remark Timer value is 64-bit, but acquired with 2 accesses.
 *         It means acquisition has to ensure 2 part of counter comes from same original value.
 *
 * @return 64-bit timer value
 */
static uint64_t arm_a9_gt_timer_read(void)
{
	uint32_t lower, upper, old;

	/* Loop to ensure 2 parts of counter are related */
	upper = gt_ref->counter[1];
	do {
		old   = upper;
		/* 64-bit counter cannot be accessed at once */
		lower = gt_ref->counter[0];
		upper = gt_ref->counter[1];
	} while (upper != old);

	/* Pack counter */
	return ((uint64_t)upper) << 32 | lower;
}

/**
 * @brief Set counter register with given value.
 * @param value [IN] to write in counter.
 */
inline static void arm_a9_gt_timer_write(uint64_t value)
{
	gt_ref->counter[1] = value >> 32;
	gt_ref->counter[0] = value;
}

/**
 * @brief Set comparison value and auto-increment if periodic.
 */
static void arm_a9_gt_timer_setcomp(uint32_t delta, int periodic)
{
	uint64_t counter;
	
	/* Disable comparison to avoid IRQ raising */
	gt_ref->ctrl &= ~ARM_A9_GT_CTRL_COMP_ENABLE;

	/* Set comparison value */
	counter = arm_a9_gt_timer_read() + delta;
	gt_ref->cmp_value[1] = counter >> 32;
	gt_ref->cmp_value[0] = counter;

	if (periodic)
	{
		gt_ref->auto_increment = delta;
		gt_ref->ctrl |= ARM_A9_GT_CTRL_AUTO_INC;
	}

	/* Enable comparison */
	gt_ref->ctrl |= ARM_A9_GT_CTRL_COMP_ENABLE | ARM_A9_GT_CTRL_IRQ_ENABLE;
}

/**
 * @brief Set period and start timer.
 */
inline static void arm_a9_gt_timer_start(void)
{
	DBG("Start global timer!\n");

	gt_ref->ctrl &= ~ARM_A9_GT_CTRL_TIMER_ENABLE;

	arm_a9_gt_timer_setcomp(GT_TICKS(periodic_timer.period), 1); /* For test */

	gt_ref->ctrl |= ARM_A9_GT_CTRL_TIMER_ENABLE;

#ifdef ARM_A9_GT_TEST_CLOCK_SRC
	uint64_t ref = clocksource_timer.read() + (periodic_timer.period << 9);
	printk("CLOCK TEST: waiting 5 seconds...\n");
	while (clocksource_timer.read() < ref);
	printk("CLOCK TEST: done\n");
#endif
}

/**
 * @brief Stop running timer.
 */
inline static void arm_a9_gt_timer_stop(void)
{
	gt_ref->ctrl &= ~(ARM_A9_GT_CTRL_TIMER_ENABLE | ARM_A9_GT_CTRL_COMP_ENABLE);
}

/**
 * @brief Initialize ARM Cortex-A9 Global Timer driver at DTB parse time.
 * @param dev [IN] device tree node of GT (parsed from the DTB)
 * @return 0
 */
static int arm_a9_gt_timer_init(dev_t *dev)
{
	DBG("Global timer init...\n");

	/* Get base address of Timer memory area */
	gt_ref = (struct arm_a9_global_timer_regs*) dev->base;

	arm_a9_gt_timer_stop();

#ifdef ARM_A9_GT_TEST_CLOCK_SRC
	clocksource_timer = (clocksource_timer_t) {
		.read    = arm_a9_gt_timer_read,
		.nb_wrap = 0
	};
#endif

	/* Map timer with generic periodic timer interface */
	periodic_timer = (periodic_timer_t) {
		.start = arm_a9_gt_timer_start
	};

	/* Register IRQ routine, and enable corresponding IRQ */
	irq_bind(dev->irq, periodic_timer_isr, 0);
	irq_enable(dev->irq);

	return 0;
}

/*
 * Map driver initialization with device tree declaration
 * and export function pointer to specific init area
 */
REGISTER_DRIVER(arm_a9_gt_drv, "arm,cortex-a9-global-timer", arm_a9_gt_timer_init);
