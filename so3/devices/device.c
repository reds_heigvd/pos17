/*
 * Generic core driver functions
 *
 */

#if 1
#define DEBUG
#endif

#include <common.h>
#include <heap.h>
#include <memory.h>

#include <device/device.h>
#include <device/driver.h>
#include <device/serial.h>
#include <device/irq.h>
#include <device/timer.h>
#include <device/gpio.h>

/*
 * Read the content of a device tree and associate a generic device info structure to each
 * relevant entry.
 *
 * So far, the device tree can have only one level of subnode (meaning that the root can contain only
 * nodes at the same level. Managing further sub-node levels require to adapt kernel/fdt.c
 *
 */
void parse_dtb(void) {
	unsigned int count;
	driver_entry_t *driver_entries;
	dev_t *dev;
	int i;
	int offset, new_off;
	bool found;

	count = ll_entry_count(driver_entry_t, driver);

	driver_entries = ll_entry_start(driver_entry_t, driver);

	offset = 0;

	DBG("Now scanning the device tree to retrieve all devices...\n");

	dev = (dev_t *) malloc(sizeof(dev_t));
	ASSERT(dev != NULL);
	found = false;

	while ((new_off = get_dev_info((void *) _fdt_addr, offset, "*", dev)) != -1) {

		for (i = 0; i < count; i++) {

			if (strcmp(dev->compatible, driver_entries[i].compatible) == 0) {

				found = true;

				DBG("Found compatible: %s\n",driver_entries[i].compatible);
				DBG("    Compatible:   %s\n", dev->compatible);
				DBG("    Base address: 0x%08X\n", dev->base);
				DBG("    Size:         0x%08X\n", dev->size);
				DBG("    IRQ:          %d\n", dev->irq);

#ifdef CONFIG_MMU
				dev->base = io_map(dev->base, dev->size);
#endif // CONFIG_MMU

				driver_entries[i].init(dev);

				break;
			}
		}
		if (!found)
			free(dev);

		offset = new_off;

		dev = (dev_t *) malloc(sizeof(dev_t));
		ASSERT(dev != NULL);
		found = false;
	}

	/* We have always the last allocation which will not be used */
	free(dev);
}

/*
 * Main device initialization function.
 */
void devices_init(void) {

	/* Interrupt management subsystem initialization */
	irq_init();

	serial_init() ;

	timer_init();

	gpio_init();

	/* Parse the associated dtb to initialize all devices */
	parse_dtb();
}
