/**
 * This file contains the body of all functions which allow to initialize the
 * GIC and handle the interrupts.
 *
 * @authors Luca Sivillica & Gaëtan Othenin-Girard
 * @date    31.10.2017
 *
 * @version 1.0
 */

#if 0
#define DEBUG
#endif

#include <device/arch/arm_gic_v2.h>
#include <device/driver.h>
#include <device/irq.h>
#include <types.h>
#include <common.h>

static struct intc_regs *gic_data; // represents all registers of the GIC

/**
 * Configure the GIC distributor.
 */
static void gic_dist_config(void) {
	unsigned int i;

	/* set all SPI interrupts to be level triggered, active low */
	for (i = 32; i < NR_IRQS; i += 16) {
		*(gic_data->gicd_icfgr + i / 16) = GICD_INT_ACTLOW_LVLTRIG;
	}

	/* set priority on all SPI interrupts */
	for (i = 32; i < NR_IRQS; i += 4) {
		*(gic_data->gicd_ipriorityr + i / 4) = GICD_INT_DEF_PRI_X4;
	}

	/* disable all SPI interrupts. Leave the PPI and SGIs alone
	   as they are enabled by redistributor registers */
	for (i = 32; i < NR_IRQS; i += 32) {
		*(gic_data->gicd_icenabler + i / 32) = GICD_INT_EN_CLR_X32;
	}
}

/**
 * Initializes the GIC distributor.
 */
static void gic_dist_init(void) {
	unsigned int i;
	uint32_t cpuMask;

	DBG("Begin init GIC distributor...\n");

	/* first of all we disable the GIC distributor */
	gic_data->gicd_ctlr = GICD_DISABLE;

	/* prepare the CPU 0 mask to set it as target for all SPIs */
	cpuMask   = GICD_CPU_0_MASK;
	cpuMask  |= cpuMask << 8;
	cpuMask  |= cpuMask << 16;

	/* set all SPI interrupts to the target CPU 0 */
	for (i = 32; i < NR_IRQS; i += 4) {
		*(gic_data->gicd_itargetsr + i / 4) = cpuMask;
	}

	/* configure the distributor */
	gic_dist_config();

	/* finally we enable the GIC distributor */
	gic_data->gicd_ctlr = GICD_ENABLE;
}

/**
 * Configure the GIC CPU interface.
 */
static void gic_cpu_config(void) {
	unsigned int i;

	/* deal with the banked PPI and SGI interrupts, disable all
	   PPI interrupts, ensure all SGI interrupts are enabled */
	*(gic_data->gicd_icenabler) = GICD_INT_EN_CLR_PPI;
	*(gic_data->gicd_isenabler) = GICD_INT_EN_SET_SGI;

	/* set priority on PPI and SGI interrupts */
	for (i = 0; i < 32; i += 4) {
		*(gic_data->gicd_ipriorityr + i / 4) = GICD_INT_DEF_PRI_X4;
	}
}

/**
 * Enables the signaling of interrupts by the CPU
 * interface to the connected processor.
 */
static void gic_cpu_if_up(void) {
	uint32_t bypass = 0;

	/* preserve bypass disable bits to be written back later */
	bypass  = gic_data->gicc_ctlr;
	bypass &= GICC_DIS_BYPASS_MASK;

	gic_data->gicc_ctlr = bypass | GICC_ENABLE;
}

/**
 * Initializes the GIC CPU interface.
 */
static void gic_cpu_init(void) {
	DBG("Begin init GIC CPU interface...\n");

	gic_cpu_config();

	/* set the priority filter */
	gic_data->gicc_pmr = GICC_INT_PRI_THRESHOLD;

	gic_cpu_if_up();
}

/**
 * Allows to mask an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gic_irq_mask(unsigned int irq) {
	uint32_t maskIrqID = 1 << (irq % 32);

	*(gic_data->gicd_icenabler + irq / 32) = maskIrqID;
}

/**
 * Allows to unmask an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gic_irq_unmask(unsigned  int irq) {
	uint32_t maskIrqID = 1 << (irq % 32);

	*(gic_data->gicd_isenabler + irq / 32) = maskIrqID;
}

/**
 * Allows to enable an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gic_irq_enable(unsigned  int irq) {
	gic_irq_unmask(irq);
}

/**
 * Allows to disable an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gic_irq_disable(unsigned  int irq) {
	gic_irq_mask(irq);
}

/**
 * Allows to handle an interrupt.
 */
static void gic_irq_handle(void) {
	uint32_t irqstat;
	uint32_t irqnr;

	do {
		/* get the interrupt ID */
		irqstat   = gic_data->gicc_iar;
		irqnr     = irqstat & GICC_IAR_INT_ID_MASK;

		/* the interrupt is a PPI or a SPI */
		if (irqnr > 15 && irqnr < NR_IRQS) {
			irq_process(irqnr);

			gic_data->gicc_eoir = irqnr;

		/* the interrupt is a SGI */
		} else if (irqnr < 16) {
			gic_data->gicc_eoir = irqstat;
		}

	} while (irqnr < NR_IRQS);
}

/**
 * Initializes the GIC.
 *
 * @param dev the pointer to the structure representing the GIC device
 */
static int gic_init(dev_t *dev) {
	DBG("Begin init GIC...\n");

	gic_data = (struct intc_regs *) dev->base;

	/* make the link between the GIC and the structure that allows
	   to handle the interrupts */
	irq_ops.irq_enable  = gic_irq_enable;
	irq_ops.irq_disable = gic_irq_disable;
	irq_ops.irq_mask    = gic_irq_mask;
	irq_ops.irq_unmask  = gic_irq_unmask;
	irq_ops.irq_handle  = gic_irq_handle;

	gic_dist_init();

	gic_cpu_init();

	DBG("End init GIC...\n");

	return 0;
}

REGISTER_DRIVER(gic_400, "arm-cortex-a15-gic", gic_init);
