/**
 * This file contains the body of all functions which allow to initialize the
 * GIC and handle the interrupts.
 *
 * @author  David Truan
 * @date    29.11.2017
 *
 * @version 1.0
 */

#include <common.h>
#include <types.h>

#include <device/arch/arm_gic_v1.h>
#include <device/driver.h>
#include <device/irq.h>


static gicc_t *gicc_dev; // represents all registers of the GIC CPU interface
static gicd_t *gicd_dev; // represents all registers of the GIC distributor

/**
 * Configure the GIC distributor.
 */
static void gicd_config(void) {
	unsigned int i;

	/* set all SPI interrupts to be level triggered, active low */
	for (i = 32; i < NB_IRQS; i += 16) {
		*(gicd_dev->icfgr + (i / 16)) = GICD_INT_ACTLOW_LVLTRIG;
	}

	/* set priority on all SPI interrupts */
	for (i = 32; i < NB_IRQS; i += 4) {
		*(gicd_dev->ipriorityr + (i / 4)) = GICD_INT_DEF_PRI_X4;
	}

	/* disable all SPI interrupts. Leave the PPI and SGIs alone
	   as they are enabled by redistributor registers */
	for (i = 32; i < NB_IRQS; i += 32) {
		*(gicd_dev->icenabler + (i / 32)) = GICD_INT_EN_CLR_X32;
	}
}


/**
 * Configure the GIC CPU interface.
 */
static void gicc_config(void) {
	unsigned int i;

	// Disable PPI and enable SGI.
	*(gicd_dev->icenabler) = GICD_INT_EN_CLR_PPI;
	*(gicd_dev->isenabler) = GICD_INT_EN_SET_SGI;

	// Set priority on PPI and SGI interrupts.
	for (i = 0; i < 32; i += 4) {
		*(gicd_dev->ipriorityr + (i / 4)) = GICD_INT_DEF_PRI_X4;
	}
}

/**
 * Allows to mask an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gicc_irq_mask(unsigned int irq) {
	uint32_t maskIrqID = 1 << (irq % 32);

	*(gicd_dev->icenabler + (irq / 32)) |= maskIrqID;
}

/**
 * Allows to unmask an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gicc_irq_unmask(unsigned  int irq) {
	uint32_t maskIrqID = 1 << (irq % 32);

	*(gicd_dev->isenabler + (irq / 32)) |= maskIrqID;
}

/**
 * Allows to enable an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gicc_irq_enable(unsigned  int irq) {
	gicc_irq_unmask(irq);
}

/**
 * Allows to disable an interrupt.
 *
 * @param irq the interrupt ID
 */
static void gicc_irq_disable(unsigned  int irq) {
	gicc_irq_mask(irq);
}

/**
 * Allows to handle an interrupt.
 */
static void gicc_irq_handle(void) {
	uint32_t irqstat;
	uint32_t irqnr;

	do {
		// Get the interrupt ID.
		irqstat = gicc_dev->iar;
		irqnr   = irqstat & GICC_IAR_INT_ID_MASK;

		// The interrupt is a SPI.
		if (irqnr > 31 && irqnr < NB_IRQS) {
			irq_process(irqnr);

			gicc_dev->eoir = irqstat;
		}
		// The interrupt is a SGI.
		else if (irqnr < 32) {
			gicc_dev->eoir = irqstat;
		}
	// When there is no more irq to handle, `irqnr` = 1023 (spurious IRQ).
	} while (irqnr != GICC_INT_SPURIOUS);

}


/**
 * Initializes the GIC CPU interface.
 */
static int gicc_init(dev_t *dev) {
	DBG("Begin init GIC CPU interface...\n");

	gicc_dev = (gicc_t *)dev->base;

	gicc_config();

	// Set the priority filter.
	gicc_dev->pmr = GICC_INT_PRI_THRESHOLD;

	// Make the link between the GIC and the structure that allows to handle
	// the interrupts.
	irq_ops.irq_enable  = gicc_irq_enable;
	irq_ops.irq_disable = gicc_irq_disable;
	irq_ops.irq_mask    = gicc_irq_mask;
	irq_ops.irq_unmask  = gicc_irq_unmask;
	irq_ops.irq_handle  = gicc_irq_handle;


	// Enable GICC.
	gicc_dev->ctlr = GICC_ENABLE;

	DBG("End init GICC...\n");

	return 0;
}

/**
 * Initializes the GIC distributor.
 */
static int gicd_init(dev_t *dev) {
	unsigned int i;
	uint32_t cpuMask;

	DBG("Begin init GIC distributor...\n");

	gicd_dev = (gicd_t *)dev->base;

	/* first of all we disable the GIC distributor */
	gicd_dev->ctlr = GICD_DISABLE;

	/* prepare the CPU 0 mask to set it as target for all SPIs */
	cpuMask   = GICD_CPU_0_MASK;
	cpuMask  |= cpuMask << 8;
	cpuMask  |= cpuMask << 16;

	/* set all SPI interrupts to the target CPU 0 */
	for (i = 32; i < NB_IRQS; i += 4) {
		*(gicd_dev->itargetsr + i / 4) = cpuMask;
	}

	/* configure the distributor */
	gicd_config();

	/* finally we enable the GIC distributor */
	gicd_dev->ctlr = GICD_ENABLE;

	DBG("End init GICD...\n");

	return 0;
}


REGISTER_DRIVER(arm_gicd_v1, "arm,cortex-a9-gicd", gicd_init);
REGISTER_DRIVER(arm_gicc_v1, "arm,cortex-a9-gicc", gicc_init);
