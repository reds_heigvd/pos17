/*
 * bcm283x_intc.h
 *
 *  Created on: Dec 6, 2017
 *      Author: camilo
 */

#if 1
#define DEBUG
#endif


#include <device/arch/bcm283x_intc.h>
#include <device/irq.h>
#include <device/driver.h>

/* debug */


static bcm283x_intc_t *intc_regs;



static inline void intc_enable(unsigned int irq)
{
	if (irq < 32) {
		intc_regs->enable_irqs_1 |= (0x1 << irq);
	}
	else if (irq < 64) {
		intc_regs->enable_irqs_2 |= (0x1 << (irq % 32));
	}
	else if (irq < 72) {
		intc_regs->enable_basic_irqs |= (0x1 << (irq % 32));
	}
}

static inline void intc_disable(unsigned int irq)
{
	if (irq < 32) {
		intc_regs->disable_irqs_1 |= (0x1 << irq);
	}
	else if (irq < 64) {
		intc_regs->disable_irqs_2 |= (0x1 << (irq % 32));
	}
	else if (irq < 72) {
		intc_regs->disable_basic_irqs |= (0x1 << (irq % 32));
	}
}

/*
 * same as disable
 */
static inline void intc_mask(unsigned int irq)
{
	intc_disable(irq);
}

/*
 * same as enable
 */
static void inline intc_unmask(unsigned int irq)
{
	intc_enable(irq);
}

static inline void intc_handle(void){
	// call handlers (they clear)
	uint32_t irq_index;
	uint32_t irqs_peripherals;
	uint32_t irqs_1;
	uint32_t irqs_2;

	irqs_peripherals = intc_regs->irq_basic_pending & INTC_BASE_PERIPHERALS_IRQ_MASK;
	irq_index = 64;
	do {
		if (irqs_peripherals & 0x1) {
			irq_process(irq_index);
		}
		++irq_index;
	}
	while(irqs_peripherals >>= 0x1);

	irqs_1 = intc_regs->irq_pending_1;
	irq_index = 0;
	do {
		if (irqs_1 & 0x1) {
			irq_process(irq_index);
		}
		++irq_index;
	}
	while(irqs_1 >>= 0x1);

	irqs_2 = intc_regs->irq_pending_2;
	irq_index = 32;
	do {
		if (irqs_2 & 0x1) {
			irq_process(irq_index);
		}
		++irq_index;
	}
	while(irqs_2 >>= 0x1);
	// clear in handlers
}


static int intc_init(dev_t *dev)
{
	/* set handlers of generic IRQ kernel interface */
	irq_ops = (irq_ops_t) {
		.irq_enable  = intc_enable,
		.irq_disable = intc_disable,
		.irq_mask    = intc_mask,
		.irq_unmask  = intc_unmask,
		.irq_handle  = intc_handle
	};

	intc_regs = (bcm283x_intc_t*)dev->base;

	return 0;
}




/*
 * Map driver initialization with device tree declaration
 * and export function pointer to specific init area
 */
REGISTER_DRIVER(bcm283x_intc, "brcm,bcm2835-armctrl-ic", intc_init);



