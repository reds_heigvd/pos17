/*
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 * Copyright (c) 2016, 2017             Sootech SA,              Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA
 * and must not be shared in any case.
 * 
 * FILE: gic.c
 * 
 * @brief   Here goes implementation of GICv2 interface for GIC-400, with initialization.
 * @author  Luca Sivillica & Gaëtan Othenin-Girard
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 1.0
 * @since   2017-10-26
 */

#if 0
	#define DEBUG 1
#endif

#include <common.h>
#include <device/device.h>
#include <device/driver.h>
#include <device/irq.h>
#include <device/arch/gic.h>

#define GIC_V1_DEV "arm,gic-v1"
#define GIC_V2_DEV "arm,gic-v2"

/**
 * @brief Reference over physical location of GIC distributor
 */
static struct gic_dist_regs* volatile gicd_ref;

/**
 * @brief Reference over physical location of GIC CPU interface
 */
static struct gic_cpu_regs* volatile gicc_ref;

/**
 * @brief  Get current cpu mask for SPI and PPI.
 * @return mask
 */
static uint8_t gic_get_cpumask(void)
{
	uint32_t mask, i;

	for (i = mask = 0; i < 32; i += 4)
	{
		mask  = gicd_ref->itargetsr[i / 4];
		mask |= mask >> 16;
		mask |= mask >> 8;
		if (mask)
		{
			break;
		}
	}

	if (!mask)
	{
		printk("GIC CPU mask not found - kernel will fail to boot.\n");
	}

	return mask;
}

/**
 * @brief Mask given IRQ source in distributor
 * @param irq number of IRQ source
 */
static inline void gic_irq_mask(unsigned int irq)
{
	gicd_ref->icenabler[irq / 32] = 1 << (irq % 32);
}

/**
 * @brief Unmask given IRQ source in distributor
 * @param irq number of IRQ source
 */
static inline void gic_irq_unmask(unsigned int irq)
{
	gicd_ref->isenabler[irq / 32] = 1 << (irq % 32);
}

/**
 * @brief Enable sensitivity to given IRQ source
 * @param irq number of IRQ source
 */
static inline void gic_irq_enable(unsigned int irq)
{
	gicd_ref->isactiver[irq / 32] = 1 << (irq % 32);
}

/**
 * @brief Disable sensitivity to given IRQ source
 * @param irq number of IRQ source
 */
static inline void gic_irq_disable(unsigned int irq)
{
	gicd_ref->icactiver[irq / 32] = 1 << (irq % 32);
}

/**
 * @brief Routine to handle and dispatch IRQs, pending or not.
 * @remark Calls routine from IRQ generic interface.
 */
static inline void gic_irq_handle(void)
{
	uint32_t irqstat,
	         irq_id;

	DBG("IRQ HANDLE !!!!!!!!");

	do
	{
		/* access to interrupt source information (IRQID and CPUID) */
		irqstat = gicc_ref->iar & GICC_IAR_MASK;
		irq_id  = irqstat       & GICC_IAR_INT_ID_MASK;

		if (irq_id < NR_SGI) /* if SGI */
		{
			/* indicate end of interrupt (with CPU ID) */
			gicc_ref->eoir = irqstat;
		}
		else if (irq_id < NR_IRQS) /* if PPI or SPI */
		{
			/* call of routine from IRQ generic interface */
			irq_process(irq_id);

			/* indicate end of interrupt (only IRQID) */
			gicc_ref->eoir = irq_id;
		}
	} while (irq_id < NR_IRQS); /* if IRQ source is unknown */
}

/**
 * @brief Enable GIC and disable bypass of GIC.
 * @param gic [IO] base address to GIC registers
 */
static inline void gic_cpu_if_up(struct gic_cpu_regs * volatile gicc)
{
	gicc->ctlr = (gicc->ctlr & GICC_DIS_BYPASS_MASK) | GICC_ENABLE;
}

/**
 * @brief Disable GIC.
 * @param gic [IO] base address to GIC registers
 */
static inline void gic_cpu_if_down(struct gic_cpu_regs * volatile gicc)
{
	gicc->ctlr &= ~GICC_ENABLE;
}

/**
 * @brief Configure GIC CPU 0 interface.
 * @param gic [IO] base address to GIC registers
 */
static inline void gic_cpu_cfg(struct gic_dist_regs * volatile gicd)
{
	int i;

	/* disable forwarding of PPI */
	gicd->icenabler[0] = GICD_INT_EN_CLR_PPI;
	/* enable  forwarding of SGI */
	gicd->isenabler[0] = GICD_INT_EN_SET_SGI;

	/* set priority of SGI to 160 */
	for (i = 0; i < 8; ++i)
	{
		gicd->ipriorityr[i] = GICD_INT_DEF_PRI_X4;
	}
}

/**
 * @brief Initialize GIC CPU 0 interface.
 * @param gic [IO] base address to GIC registers
 */
static inline void gic_cpu_init(struct gic_cpu_regs * volatile gicc, struct gic_dist_regs * volatile gicd)
{
	DBG("Begin init GIC CPU interface...\n");

	gic_cpu_cfg(gicd);
	gicc->pmr = GICC_INT_PRI_THRESHOLD;
	gic_cpu_if_up(gicc);

	DBG("End init GIC CPU interface...\n");
}

/**
 * @brief Configure GIC distributor.
 * @param gic [IO] base address to GIC registers
 */
static inline void gic_dist_cfg(struct gic_dist_regs * volatile gicd)
{
	int i;

	/* except SGI+PPI, set all SPI interrupts to be level triggered, active low */
	for (i = 32; i < NR_IRQS; i += 16)
	{
		gicd->icfgr[i / 16] = GICD_INT_ACTLOW_LVLTRIG;
	}

	/* except SGI+PPI, set priority to 160 on all SPI interrupts */
	for (i = 32; i < NR_IRQS; i += 4)
	{
		gicd->ipriorityr[i / 4] = GICD_INT_DEF_PRI_X4;
	}

	/*
	 * Disable all SPI interrupts.
	 * Leave the PPI and SGIs alone as they are enabled by redistributor registers
	 */
	for (i = 32; i < NR_IRQS; i += 32)
	{
		gicd->icenabler[i / 32] = GICD_INT_EN_CLR_X32;
	}
}

/**
 * @brief Initialize GIC distributor.
 * @param gic [IO] base address to GIC registers
 */
static inline void gic_dist_init(struct gic_dist_regs * volatile gicd)
{
	int i;

	DBG("Begin init GIC distributor...\n");

	/* disable distributor during configuration */
	gicd->ctlr = GICD_DISABLE;

	/* for each SPI source, map to CPUs (CPU0 here) */
	for (i = 32; i < NR_IRQS; i += 4)
	{
		gicd->itargetsr[i / 4] = GICD_CPU_ENABLE(0);
	}

	/* configure distributor sensitivity to interrupt sources */
	gic_dist_cfg(gicd);

	/* enable distributor once configuration done */
	gicd->ctlr = GICD_ENABLE;

	DBG("End init GIC distributor...\n");
}

/**
 * @brief Initialize GIC CPU interface.
 * @param dev [IN] device tree node of GIC
 * @return 0
 */
static int gicc_init(dev_t *dev)
{
	/* set handlers of generic IRQ kernel interface */
	irq_ops = (irq_ops_t) {
		.irq_enable  = gic_irq_enable,
		.irq_disable = gic_irq_disable,
		.irq_mask    = gic_irq_mask,
		.irq_unmask  = gic_irq_unmask,
		.irq_handle  = gic_irq_handle
	};

	/* get base address of GIC (from device tree) */
	gicc_ref = (struct gic_cpu_regs*) dev->base;

	/* configure both distributor and CPU interfaces */
	gic_cpu_init(gicc_ref, gicd_ref);

	DBG("End init GIC...\n");

	return 0;
}

static int gicd_init(dev_t *dev)
{
	DBG("Start init GIC...\n");

	gicd_ref = (struct gic_dist_regs*) dev->base;
	gic_dist_init(gicd_ref);

	return 0;
}

/*
 * Map driver initialization with device tree declaration
 * and export function pointer to specific init area
 */
REGISTER_DRIVER(gic_v1_dist, GIC_V1_DEV"-dist", gicd_init);
REGISTER_DRIVER(gic_v2_dist, GIC_V2_DEV"-dist", gicd_init);
REGISTER_DRIVER(gic_v1_cpu,  GIC_V1_DEV"-cpu",  gicc_init);
REGISTER_DRIVER(gic_v2_cpu,  GIC_V2_DEV"-cpu",  gicc_init);
