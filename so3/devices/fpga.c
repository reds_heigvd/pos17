/**
 * This file contains the functions from the HAL for FPGA from the Zynq-Zybo board.
 *
 * @authors Gaëtan Othenin-Girard
 * @date    06.11.2017
 *
 * @version 1.0
 */

#include <device/fpga.h>

int fpga_get_reg_val(int reg_addr)
{

}
void fpga_set_reg_val(int reg_addr, int value)
{

}

void fpga_get_reg(fpga_reg_t *reg, int address)
{

}
