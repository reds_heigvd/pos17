/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - June 2017: Alexandre Malki
 * - August 2017: Daniel Rossier
 *
 */
#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/serial.h>
#include <device/arch/pl011.h>

#include <mach/uart.h>

#include <asm/io.h>                 /* ioread/iowrite macros */

static dev_t pl011_dev;

static int  pl011_put_byte(char c) {

	while ((ioread16(pl011_dev.base + UART01x_FR) & UART01x_FR_TXFF)) ;

	iowrite16(pl011_dev.base + UART01x_DR, c);

	return 1;
}


static char pl011_get_byte(void) {

	/* Poll while nothing available */
	while (ioread8(pl011_dev.base + UART01x_FR) & UART01x_FR_RXFE);

	return ioread16(pl011_dev.base + UART01x_DR);
}

static int pl011_init(dev_t *dev) {

	/* Init pl011 UART */

	memcpy(&pl011_dev, dev, sizeof(dev_t));

	serial_ops.put_byte = pl011_put_byte;
	serial_ops.get_byte = pl011_get_byte;

	return 0;
}

void __ll_put_byte(char c) {
	while ((ioread16(UART_BASE + UART01x_FR) & UART01x_FR_TXFF)) ;

	iowrite16(UART_BASE + UART01x_DR, c);
}


REGISTER_DRIVER(pl011_uart, "serial,pl011", pl011_init);
