/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 *
 * This software is released under the MIT License whose terms are defined hereafter.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 * Contributors:
 *
 * - September 2017: Daniel Rossier
 */

#include <common.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/serial.h>

#include <device/arch/ns16550.h>
#include <mach/uart.h>

#include <asm/io.h>                 /* ioread/iowrite macros */

static dev_t ns16550_dev =
{
  .base = UART_BASE,
};

static int baudrate_div_calc(int baudrate)
{
	int divider;

	/* Internal clock is at 24 MHz (UART_CLK_FREQ) */
	divider = (UART_CLK_FREQ + (baudrate * 8)) / (baudrate * 16);

	/* Divider cannot be encoded on 16 bits */
	if (divider > UINT16_MAX)
		BUG();

	return divider;

}

static int ns16550_put_byte(char c)
{
	ns16550_t *ns16550 = (ns16550_t *) ns16550_dev.base;

	while ((ioread32(&ns16550->lsr) & UART_LSR_THRE) == 0) ;

	if (c == '\n') {
		iowrite8(&ns16550->rbr, '\n');	/* Line Feed */

		while ((ioread32(&ns16550->lsr) & UART_LSR_THRE) == 0) ;

		iowrite8(&ns16550->rbr, '\r');	/* Carriage return */

	} else {
		/* Output character */
		
		iowrite8(&ns16550->rbr, c); /* Transmit char */
	}

	return 0;
}


void __ll_put_byte(char c)
{

	/* Busy wait TX empty */
	while ((ioread32(UART_BASE + UART_LSR) & UART_LSR_THRE) == 0);

	/* Output character */
	if (c == '\n') {
		iowrite8(UART_BASE + UART_THR, '\n'); /* Line Feed */

		while ((ioread32(UART_BASE + UART_LSR) & UART_LSR_THRE) == 0) ;
		iowrite8(UART_BASE + UART_THR, '\r'); /* Carriage return */
	}
	else
		iowrite8(UART_BASE + UART_THR, (uint8_t) c);
}


static char ns16550_get_byte(void)
{	 
	ns16550_t *ns16550 = (ns16550_t *) ns16550_dev.base;

	while ((ioread32(&ns16550->lsr) & UART_LSR_DR) == 0);

	return ioread32(&ns16550->rbr);
}

static int ns16550_init(dev_t *dev)
{
	int baudrate = UART_BAUDRATE;
	int divider;
	ns16550_t *ns16550 = (ns16550_t *)dev->base;

	/* Pins multiplexing skipped here for simplicity (done by bootloader) */
	/* Clocks init skipped here for simplicity (done by bootloader) */

	/* Initialize UART controller */
	memcpy(&ns16550_dev, dev, sizeof(dev_t));

	serial_ops.put_byte = ns16550_put_byte;
	serial_ops.get_byte = ns16550_get_byte;

	/* Ensure all interrupts are disabled */
	iowrite32(&ns16550->ier, 0);

	/* Put the UART in 'Configuration mode A' to allow baudrate configuration */
	iowrite32(&ns16550->lcr, UART_LCR_DLAB);

	/* Configure baudrate */
	divider = baudrate_div_calc(baudrate);
	if (divider < 0) {
		return -1;
	}

	iowrite32(&ns16550->dll, divider & 0xFF);
	iowrite32(&ns16550->dlh, (divider >> 8) & 0xFF);

	/* 8N1 standard configuration */
	iowrite32(&ns16550->lcr, UART_PARITY_DIS | UART_1_STOP | UART_8BITS );

	/* Force RTS and DTR lines */
	iowrite32(&ns16550->mcr, UART_RTS | UART_DTR);

	return 0;

}

REGISTER_DRIVER(ns16550_uart, "serial,ns16550", ns16550_init);
