/**
 * -- Smart Object Oriented --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA
 * and must not be shared in any case.
 *
 * FILE: xuartps.c
 *
 * @brief   This file contains the body of all functions which allow
 *          to initialize the UART and handle the communication with it.
 * @author  Luca   Sivillica [CREATED BY]
 * @author  Julien Baeriswyl [CREATED BY] <julien.baeriswyl@heig-vd.ch>
 * @version 1.0
 * @since   2017-11-29
 */
#include <common.h>
#include <types.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/serial.h>
#include <device/arch/xuartps.h>

#include <mach/uart.h>

/**
 * @brief Reference over physical location of UART.
 * @remark Value is written twice:
 *          - Once at compile time.
 *          - Once when DTB is parsed.
 *         This technique allow to use UART very quickly, before drivers initialization.
 */
static struct xuartps_regs* volatile uart_ref = (struct xuartps_regs*)UART_BASE;

/* TODO: fix init
static int chip_init_done = 0;

inline static void __xuartps_init (void)
{
	if (!chip_init_done)
	{
		chip_init_done  = 1;
		uart_ref->ctrl |= (XUARTPS_CTRL_TXRST | XUARTPS_CTRL_RXRST);
		uart_ref->mode |= XUARTPS_MODE_PARITY_NONE;
	}
}
*/

/**
 * @brief  Send (wait availability) byte through UART.
 * @remark If c is '\n', then '\r' is sent first.
 * @param  c [IN] byte to send
 * @return 1
 */
static int xuartps_put_byte(char c)
{
	/* TO REMOVE
	uart_ref->ctrl &= XUARTPS_CTRL_TX_DIS;
	uart_ref->ctrl |= XUARTPS_CTRL_TX_EN;
	*/

	/*
	 * If we detect a line feed we put before the carriage return
	 * to go to the next line.
	 */
	if (c == '\n')
	{
		while (!(uart_ref->channel_status & XUARTPS_STATUS_TXEMPTY));
		uart_ref->tx_rx_fifo = '\r';
	}

	/* Wait on empty send FIFO */
	while (!(uart_ref->channel_status & XUARTPS_STATUS_TXEMPTY));
	uart_ref->tx_rx_fifo = c;

	return 1;
}

/**
 * @brief Receive (wait availability) byte from UART.
 * @return received byte
 */
static char xuartps_get_byte(void)
{
	/* TO REMOVE
	uart_ref->ctrl &= XUARTPS_CTRL_RX_DIS;
	uart_ref->ctrl |= XUARTPS_CTRL_RX_EN;
	 */

	/* Poll while nothing available */
	while (uart_ref->channel_status & XUARTPS_STATUS_RXEMPTY);
	return uart_ref->tx_rx_fifo;
}

/**
 * @brief Initialize Xilinx Zynq-7000 UART at DTB parse time.
 * @param dev [IN] device tree node of UART
 * @return 0
 */
static int xuartps_init(dev_t *dev)
{
	DBG("UART init...\n");
	/* Init xuartps UART */
	uart_ref = (struct xuartps_regs*) dev->base;

	/* configure generic serial device of so3 */
	serial_ops.put_byte = xuartps_put_byte;
	serial_ops.get_byte = xuartps_get_byte;

	return 0;
}

/**
 * @brief Make UART sending feature available before DTB parsing.
 *        This function is the low level put byte that allows
 *        to print logs early during the bootstrap.
 * @remark If c is '\n', then '\r' is sent first.
 * @param  c [IN] byte to send
 */
void __ll_put_byte(char c)
{
	xuartps_put_byte(c);
}

/*
 * Map driver initialization with device tree declaration
 * and export function pointer to specific init area
 */
REGISTER_DRIVER(xuartps_drv, "xlnx,xuartps", xuartps_init);
