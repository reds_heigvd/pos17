#include <device/arch/bcm283x_mu.h>

#include <device/device.h>
#include <device/driver.h>
#include <device/serial.h>

#include <mach/uart.h>

#include <asm/io.h>

static dev_t bcm283x_mu_dev = {
		.base = UART_BASE,
};

static int put_byte(char c) {
	// Busy wait until it has one available space in his fifo transmitter
	while(!(ioread32(bcm283x_mu_dev.base + MU_LSR_REG) & MU_LSR_TX_EMPTY));

	iowrite32(bcm283x_mu_dev.base + MU_IO_REG, c);

	if(c == '\n') {
		while(!(ioread32(bcm283x_mu_dev.base + MU_LSR_REG) & MU_LSR_TX_EMPTY));
		iowrite32(bcm283x_mu_dev.base + MU_IO_REG, '\r');
	}

	return 0;
}

static char get_byte(void) {
	// Busy wait until it has received at least one byte in his fifo receiver
	while(!(ioread32(bcm283x_mu_dev.base + MU_LSR_REG) & MU_LSR_DATA_READY));

	return ioread32(bcm283x_mu_dev.base + MU_IO_REG);
}

void __ll_put_byte(char c) {
	// Busy wait until it has one available space in his fifo transmitter
	while(!(ioread32(UART_BASE + MU_LSR_REG) & MU_LSR_TX_EMPTY));

	iowrite32(UART_BASE + MU_IO_REG, c);

	if(c == '\n') {
		while(!(ioread32(UART_BASE + MU_LSR_REG) & MU_LSR_TX_EMPTY));
		iowrite32(UART_BASE + MU_IO_REG, '\r');
	}
}

static int bcm283x_mu_init(dev_t *dev) {
	memcpy(&bcm283x_mu_dev, dev, sizeof(dev_t));

	serial_ops.put_byte = put_byte;
	serial_ops.get_byte = get_byte;

	return 0;
}

REGISTER_DRIVER(bcm283x_mu, "serial,bcm283x_mu", bcm283x_mu_init);
