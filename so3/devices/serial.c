/*
 * serial.c
 */

#include <common.h>

#include <device/serial.h>

#include <asm/processor.h>

serial_ops_t serial_ops;

/* Outputs an ASCII string to console */
int serial_putc(char c) {
	if (serial_ops.put_byte == NULL) {
		__ll_put_byte(c);
		return 1;
	} else
		return serial_ops.put_byte(c);
}

/* Get a byte from the UART. */
char serial_getc(void) {
	return serial_ops.get_byte();
}

/* Sends some bytes to the UART */
int serial_write(char *str, int len) {
	int i;
	uint32_t flags;

	flags = local_irq_save();

	for (i = 0; i < len; i++)
		if (str[i] != 0)
			serial_putc(str[i]);

	local_irq_restore(flags);

	return len;
}

/* Gets at most len bytes into buf.
 * Puts a '\0' either replacing '\n' or at the end of buf.
 * Returns the number of characters written (excluding '\0'). */
int serial_read(char *buf, int len) {
	int i = 0;

	while (1) {
		buf[i] = serial_getc();

		if (buf[i] == '\r' || buf[i] == '\n') {
			/* replace '\r' or '\n' with '\0' */
			buf[i] = '\0';
			break;
		}

		if (i == len-2) {
			/* add '\0' in the last empty space */
			i++;
			buf[i] = '\0';
			break;
		}

		i++;
	}

	return i;
}

/*
 * Main device initialization function
 */
void serial_init(void) {

	memset(&serial_ops, 0, sizeof(serial_ops_t));

}


