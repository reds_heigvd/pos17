/*
 * gpio.c
 *
 * Auteur : Sydney Hauke
 */

#include <stddef.h>
#include <device/gpio.h>

gpio_ops_t gpio_ops;

gpio_desc_t gpio_desc[NR_GPIOS];

int
gpio_is_valid(unsigned int gpio)
{
	return gpio_ops.is_valid(gpio);
}

void
gpio_config_dir(unsigned int gpio, gpio_dir_t dir)
{
	gpio_ops.config_dir(gpio, dir);
}

gpio_state_t
gpio_get(unsigned int gpio)
{
	return gpio_ops.get(gpio);
}

void
gpio_set(unsigned int gpio, gpio_state_t state)
{
	gpio_ops.set(gpio, state);
}

int
gpio_register_int(unsigned int gpio, gpio_edge_dir_t edge, gpio_int_handler_t action)
{
	if(!gpio_ops.is_valid(gpio)) {
		return -1;
	}

	gpio_desc[gpio].action = action;

	return gpio_ops.enable_int(gpio, edge);
}

int
gpio_unregister_int(unsigned int gpio)
{
	gpio_desc[gpio].action = NULL;

	return gpio_ops.disable_int(gpio);
}

void
gpio_init(void)
{
	int i;

	memset(&gpio_ops, 0, sizeof(gpio_ops_t));

	for(i = 0; i < NR_GPIOS; i++) {
		gpio_desc[i].action = NULL;
	}
}
