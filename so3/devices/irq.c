/*
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 *
 */

#if 1
	#define DEBUG
#endif

#if 0
	#define JITTER_MEASURE
#endif

#include <common.h>
#include <tinyprintf.h>
#include <softirq.h>
#include <stddef.h>

#include <device/irq.h>

#ifdef JITTER_MEASURE
#include <jitter.h>
static jitter_measure_t jit;
#endif

static irqdesc_t irqdesc[NR_IRQS];

irq_ops_t irq_ops;

irqdesc_t *get_irqdesc(int irq) {
	return &irqdesc[irq];
}

int irq_process(int irq) {
	int ret;

	if (irqdesc[irq].action != NULL)
		ret = irqdesc[irq].action(irq);

	return ret;
}


int irq_bind(int irq, irq_handler_t handler, int flags) {


	DBG("Binding irq %d with action at %p\n", irq, handler);
	irqdesc[irq].action = handler;
	irqdesc[irq].irqflags = flags;

	return 0;

}

void irq_mask(int irq) {

	irq_ops.irq_mask(irq);
}

void irq_unmask(int irq) {

	irq_ops.irq_unmask(irq);
}

void irq_enable(int irq) {

	irq_ops.irq_enable(irq);
	irq_ops.irq_unmask(irq);
}

void irq_disable(int irq) {

	irq_ops.irq_mask(irq);
	irq_ops.irq_disable(irq);
}

void irq_handle(void) {
#ifdef JITTER_MEASURE
	jitter_measure(&jit);
	irq_ops.irq_handle();
	jitter_measure(&jit);

	printk("\nGeneral ISR latency : %lu\n", (long unsigned int)jitter_latency(&jit));
#else
	irq_ops.irq_handle();
#endif
}

/*
 * Main interrupt device initialization function
 */
void irq_init(void) {
	int i;

#ifdef JITTER_MEASURE
	jitter_init(&jit);
#endif

	memset(&irq_ops, 0, sizeof(irq_ops_t));

	for (i = 0; i < NR_IRQS; i++) {
		irqdesc[i].action = NULL;

		irqdesc[i].irqflags = 0;
	}

	/* Initialize the softirq subsystem */
	softirq_init();
}
