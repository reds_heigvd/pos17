#include <device/driver.h>

#include <mach/fpga.h>

static int altr_axi_init(dev_t *dev) {
	__fpga_axi_bridge = dev->base;

	return 0;
}

REGISTER_DRIVER(altera_axi_bridge, "altr,axi-bridge", altr_axi_init);
