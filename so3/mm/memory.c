#if 1
#define DEBUG
#endif

#include <common.h>
#include <memory.h>
#include <types.h>

#include <asm/memory.h>
#include <asm/mmu.h>

uint32_t *__sys_l1pgtable;

// Available IO mapping.
static uint32_t __io_mapping;

void io_map_init(void) {
	__io_mapping = IO_MAPPING_BASE;
}

uint32_t io_map(uint32_t phys_addr, uint32_t size) {
	uint32_t virt_addr, offset;

	if (size >= SECTION_SIZE) {
		__io_mapping = ALIGN_UP(__io_mapping, SECTION_SIZE);
	} else {
		__io_mapping = ALIGN_UP(__io_mapping, PAGE_SIZE);
	}

	offset = phys_addr & 0xFFF;

	virt_addr = __io_mapping;

	create_mapping(virt_addr, phys_addr, size);

	__io_mapping += size;

	return virt_addr + offset;
}
