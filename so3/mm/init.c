/*
 * SO3 kernel memory management
 *
 */
#define DEBUG

#include <common.h>
#include <memory.h>
#include <heap.h>
#include <fdt.h>
#include <sizes.h>

#include <asm/cacheflush.h>

#include <generated/autoconf.h>

extern unsigned long __bss_start, __bss_end;
extern unsigned long __vectors_start, __vectors_end;
mem_info_t mem_info;

/*
 * Clear the .bss section in the kernel memory layout.
 */
void clear_bss(void) {
	unsigned char *cp = (unsigned char *) &__bss_start;

	/* Zero out BSS */
	while (cp < (unsigned char *) &__bss_end)
		*cp++ = 0;
}

/*
 * Main memory init function
 */

void memory_init(void) {


	/* Initialize the kernel heap */
	heap_init();

#ifdef CONFIG_MMU
	// Initialize IO mapping.
	io_map_init();
#endif // CONFIG_MMU
}

