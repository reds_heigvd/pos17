---
lang: fr

numbersections: true

papersize: a4

geometry: margin=2cm

header-includes:
    - \usepackage{etoolbox}
    - \usepackage{fancyhdr}
    - \usepackage[T1]{fontenc}
    - \usepackage{xcolor}
    - \usepackage{graphicx}
    - \usepackage{tikz}
    - \usepackage{hyperref}
    - \usepackage{caption}
    - \usepackage{float}

    # Some beautiful colors.
    - \definecolor{pblue}{rgb}{0.13, 0.13, 1.0}
    - \definecolor{pgray}{rgb}{0.46, 0.45, 0.48}
    - \definecolor{pgreen}{rgb}{0.0, 0.5, 0.0}
    - \definecolor{pred}{rgb}{0.9, 0.0, 0.0}

    - \renewcommand{\ttdefault}{pcr}

    # 'fancyhdr' settings.
    - \pagestyle{fancy}
    - \fancyhead[CO,CE]{}
    - \fancyhead[LO,LE]{Projet Zybo}
    - \fancyhead[RO,RE]{HEIG-VD - POS 2017}

    # Redefine TOC style.
    - \setcounter{tocdepth}{3}

    # 'listings' settings.
    - \lstset{breaklines = true}
    - \lstset{backgroundcolor = \color{black!10}}
    - \lstset{basicstyle = \ttfamily}
    - \lstset{breakatwhitespace = true}
    - \lstset{columns = fixed}
    - \lstset{commentstyle = \color{pgreen}}
    - \lstset{extendedchars = true}
    - \lstset{frame = trbl}
    - \lstset{frameround = none}
    - \lstset{framesep = 2pt}
    - \lstset{keywordstyle = \bfseries}
    - \lstset{language = C}
    - \lstset{numbers=left,xleftmargin=2em,xrightmargin=0.25em}
    - \lstset{numberstyle = \small\ttfamily}
    - \lstset{showstringspaces = false}
    - \lstset{stringstyle = \color{pred}}
    - \lstset{tabsize = 2}

    # 'listings' not page breaking.
    - \BeforeBeginEnvironment{lstlisting}{\begin{minipage}{\textwidth}}
    - \AfterEndEnvironment{lstlisting}{\end{minipage}}

    # Set links colors
    - \hypersetup{colorlinks,citecolor=black,filecolor=black,linkcolor=black,urlcolor=black}
    -
---
\makeatletter
\renewcommand{\@maketitle}{%
\newpage
\null
\vfil
\begingroup
\let\footnote\thanks
\begin{center}
\includegraphics[width=10cm]{images/Zybo.png}\vskip1.5em
{\Huge Projet Zybo}\vskip0.5em
{\LARGE Rapport intermédiaire}\vskip1.5em
{
    \begin{tabular}{ll}
        \underline{Membres} & Luca Sivillica \\
        & Gaëtan Othenin-Girard \\
        \\
        \underline{Professeur} & Daniel Rossier \\
        \underline{Assistant} & Jean-Pierre Miceli
    \end{tabular}}\vskip1.5em
{\large\@date}
\end{center}
\endgroup
\vfil
}
\makeatother

\title{}

\date{HEIG-VD - Novembre 2017}

\maketitle

\begin{tikzpicture}[remember picture,overlay]
   \node[anchor=north east,inner sep=0.25cm] at (current page.north east)
              {\includegraphics[width=5cm]{images/HEIG_logo.png}};
\end{tikzpicture}

\newpage

\newpage

\tableofcontents

\listoffigures

\newpage

# Introduction
Dans le cadre du cours POS (*Portage de systèmes d'exploitation*), nous avons réalisé un projet de portage du système d'exploitation *SO3* sur la plateforme *Zybo* (*ZYnq BOard*). Cette carte est principalement composée, entre autre, de deux coeurs *ARM Cortex-A9* et d'une FPGA *Xilinx*. Elle comporte aussi des GPIO connecté à 6 boutons poussoirs, 4 switchs et 5 leds. Le schéma ci-dessous présente l'architecture de la board *Zybo*. Dans ce projet, nous avons principalement utilisé le bus `General Purpose AXI Ports` connecté à la FPGA.

\begin{figure}[H]
         \centering
         {\includegraphics[width=10cm]{images/Zybo_arch.jpg}}
         \caption[Architecture de la board Zybo]{Architecture de la board Zybo}
 \end{figure}

Ce projet est découpé en plusieurs sous-étapes dont ce rapport présente les deux premières : le portage d'U-Boot et l'implémentation d'un GIC (*Generic Interrupt Controller*) pour plateforme émulée.

# Etape U-Boot
Le but de cette première étape est de comprendre le fonctionnement d'U-Boot et ainsi le porter sur la carte Zybo.

## Partie 1 : commande "Hello World"
Dans un premier temps, nous avons dû créer une nouvelle commande U-Boot et l'intégrer dans U-Boot pour *vexpress*. Afin de comprendre comment une commande est ajoutée, nous nous sommes référés aux documents U-Boot ainsi qu'au code source de commandes déjà implémentées.

Pour commencer, nous avons déterminé dans quel répertoire des sources d'U-Boot devait se trouver notre commande. Dans U-Boot pour *vexpress*, les sources des commandes se trouvent dans le répertoire *common*. Nous avons donc créé un fichier `cmd_hello.c` avec le fonctionnement de la commande :

```
static int do_hello(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {

	printf("Hello world !\n");

	return 0;
}
```

Une fois le code écrit, il faut pouvoir intégrer la commande dans U-Boot. Pour cela, il faut deux étapes :

1. Il faut premièrement utiliser la macro d'ajout de commande (déclarée dans `command.h`). Cette macro prend comme paramètre son nom (le nom tapé par l'utilisateur pour l'appeler), son nombre d'arguments (ce nombre compte la commande elle-même comme un argument), si celle-ci est répétable (0 ou 1), la fonction à exécuter, la description de la commande et enfin le texte affiché dans *help* :

```
U_BOOT_CMD(
	hello,	1,	0,	do_hello,
	"print \"hello world\" on the console",
	"\n"
	"      - print \"hello world\" on the console"
);
```

2. Il faut ensuite ajouter notre nouvelle commande dans le *Makefile*. Celle-ci doit être ajoutée dans la liste des commande :

```
obj-$(CONFIG_CMD_HELLO) += cmd_hello.o
```

Cependant, la variable `CONFIG_CMD_HELLO` n'est pas encore déclarée. Pour cela, il faut modifier le fichier `include/configs/vexpress_common.h` en ajoutant le *define* :

```
#define CONFIG_CMD_HELLO
```

Les deux étapes terminées, il faut maintenant compiler U-Boot et lancer *qemu* afin de tester son bon fonctionnement.

\begin{figure}[H]
         \centering
         {\includegraphics[width=10cm]{screenshots/Qemu_HelloCommand.png}}
         \caption[Test de la commande hello sur qemu]{Test de la commande hello sur qemu}
 \end{figure}

## Partie 2 : Déploiement de U-Boot
Cette deuxième partie consiste à déployer U-Boot sans ajout sur la carte Zybo et d'ensuite ajouter la commande `hello` à U-Boot pour Zybo. Dans cette étape, nous allons utiliser la version 2016 d'U-Boot. Afin de déployer U-Boot sur la Zybo, nous avons utilisé les outils Xilinx Vivado et le SDK.

Les étapes résumées ci-dessous sont détaillées dans le document *Guide d'installation pour Zybo (v2017.1.1)*.

### Déploiement U-Boot simple
Premièrement, il faut générer le bitstream de la Zybo grâce à Vivado. Pour cela, nous avons utilisé les fichiers du *zybo_base_system* afin de créer le bitstream dans Vivado.

Il a ensuite fallu lancer le SDK afin de créer le `fsbl` (*first stage bootloader*) et le compiler.

**Remarque:** Pour cette étape, nous avons installé les programmes Xilinx sur un répertoire sur la machine hôte et partagé avec la machine virtuelle car celle-ci ne disposait pas assez d'espace. La compilation du `fsbl` échouait car le lien de certaines librairies ne se faisait pas correctement. Il faut impérativement installer les programmes directement sur le disque dur de la machine.

Une fois le `fsbl` compilé, il faut compiler U-Boot 2016. Il faut ensuite créer l'image de boot de la Zynq depuis le SDK Xilinx en ajoutant le `fsbl.elf`, `u-boot.elf` ainsi que le bitstream de la Zybo.

L'image de boot doit être copié sur la partition *fat32* prévue à cet effet sur la carte SD et celle-ci placée dans la Zybo. Au démarrage de la carte, U-Boot se lance.

### Déploiement U-Boot avec commande
Pour cette étape, les manipulations du déploiement d'U-Boot simple (génération du bitstream et compilation du `fsbl`) n'ont pas besoin d'être refaites. Le but ici est de redéployer U-Boot mais cette fois-ci en incluant la commande `hello` (voir Partie 1).

Il faut donc reprendre le code (`cmd_hello.c`) écrit lors de la première partie. Cette fois-ci, le fichier doit être placé dans le répertoire *cmd* du projet U-Boot 2016 et renommé `hello.c`. Il faut ensuite, tout comme pour *vexpress*, rajouter la commande dans le *Makefile*. La différence avec la première méthode est qu'il faut définir la fonction dans le fichier *Kconfig*. En effet, U-Boot 2016 utilise *Kbuild* et il faut donc ajouter la commande dans la configuration (le fichier *Kconfig* du répertoire *cmd*). Nous avons ajouté un menu *Personnal commands* afin d'ajouter nos commandes :

```
menu "Personnal commmands"

config CMD_HELLO
	bool "hello"
	default y
	help
	  This command prints "hello world" on the console.

config CMD_FPGA_IO
	bool "fpgaIO"
	default y
	help
	  This command allows to read the state of a push button on the FPGA
	  and turn on a LED on the FPGA.

endmenu
```

Nous avons ensuite pu vérifier que la commande `hello` a bien été ajouté grâce à la commande `make menuconfig` :

\begin{figure}[H]
         \centering
         {\includegraphics[width=10cm]{screenshots/Kconfig_AddHelloCommand.png}}
         \caption[Kbuild, ajout de la commande hello]{Kbuild, ajout de la commande hello}
 \end{figure}

Une fois U-Boot compilé, il faut à nouveau créer l'image de boot dans le SDK Xilinx et le copier sur la partition de la carte SD.

**Remarque:** Cette étape a engendré un problème qui est resté longtemps sans solution. Effectivement, lors du démarrage de la carte Zybo, U-Boot commençait à s'initialiser, crashait et redémarrait en boucle. Le problème n'apparaissait que lorsqu'une commande était ajoutée. Nous avons alors effectué des recherches pour trouver l'origine et une résolution du problème qui venait probablement de la compilation du *fsbl*.

#### Source du problème
Premièrement, il faut noter le fait que le problème n'apparaissait que lorsque la commande `hello` était ajoutée. Le message d'erreur avant le plantage d'U-Boot suivant apparaissait :

\begin{figure}[H]
         \centering
         {\includegraphics[width=12cm]{screenshots/crash_u-boot.jpg}}
         \caption[Crash d'U-Boot après l'ajout de la commande hello]{Crash d'U-Boot après l'ajout de la commande hello}
 \end{figure}

Des recherches ont d'abord été faites sur internet afin de trouver une origine au problème. Malheureusement, il n'y avait que très peu d'informations concernant cette erreur spécifiquement, la seule information parlant plus ou moins de ce problème spécifique indiquait qu'il pouvait venir d'un alignement mémoire. Nous avons écarté ce problème car il n'y avait aucune raison qu'il y ait un mauvais alignement mémoire.

Les recherches sur internet n'ayant pas de réponses précises sur le problèmes rencontré, il a fallu invertiguer dans le code. Heureusement, U-Boot nous indique quelques informations quant à l'origine du problème. En reprenant les informations, indiquées, nous avons pu déterminer à quel endroit exactement le programme s'arrêtait (adresses courante du *pc*). En analysant le code assembleur à l'adresse trouvée, nous nous sommes rendu compte que le programme crashait au moment où il effectuait une comparaison de chaîne de caractère avec `strcmp` dans une fonction permettant de trouver les commandes integrées dans U-Boot en parcourant la table des commandes. Nous pouvons retrouver la ligne de code dans la fonction parcourant la table des commandes (fonction `find_cmd_tbl` dans `common/command.c` à la ligne 107) :

```
if (strncmp(cmd, cmdtp->name, len) == 0) {
```

En collaboration avec notre professeur, nous avons ensuite effectué différents tests afin de déterminer la cause du problème.

En ajoutant un affichage de la valeur des arguments de `strcmp`, il apparaissant que l'on comparait la chaîne "test" avec chaque nom de commande sur 4 caractères. Lorsque la commande était "hello", le programme crashait. Le problème venait donc bien de l'ajout de cette commande, mais il était impossible de déterminer précisemment à quel moment cela posait soucis.

Nous avons ensuite essayé de déterminer dans la fonction `main_loop` du fichier `common/main.c` à quel moment le programme s'arrêtait avec une suite de `printf`. Nous avons alors découvert que le programme crashait après avoir appelé `cli_init()` (ligne 54). Nous avons alors commenté cette ligne et le programme replantait à la ligne 66 (appel à `autoboot_command()`) que nous avons aussi commenté. U-Boot ne plantait alors plus. Mais les commandes n'avaient pas été chargées et donc rien ne pouvait être utilisé dans le terminal.

Nous avons donc encore essayé de désactiver des modules U-Boot non-utilisés qui semblaient poser des problèmes, sans succès. Nous avons alors lancer un `make clean` sur le fsbl et U-Boot. Nous avons essayé une autre solution proposée par notre professeur. Nous avons ajouté les *include* du fichier où la commande `printenv` était implémentée (`cmd/nvedit.c`) dans notre fichier `hello.c` (nous n'avions inclut que `common.h` et `command.h`) :

```
#include <common.h>
#include <cli.h>
#include <command.h>
#include <console.h>
#include <environment.h>
#include <search.h>
#include <errno.h>
#include <malloc.h>
#include <mapmem.h>
#include <watchdog.h>
#include <linux/stddef.h>
#include <asm/byteorder.h>
#include <asm/io.h>
```

Une fois recompilé, le problème était résolu. En effet, il se pouvait que le compilateur ne voit pas d'erreur dans nos "include" mais U-Boot nécessite d'inclure d'autres headers. Cepdendant, nous avons continué d'inverstiguer afin de trouver quel "include" posait problème et après les avoir tous enlevés, U-Boot avec la commande `hello` fonctionnait toujours correctement. Il est donc fortement probable que **le problème venait de la compilation du fsbl** que nous avions "nettoyée" juste avant et recompilé.

## Partie 3 : accès à la FPGA
Le but de cette partie est de développer une commande U-Boot permettant d'accéder à deux registres de la FPGA. Le but est de créer une commande permettant d'interagir avec les leds (**1**) en *write only* et les boutons poussoirs (**2**) en *read only*.

\begin{figure}[H]
         \centering
         {\includegraphics[width=10cm]{images/Zybo_Leds_Buttons.png}}
         \caption[Position des leds et boutons sur la Zybo]{Position des leds et boutons sur la Zybo}
 \end{figure}

### Adresses des registres
La première étape consistait à trouver à quelle adresse se trouvent les registres de la FPGA. En recherchant dans la documentation de Xilinx, nous avons tout d'abord trouvé l'adresse de base de la FPGA (nous n'utiliserons que *M_AXI_GP0*) :

\begin{figure}[H]
         \centering
         {\includegraphics[width=10cm]{images/Zynq_Adresses.jpg}}
         \caption[Plan d'adressage de la Zynq]{Plan d'adressage de la Zynq}
 \end{figure}

Puis, nous avons aussi trouvé dans la documentation la description des registres de la FPGA. Afin d'écrire les valeurs des leds et lire les switchs, nous allons utiliser le registre *GPIO_Data* à l'offset `0x0000` :

\begin{figure}[H]
         \centering
         {\includegraphics[width=10cm]{images/Zynq_AXI_Registers.png}}
         \caption[Registres de l'AXI GPIO de la Zynq]{Registres de l'AXI GPIO de la Zynq}
 \end{figure}

Nous avons ensuite trouvé les adresses exactes de nos leds et switchs (dépendant du *bitstream*) dans le logiciel *Vivado* dans *Open Block Design* (**1**), puis dans l'onglet *Address Editor* (**2**) :

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/Vivado_RegAdresses.png}}
         \caption[Affichage des registres sous Vivado]{Affichage des registres sous Vivado}
 \end{figure}

Les registres se trouvent donc aux adresses `0x41200000` (boutons) et `0x41210000` (leds). Chaque registre a une taille de 32 bits et la valeur modifiable (DATA) se trouve à l'offset `0x0000`. Nous avons donc créé des *define* dans notre commande `fpgaIO` permettant d'accéder aux registres :

```
#define FPGA_AXI_GPIO0_DATA_LEDS *(vu32 *) 0x41210000 // FPGA register of LEDs
#define FPGA_AXI_GPIO0_DATA_BTNS *(vu32 *) 0x41200000 // FPGA register of push buttons
```

**Remarque:** Nous avions d'abord recherché dans la *datasheet* de la Zybo pour trouver les adresses des leds et des switchs, mais nous n'avons trouvé celles-ci que dans *Vivado* étant donné qu'elles dépendent du bitstream.

### Accès aux registres
Une fois les adresses trouvées, nous avons créé des fonctions permettant de lire l'état des switchs et écrire dans les leds. Chaque fonction utilise un numéro de switch/led donné en paramètre et permettant d'accéder au bon bit du registre grâce à un décalage.

**Lecture des switchs :**

Cette fonction va lire l'état d'un bouton dont le numéro est donné en paramètre :

```
u8 readButton(u8 buttonNumber)  {
	return FPGA_AXI_GPIO0_DATA_BTNS & (1 << buttonNumber) ? 1 : 0;
}
```

**Ecriture des leds :**

Nous avions d'abord pensé créer deux fonctions, pour allumer et éteindre une led. Cependant, la relecture de l'état des leds n'est pas possible et nous ne pouvons donc pas modifier la valeur d'un seul bit et garder l'état des autres. Nous avons donc créé une seule fonction permettant d'allumer une led dont la position est passée en paramètre :

```
void  setLed(u8 ledNumber) {
	FPGA_AXI_GPIO0_DATA_LEDS = (1 << ledNumber);
}
```

### Commande
Une fois les fonctions d'accès aux valeurs des boutons et leds créées, nous avons développé notre commande. Celle-ci possède 2 options prenant chacune un paramètre correspondant au numéro de la led ou du bouton :

- led-on [led number] : permet d'allumer la led.
- btn-state [btn number] : permet de vérifier l'état du bouton.

Nous avons donc écrit la fonction `do_fpgaIO` allant vérifier les paramètres donnés (et affiche un message d'aide si ceux-ci sont incorrects) et qui va utiliser les fonctions d'accès décrits précédemment afin d'intéragir avec les IO de la FPGA.

La commande a ensuite été ajoutée grâce à l'appel de la macro `U_BOO_CMD` :

```
U_BOOT_CMD(
	fpgaIO,	CMD_EXCPECTED_ARGS,	0,	do_fpgaIO,
	"allows to read the state of one push button and turn on one led on the FPGA.",
	" led-on | btn-state <IO number>\n"
	"led operation:\n"
	"      - led-on    <led number>   turn on a led\n"
	"push button operation:\n"
	"      - btn-state <btn number>   read the state of a button: 1 if is activated, otherwise 0"
);
```

Finalement, nous avons effectué la modification du fichier *Kconfig* et l'ajout de la commande dans le *Makefile* (voir point *Déploiement U-Boot avec commande* de la partie 2).

## Tests
Une fois U-Boot déployé sur la Zybo, nos deux commandes implémentées et ajoutées à U-Boot en ayant éliminer le problème au démarrage, nous avons pu tester leur fonctionnement. Nous avons d'abord effectué un simple test en appelant la commande `hello` qui nous a bien affiché le message attendu. Ensuite, nous avons testé la commande `fpgaIO` tout d'abord avec les leds où chaque led a pu être allumée et ensuite avec les boutons poussoirs dont nous avons pu lire l'état `1` lorsqu'ils étaient appuyés et `0` autrement.

# Etape générique - Emulation
Le but de cette étape est d'analyser et de développer le code du GIC (*Generic Interrupt Controller*) sur le système SO3 pour la plateforme *vexpress*.

## Le GIC
Le but principal du GIC est de centraliser et de dispatcher les interruptions sur les différents CPU d'un système. Il est découpé en deux parties qui sont le **distributeur** et l'**interface CPU**. Les autres tâches du GIC sont par exemple la configuration des priorités des interrutpions, l'activation ou désactivation de celles-ci, définir si elles peuvent être préemptés ou encore, par exemple, filtrer toutes les interruptions ayant une priorité inférieure à une priorité donnée.

\begin{figure}[H]
         \centering
         {\includegraphics[width=10cm]{screenshots/how_gic_works.png}}
         \caption[Fonctionnement d'un GIC avec quatre CPU]{Fonctionnement d'un GIC avec quatre CPU}
 \end{figure}

Le GIC possède plusieurs lignes d'entrée d'interruptions et une ligne de sortie pour chaque CPU du système. Entre le GIC et les CPU, on trouve une *CPU Interface* qui sert d'interface entre le CPU et le distributeur. Elle permet d'identifier le numéro d'une IRQ et gérer le comportement des interruptions.

Dans le cas de SO3, ce dernier ne supporte qu'un seul CPU. Ce document va donc présenter l'implémentation du GIC pour un système monoprocesseur. L'implémentation du GIC au niveau software nécessite donc trois étapes :

1. Initialisation du distributeur : le distributeur est le système allant dispatcher les différentes interruptions vers les CPU de la machine.
2. Initialisation des interfaces CPU : le but est d'initialiser les registres utilisés par les interfaces CPU, ainsi que l'activation du signalement d'une interruption au CPU.
3. Fonction de gestion des IRQ : il faut ensuite coder le comportement du GIC, c'est-à-dire les fonctions permettant de gérer les interruptions.

Le développement d'un GIC pour un système multiprocesseur comporte les mêmes étapes d'initialisation (avec l'initialisation de registres supplémentaires), mais possédera une partie plus complexe au niveau de l'algorithme de distribution.

## Implémentation du GIC
Comme décrit précédemment, l'implémentation du GIC se découpe en trois parties principales. Afin de comprendre et réaliser notre propre GIC pour SO3, nous nous sommes inspirés du code du GIC Linux et nous sommes aidés de la documentation *ARM*.

Le GIC sous SO3 possède déjà un fichier header (`gic.h`) possédant des `define` et une structure représentant tous les registres du GIC. Les registres sont découpés en deux sections : les registres du distributeur et ceux de l'interface CPU.

**Remarque:** Nous avons ajouté des `define` dans le fichier `gic.h` nécessaire à l'implémentation du GIC. Ils seront indiqués dans la description de chaque étape du développement.

### Adresse de base et device-tree
Dans le fichier header `gic.h`, il y a une structure qui représente les registres du GIC. Cependant, l'adresse de base du GIC n'est pas précisée. En effet, celle-ci n'est pas forcément la même sur toutes les plateformes, contrairement aux *offsets* des registres qui, eux, sont les mêmes pour tous les GIC (de même version).

Pour utiliser l'adresse de base du GIC, nous avions tout d'abord utilisé une adresse *fixe* définie en dur dans le code. Cependant, le GIC implémenté dans le Kernel devrait être indépendant de la plateforme. L'utilisation d'un device-tree est alors indispensable. Nous avons donc modifié le device-tree de SO3 afin d'ajouter notre GIC :

```
/* General interrupt controller */
interrupt-controller@2c001000 {
        compatible = "arm-cortex-a15-gic";
        reg = <0x2c001000 0x1000>; /* GIC distributor */
        status = "ok";
};
```

Le but est ensuite de récupérer les adresses déterminées dans le device-tree afin de pouvoir accéder aux registres du GIC. Pour cela, nous avons d'abord ajouté la macro `REGISTER_DRIVER()` permettant d'enregistrer notre périphérique afin que, lors du parse du device-tree, le *parser* puisse trouver notre driver. Les informations de notre noeud sont récupérées et stockées dans une structure `dev_t` via une fonction passée en paramètre de la macro (notre fonction d'initialisation). La macro prend trois paramètre correspondant respectivement au nom du device, la chaîne de caractère du paramètre compatible et la fonction d’initialisation allant prendre en paramètre la structure `dev_t` qui est remplie avec les informations parsées. Cette fonction correspond à l'initialisation de notre GIC.

```
REGISTER_DRIVER(gic_400, "arm-cortex-a15-gic", gic_init);
```

Il faut ensuite écrire la fonction d'initialisation générale du GIC (passée en paramètre de la macro). Cette fonction va d'abord permettre de récupérer l'attribut de la structure `dev_t` nous intéressant (l'adresse de base) :

```
gic_data = (struct intc_regs *) dev->base;
```

Ensuite, il faut *mapper* nos fonctions du contrôleurs du GIC mises à disposition pour le reste du système. Pour cela, nous utilisons la structure `irq_ops` (disponible dans `device/irq.h`) afin de relier nos fonction. Cette structure générale va ajouter une couche d'abstraction afin de pouvoir interagir avec les interruptions de manière totalement transparente :

```
/* make the link between the GIC and the structure that allows
   to handle the interrupts */
irq_ops.irq_enable  = gic_irq_enable;
irq_ops.irq_disable = gic_irq_disable;
irq_ops.irq_mask    = gic_irq_mask;
irq_ops.irq_unmask  = gic_irq_unmask;
irq_ops.irq_handle  = gic_irq_handle;
```

Pour finir, nous appelons nos fonctions d'initialisations du distributeur et de l'interface CPU :

```
gic_dist_init();

gic_cpu_init();
```

La fonction d'initialisation générale étant écrite, il faut maintenant créer toutes les fonctions appelée par celle-ci afin d'initialiser les différentes parties du GIC et de mettre à disposition ses fonctionnalités.

### Initialisation du distributeur
Nous avons créé un diagramme d'activité afin de décrire le processus d'initialisation du distributeur. Cette initialisation va travailler avec les différents registres du distributeur (précisés dans le diagramme) afin de *setter* toutes les valeurs nécessaires.

\begin{figure}[H]
         \centering
         {\includegraphics[height=15cm]{diagrams/DistInit_Diagram.png}}
         \caption[Diagramme de l'initialisation du distributeur]{Diagramme de l'initialisation du distributeur}
 \end{figure}

**Détails d'implémentation :**

La première étape d'initialisation du distributeur consiste à désactiver le distributeur via le registre `GICD_CTLR`.

```
/* first of all we disable the GIC distributor */
gic_data->gicd_ctlr = GICD_DISABLE;
```

Puis, il faudrait, dans le cas de plusieurs coeurs, récupérer le nombre d'IRQ maximum que le ou les CPU peut/peuvent gérer. Cette valeur est stockée dans le registre `GICD_TYPER` sur les bits 0 à 4. Une fois cette valeur récupérée, on additionne 1 et ensuite on multiplie le tout par 32. Puis, on vérifie que le nombre maximum d'IRQ ne dépasse pas 1020 (nombre maximum géré par le GIC). Si c'est le cas, on fixe ce nombre à 1020. Dans notre cas (sur SO3), le nombre maximum d'IRQ est fixé à **128** (un `define` est déclaré dans `gic.h`).

```
#define NR_IRQS		128
```

Il faut ensuite récupérer le masque de notre CPU afin de pouvoir ensuite *setter* ce CPU comme étant la cible par défaut de nos interruptions. Nous avions tout d'abord écrit une fonction permettant de récupérer le masque de notre CPU. Celle-ci parcourait les registres `GICD_ITARGETSR` afin de récupérer le masque sur les bits correspondants.

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GICD_ITARGETSR.png}}
         \caption[Registre GICD\_ITARGETSR]{Registre GICD\_ITARGETSR}
 \end{figure}

Chaque bloc de 8 bits correspond à une interruption et chaque bit de ces blocs correspond à une interface CPU.

\begin{figure}[H]
         \centering
         {\includegraphics[width=7cm]{screenshots/GIC_ITARGETSR_CPUinterfaces.png}}
         \caption[Correspondance des bits du registre GICD\_ITARGETSR avec les CPU]{Correspondance des bits du registre GICD\_ITARGETSR avec les CPU}
 \end{figure}


SO3 ne supportant qu'un seul CPU, nous avons modifié la façon d'utiliser le masque CPU. En effet, nous avons ajouté une valeur définie dans `gic.h` correspondant au masque représentant uniquement le CPU0, c'est-à-dire la valeur `0x1` (voir tableau des valeurs du registre).

Une fois le masque défini, il faut créer un masque contenant notre masque du CPU0 pour chaque champ (correspondant aux SPI) des registres `GICD_ITARGETSR` et le remplir avec notre masque. Ainsi, toutes les IRQ de type SPI cibleront notre CPU0 :

```
/* prepare the CPU 0 mask to set it as target for all SPIs */
cpuMask   = GICD_CPU_0_MASK;
cpuMask  |= cpuMask << 8;
cpuMask  |= cpuMask << 16;

/* set all SPI interrupts to the target CPU 0 */
for (i = 32; i < NR_IRQS; i += 4) {
        *(gic_data->gicd_itargetsr + i / 4) = cpuMask;
}
```

L'étape suivante consiste à configurer le distributeur et plus spécifiquement à donner des paramètres pour nos SPI. Cette configuration se découpe en trois étapes :

1. Configurer les SPI pour qu'elles soient sensibles au niveau du signal et active bas (lorsque le niveau du signal est bas, l'interruption est active). Pour cela, il faut *setter* la valeur `0x0` à tous les champs (2 bits) des registres `GICD_ICFGR` :

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GICD_ICFGR.png}}
         \caption[Registre GICD\_ICFGR]{Registre GICD\_ICFGR}
 \end{figure}

```
/* set all SPI interrupts to be level triggered, active low */
for (i = 32; i < NR_IRQS; i += 16) {
        *(gic_data->gicd_icfgr + i / 16) = GICD_INT_ACTLOW_LVLTRIG;
}
```

2. Configurer les priorités pour les SPI. Pour cela, on va fixer une valeur de priorité (plus cette valeur est basse et plus la priorité est élevée) sur les registres `GICD_IPRIORITYR`. Nous avons choisi la valeur `0xA0` (valeur utilisée dans l'initialisation du GIC sur Linux), mais celle-ci est complétement arbitraire. Si nous voulions, par exemple, des priorités très élevées, il aurait fallu *setter* cette valeur à `0x0` :

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GICD_IPRIORITYR.png}}
         \caption[Registre GICD\_IPRIORITYR]{Registre GICD\_IPRIORITYR}
 \end{figure}

```
/* Set priority on all SPI interrupts */
for (i = 32; i < NR_IRQS; i += 4) {
	*(gic_data->gicd_ipriorityr + i / 4) = GICD_INT_DEF_PRI_X4;
}
```

3. Désactiver toutes les IRQ de type SPI (on ne touche pas les PPI et SGI car elles sont gérées côté configuration de l'interface CPU) en donnant la valeur `0xFFFFFFFF` sur les registres `GICD_ICENABLER` (*Interrupt Clear-Enable Registers*) :

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GICD_ICENABLER.png}}
         \caption[Registre GICD\_ICENABLER]{Registre GICD\_ICENABLER}
 \end{figure}

```
/* Disable all SPI interrupts. Leave the PPI and SGIs alone
   as they are enabled by redistributor registers */
for (i = 32; i < NR_IRQS; i += 32) {
	*(gic_data->gicd_icenabler + i / 8) = GICD_INT_EN_CLR_X32;
}
```

Puis, la dernière étape de l'initialisation du distributeur du GIC est de l'activer via le registre `GICD_CTLR` *settant* son bit 0 à `1` :

```
/* finally we enable the GIC distributor */
gic_data->gicd_ctlr = GICD_ENABLE;
```

## Initialisation de l'interface CPU
Une fois le distributeur initialisé, il faut initialiser l'interface CPU. Cette partie nécessite moins de configurations que la précédente. Elle sert principalement à initialiser la communication entre le GIC et le CPU et à configurer les interruptions de type PPI et SGI.

\begin{figure}[H]
         \centering
         {\includegraphics[height=10cm]{diagrams/CPUInit_Diagram.png}}
         \caption[Diagramme d'initialisation de l'interface CPU du GIC]{Diagramme d'initialisation de l'interface CPU du GIC}
 \end{figure}

**Détails d'implémentation :**

Premièrement, il faut configurer les interruptions PPI et SGI. Pour cela, nous avons ajouté une fonction `gic_cpu_config()` allant tout d'abord désactiver les PPI et activer les SGI. Pour désactiver les PPI, on clear en activant les 16 derniers bits (correspondant aux PPI) du registres `GICD_ICENABLER`. Pour les activer les SGI, on effectue la même manipulation mais sur les 16 premiers bits (correspondant aux SGI) du registre `GICD_ISENABLER`. Puis, on ajoute les priorités pour les interruptions (PPI et SGI), comme pour le distributeur avec des valeurs de `0xA0` (valeur arbitraire reprise de Linux) sur les registres `GICD_IPRIORITYR`.

```
static void gic_cpu_config(void) {
	unsigned int i;

	/* deal with the banked PPI and SGI interrupts, disable all
	   PPI interrupts, ensure all SGI interrupts are enabled */
	*(gic_data->gicd_icenabler) = GICD_INT_EN_CLR_PPI;
	*(gic_data->gicd_isenabler) = GICD_INT_EN_SET_SGI;

	/* set priority on PPI and SGI interrupts */
	for (i = 0; i < 32; i += 4) {
		*(gic_data->gicd_ipriorityr + i / 4) = GICD_INT_DEF_PRI_X4;
	}
}
```

Deuxièmement, il faut ajouter un filtre de priorité. Cette manipulation va permettre de définir une valeur minimale de priorité. Toutes les interruptions ayant une priorité plus haute que cette valeur seront transmises au CPU. Pour cela, on utilise le registre `GICC_PMR` (*Interrupt Priority Mask Register*), dont les 8 premiers bits correspondent à la valeur minimale du filtre. Tout comme pour les priorités, nous nous sommes inspirés des valeurs du GIC de Linux et nous avons donc fixé la valeur à `0xF0`.

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GICC_PMR.png}}
         \caption[Registre GICC\_PMR]{Registre GICC\_PMR}
 \end{figure}

```
/* set the priority filter */
gic_data->gicc_pmr = GICC_INT_PRI_THRESHOLD;
```

Finalement, il faut activer la signalisation des interruptions par l'interface CPU connectée au processeur. Pour cela, nous avons ajouté une fonction `gic_cpu_if_up()` allant modifier la valeur du registre `GICC_CTLR`. La fonction va d'abord désactiver les bits de *bypass*. Puis, elle active l'interface CPU afin de permettre la signalisation de l'interruption en *settant* le bit 0 à `1` :

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GICC_CTLR.png}}
         \caption[Registre GICC\_CTLR]{Registre GICC\_CTLR}
 \end{figure}

```
static void gic_cpu_if_up(void) {
	uint32_t bypass = 0;

	/* preserve bypass disable bits to be written back later */
	bypass  = gic_data->gicc_ctlr;
	bypass &= GICC_DIS_BYPASS_MASK;

	gic_data->gicc_ctlr = bypass | GICC_ENABLE;
}
```

## Mise à disposition des fonctionnalités du GIC
Une fois le GIC initialisé, il faut encore ajouter les fonctions mises à disposition au reste du système par le GIC.

Les quatre premières fonctions mises à disposition servent à masquer/démasquer et activer/désactiver une interruption. Elles fonctionnent toutes de la même manière, c'est-à-dire qu'elles vont créer un masque d'après le numéro de l'IRQ passé en paramètre. Le masque est créé avec le numéro de l'IRQ sur lequel on effectue un *modulo 32* (en effet, nous avons 128 IRQ possibles et découpés en 4 registres de 32 bits) et en le décalant afin de sélectionner le bon bit de notre registre. On écrit ensuite la valeur dans le registre correspondant :

- **Masquage :** `GICD_ICENABLER`
- **Démasquage :** `GICD_ISENABLER`
- **Activation :** réutilisation de `gic_irq_unmask()`
- **Désactivation :** réutilisation de `gic_irq_mask()`

Exemple de code des fonctions (ici, le masquage) :

```
static void gic_irq_mask(unsigned int irq) {
	uint32_t maskIrqID = 1 << (irq % 32);

	*(gic_data->gicd_icenabler + irq / 32) = maskIrqID;
}
```

La cinquième fonction mise à disposition est le *handle* d'interruptions. Cette fonction va permettre de traiter et acquitter les interruptions en attente de traitement. Cette fonction va effectuer une boucle tant qu'il y a une interruption à traiter.

Premièrement, on va récupérer le numéro de l'interruption en attente. Le numéro de l'interruption en attente est stocké dans le registre `GICC_IAR`. La lecture de ce registre va alors acquitter notre interruption :

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GICC_IAR.png}}
         \caption[Registre GICC\_IAR]{Registre GICC\_IAR}
 \end{figure}

```
/* get the interrupt ID */
irqstat   = gic_data->gicc_iar;
irqnr     = irqstat & GICC_IAR_INT_ID_MASK;
```

Ensuite, on test le type de notre interruption. Deux cas peuvent alors se présenter :

1. Si le numéro d'interruption est compris entre 15 et 128 (nombre maximum d'IRQ), notre interruption est une PPI ou une SPI. On va alors appeler la fonction `irq_process()` allant permettre d'exécuter l'ISR correspondant à l'interruption. Ensuite, on va indiquer que l'ISR a terminé sont exécution grâce au registre `GICC_EOIR` :

```
irq_process(irqnr);

gic_data->gicc_eoir = irqnr;
```

2. Si le numéro de l'IRQ est inférieur à 16, notre interruption est une SGI. Dans ce cas, on va signaler la fin du traitement de l'interruption en indiquant l'id du CPU ayant reçu l'IRQ dans le registre `GICC_EOIR` :

```
gic_data->gicc_eoir = irqstat;
```

### Ajout dans SO3
Le GIC étant totalement terminé, il faut l'ajouter dans SO3. Pour cela, nous avons ajouté un répertoire `irq` dans `devices` dans lequel le code de notre gic a été placé, ainsi qu'un nouveau *Makefile* et un nouveau *Kconfig*. Nous avons également modifié le *Makefile* et le fichier *Kconfig* dans le répertoire `devices` afin que cette configuration soit prise en compte au moment de la compilation.

**Makefile :**

Nous avons d'abord modifié le *Makefile* des devices (`so3/devices/Makefile`) :
```
obj-y = device.o irq.o timer.o serial.o

obj-y += serial/
obj-y += irq/
```

Puis, nous avons créé notre *Makefile* dans le sous-répertoire `irq` (`so3/devices/irq/Makefile`) et ajouté la ligne suivante :

```
obj-$(CONFIG_GIC_400) += gic.o
```

**Kconfig :**

Nous avons d'abord ajouté notre sous-menu dans le fichier `so3/devices/Kconfig` :

```

menu "Drivers"

source "devices/serial/Kconfig"
source "devices/irq/Kconfig"

endmenu

```

Puis, nous avons ajouté notre configuration dans `so3/devices/irq/Kconfig` :

```
config GIC_400
    bool "gic 400"
    default y
    depends on VEXPRESS
```

## Tests
Une fois l'implémentation du GIC terminée, il a fallu effectuer des tests. En effet, il existe deux types de groupes de la classe suivant le cours POS : les groupes développant le GIC et les groupes développant les timers. Ces fonctionnalités devaient être développées pour SO3. Le but final est de mettre en commun les projets afin de pouvoir tester le fonctionnement total de ces ajouts. Notre tâche était de développer le GIC qui permet de traiter les interruptions envoyées par les timers développés par les autres groupes.

Le fonctionnement de notre GIC a donc été testé avec un groupe (Lucas Elisei et David Truan) ayant terminé le développement de leur timer et qui ont *mergé* nos deux projets. Nous avons alors pu détecter deux erreurs dans notre implémentation :

- Erreur de calcul d'indice : la première erreur venait d'une erreur de calcul d'indice sur l'accès de registres dans l'initialisation du distributeur. Nous avons donc corrigé les valeurs du calcul.
- Erreur de *parsing* du *Device-Tree* : la deuxième erreur apparaissait lors du *parsing* du *Device-Tree*. En effet, la fonction de parsing ne supportait pas certaines informations que nous avions ajoutées dans notre noeud. Nous avons donc allégé notre *Device-Tree* afin que le *parser* puisse correctement récupérer les informations spécifiques à notre GIC.

Une fois les erreurs corrigées, notre GIC a fonctionné correctement. Nous avons donc mis notre implémentation à disposition sur la branche *Zybo* du dépôt sur *Bitbucket* afin que les autres groupes ayant développé un timer puissent effectuer des tests sur leur implémentation.

\begin{figure}[H]
         \centering
         {\includegraphics[width=15cm]{screenshots/GIC_Test.png}}
         \caption[Test d'un timer avec le GIC]{Test d'un timer avec le GIC}
 \end{figure}

# Conclusion
Cette première partie du projet du cours de portage de système d'exploitation nous a permis d'acquérir de grandes connaissances sur le portage d'U-Boot et d'un OS sur une plateforme. En effet, nous avons pu expérimenter et pratiquer des cas réels que nous pourrions être amenés à rencontrer dans le monde professionnel. Les manipulations nous ont demandé un grand travail de recherche et de compréhension de mécanismes jusque-là inconnus afin de pouvoir développer et tester nos fonctionnalités.

Le fait que nous nous soyons retrouvés face à un problème au début du projet nous a permis d'utiliser toutes les compétences acquises jusqu'à présent lors de notre cursus à la HEIG-VD, mais aussi de développer de nouvelles compétences et méthodes d'analyse d'un problème.

\newpage

# Annexes

- Zybo_u-boot_installation_v2017.1.1

# Liens

**Zybo :**

- Zybo Reference Manual :

https://www.xilinx.com/support/documentation/university/XUP%20Boards/XUPZYBO/\
documentation/ZYBO_RM_B_V6.pdf

- Zynq-7000 TRM :

https://www.xilinx.com/support/documentation/user_guides/ug585-Zynq-7000-TRM.pdf

**GIC :**

- How a GIC works :

http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dai0176c/ar01s03s01.html

- ARM GIC Architecture Specification :

https://static.docs.arm.com/ihi0048/b/IHI0048B_b_gic_architecture_specification.pdf

- Programmer's Guide for ARMv8 (chapitre 10.6) :

http://infocenter.arm.com/help/topic/com.arm.doc.den0024a/DEN0024A_v8_architecture_PG.pdf

- Linux gic.c :

http://elixir.free-electrons.com/linux/v3.3/source/arch/arm/common/gic.c
