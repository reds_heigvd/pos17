# RPI3 - POS Project

Authors: Sydney **HAUKE**, Camilo **PINEDA SERNA**  

## DISCLAIMER : nous somme passés sur overleaf (latex)

## Abstract

This file presents our journey trough POS' project with the Raspberry Pi 3 (rpi3) board.

[TOC]

------------------------------------

## Part 3 UART - GIC

### UART

so3 may have

dt : uart et gic addresses

[rpi3_uarts_official](https://www.raspberrypi.org/documentation/configuration/uart.md)

priority UART : nous avons sur la board le PL011 et un mini uart. so3/devices/serial/pl011.c

BSCdoc p175 chapitre 13 UART adresses : **serial0 = "/soc/serial@7e201000";** et serial1 = "/soc/serial@7e215040";

init par parser dt. put et get byte

so3/include/devices/serial/pl011.h

avec structure avec des ptr de fct



#### pb rencontrés

endmenu in different file than menu? il manque un retour à la ligne dan le devices/Kconfig après endmenu !

uboot ne reconnait pas l'image :weary: : nous l'avons chargée, mais lors du bootm l'error était unknown image format. Le problème était que uboot n'était pas correctement configuré. Dans le .config de uboot, nous avons retiré la ligne à props du FIT : config FTI is not set. Après, avec un make oldconfig, nous avons procédé à son activation (y a verbose, non à tout le reste). et nous avons recompilé uboot.

adresse pour l'uart : nous avons pris l'adresse du plat_data_mu_serial et non pas le pl011. Nous étions partis sur l'uart0 et non l'uart1. fichier rpi.c des sources de uboo2017 /board/raspbrrypi/rpi/rpi.c.

choix entre MINI uart (à implémenter, du coup) ou bien changer la fonction des GPIO et passer de ALT0 à ALT5 pour utiliser le pl011x. Nous allons implémenter le miniuart.

### gic

RPI3 : nous avons un ARM Cortex A53, The Cortex-A53 processor implements the Generic Interrupt Controller (GIC) v4 architecture. The Cortex-A53 processor includes only the GIC CPU Interface. See the ARM® Generic Interrupt Controller Architecture Specification. [cortex a53 page 15](http://infocenter.arm.com/help/topic/com.arm.doc.ddi0500e/DDI0500E_cortex_a53_r0p3_trm.pdf)



as described in the Generic Interrupt Controller (**GICv4**) architecture. (http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0500g/BABEBAAI.html) 

recherche sur linux / raspbian

le GIC est un device, il est initialisé par le parsing du dt.





-------------------------

## Part 2 - Timer

In this part we study and develop the necessary code for a timer's implementation. 
The timer we create is a **oneshot timer**, used by SO3's kernel. We will interact with the  Generic Interrupt Controller (GIC) group.

We studyied the SO3 files and structure for the periodic timers. At the same time, we studyied the SP804 timer datasheet in order to use one in vExpress emulated environement. 

The goal of this part is to develop an app programming a high resolution timer of microseconds, and to adapt the device tree accordingly.


choix des timers, dossier devices/timer/codeici

### SO3 time
SO3 architecture is inspired by the _standard_ linux files organisation, with `arch`, `drivers`, `fs`, `kernel` and `mm`. 
We do not have `drivers` but we have the `devices` folder instead.  
Timers beeing (hardware) devices, we find their implementation in the `device` files.  

SO3/devices/timer.h defines 3 kinds of timers : periodic, oneshot and source. 
The oneshot timer structure contains an associated device and a function set_deadline. 



### SP804
In our BCM2835 we have a similar timer, the AP804. It only has one timer running in continuous mode : a periodic mode with a setup reg. The reg is set with a START value and the timer decrements. It then cycles over to the START value. This is a periodic timer where the period is configurable. (FYI the free mode runs from MAX to MIN and cycles back, no settings).  
We do not have a base address, because the manufacturer chooses where to place it. In our case, (BCM..) it's given in the BCM doc p. 196 (section 14 : timers).  



TIMER_RATE de 1MHz : frequence des ticks

timer p29



1) config irq ?

2) gic need : lance cette ISR quand telle IRQ est levée (numéro)

Nous avons (p30 schéma irq)





-----------------------------------------------------------------------

## Part 1 - "Hello World" command

In this part we added a global command for U-Boot. This command name is __hello__ and prints the "Hello World!" message on the terminal. The command must be global, wich means it should be available on several platforms (at least vExpress and the rpi3).  

We proceeded in two steps : 1) we added the command for vExpress, then checked it in qemu's vExpress emulation. 2) Then we repeated the procedure for u-boot.2016 and chceked it on our rpi3 board.

In both cases, our implementation of the hello command uses U-Boot's **puts** command and expects no arguments.

### New commands in U-boot

Adding a command in U-Boot is documented in the `u-boot-2016.09/doc/README.commands` file. We added the source code where others global commands are defined. For vExpress in ` u-boot-vexpress/common/cmd_hello.c`, and for U-boot.2016 in `u-boot-2016.09/cmd/hello.c`

Here is the command's code in both cases.

```c
#include <common.h>
#include <command.h>

static int do_hello(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
	puts("Hello World!\n");

	return 0;
}

U_BOOT_CMD(
		hello, 1, 0, do_hello,
		"prints \"Hello World!\" to the console",
		""
		);

```

### vExpress new command

For vExpress, the source code is in the `u-boot-vexpress/common/cmd_hello.c` file. We also updated the `u-boot-vexpress/common/Makefile` adding the `obj-y += cmd_hello.o` line and we rebuild u-boot-vexpress

```bash
pos17 $ cd u-boot-vexpress
u-boot-vexpress $ make vexpress_ca15_tc2_qemu_config
u-boot-vexpress $ make -j8
```

We could then test the command in qemu's vExpress.

```bash
pos17 $ ./stv 
qemu-system-arm: -tftp .: The -tftp option is deprecated. Please use '-netdev user,tftp=...' instead.
audio: Could not init `oss' audio driver


U-Boot 2014.04 (Nov 14 2017 - 14:15:56)

DRAM:  1 GiB
WARNING: Caches not enabled
Flash: 128 MiB
MMC:   MMC: 0
*** Warning - bad CRC, using default environment

In:    serial
Out:   serial
Err:   serial
Net:   smc911x-0
Warning: smc911x-0 using MAC address from net device

Hit any key to stop autoboot:  0
VExpress# hello
Hello,World!
VExpress# help
[...]
hello   - print hello world
[...]
VExpress# <ctrl+a, x>
pos17 $ 
```



### U-boot.2016 new command

For U-boot, global commands sources are located in the `u-boot-2016.09/cmd/` folder. Our command source file is named `hello.c`.


To finish adding the new command, we did not forget to update the `u-boot-2016.09/cmd/Makefile` file to add our dependency `hello.o` in the `obj-y` target and then we rebuild U-Boot with our board's prefix.
```bash
u-boot-2016.09 $ make rpi_3_32b_defconfig
```

Then we uploaded the binary into the microSD card (SD) by following the instructions given in the assignement.

1. Formatting of the card (unmounted) with the introduction lab assignement.

2. copy of our files on the card

   The following files are necessary : 

   - The bootlader we just build
   - a `config.txt` with the line `enable_uart=1`

3. Fortmater la carte SD: nous reprenons la démarche du labo d'intro.
    * la carte SD doit être __démontée__ !  
    * initialiser la table de partition  
    * formater : `sudo mkfs.vfat -F 32 /dev/mmcblk0p1`  


2. Copier nos fichiers sur la carte:  
- le bootloader U-boot que l'on vient de compiler _u-boot.bin_ qui doit être rennomé en `kernel.img`  
- le fichier de configuration lu par U-boot. Active l'UART. crée par nous `config.txt` avec `enable_uart=1`.  
- le contenu de firmware/ fourni.  TODO: kesako bootcode.bin  

3. mettre la carte SD dans la rpi3

4. ecoute et Connection sur la carte :-)  
* par picocom  

## Partie 3

driver une pin GPIO pour faire un train d'impulsion.  
Grâce à la config de u-boot, une commande existe déjà.  
`do gpio toggle GPIO26; sleep 1; done`  

Pour le programme avec une fréquence donneé, nous avons voulu des fonctionalités générique. 
Il aurait été plus simple de coder directement une seule GPIO.  
Nous voulions utiliser do_gpio des commandes commons de u-boot, 
mais elle est static et n'existe que localement.

Nous pouvons aussi enlever le static :-)  
Les adresses peutvent être trouvées dans [les spécifications du Soc BCM2837](https://ultibo.org/wiki/Unit_BCM2837):  
```c
/* BCM2837 peripherals BCM2837_PERIPHERALS_* , See: BCM2835-ARM-Peripherals.pdf */ 
BCM2837_PERIPHERALS_BASE = $3F000000;  /* Mapped to VC address 7E000000  */
/* BCM2837 GPIO BCM2837_GPIO_* , see section 6 */
BCM2837_GPIO_REGS_BASE = BCM2837_PERIPHERALS_BASE + $200000;
```
Notons que ceci sont les registres de base et non pas directement les pins GPIO.  
Il faut encore configurer les pins et écrire à la bonne adresse. 
Pour cela nous allons nous inspirer du code de do_gpio.  

[adresse et registres pour set et clear des bits](http://www.codeguru.com/IoT/raspberry-pi-3-hardware-and-system-software-reference.html)  



