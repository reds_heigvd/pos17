POS-A: Parallella project
==================================================

## Author
Lastname                  | Firstname            
------------------------- | ---------------------
Baeriswyl                 | Julien

## About [`Parallella`](https://www.parallella.org)

[`Parallella`](https://www.parallella.org) front  |  [`Parallella`](https://www.parallella.org) back
:------------------------------------------------:|:-------------------------------------------------:
![front](./img/parallella_front.png)              |  ![back](./img/parallella_back.png)

### Resources
#### Official
[Website](https://www.parallella.org)  
[Constructor](http://www.adapteva.com)  
[Project](https://github.com/parallella)  
[`Parallella U-Boot`](https://github.com/parallella/parallella-uboot)  
[`Parabuntu`](https://github.com/parallella/parabuntu)  
[Dev](http://www.parallella.org/Developers/)  

##### Manuals
[Manual](http://www.parallella.org/docs/parallella_manual.pdf)  
[Schematic](http://parallella.org/docs/parallella_schematic.pdf)  

##### Community
[Wiki](https://elinux.org/Parallella)  
[Forum](https://parallella.org/forums/)  

##### Whitepapers
[Building `Linux`](http://www.adapteva.com/white-papers/building-linux-for-parallella-platform/)  
[Platform design](http://www.adapteva.com/white-papers/parallella-platform-reference-design/)  

#### Unofficial
[Quick Start Guide](https://media.readthedocs.org/pdf/parallella-linux-quickstart-guide/latest/parallella-linux-quickstart-guide.pdf)  
[Coduin on `Github`](https://github.com/coduin)  
[saturn.ffzg.hr](https://saturn.ffzg.hr/rot13/index.cgi?action=display_html;page_name=parallella)  

## About [`Epiphany`](http://www.adapteva.com/products/e16g301/)
`Epiphany` is a kind of co-processor dedicated to parallel computing.  
This co-processor possess its own Instruction Architecture Set (ISA).  

### Resources
#### Official
[Constructor](http://www.adapteva.com)  
[Project](https://github.com/adapteva)  

##### Manuals
[`Epiphany` ISA](http://www.adapteva.com/docs/epiphany_arch_ref.pdf)  
[`Epiphany` SDK](http://adapteva.com/docs/epiphany_sdk_ref.pdf)  
[`Epiphany` Datasheet](http://adapteva.com/docs/e16g301_datasheet.pdf)  

## About POS softwares
[`U-Boot`](https://www.denx.de/wiki/U-Boot)  

### Prerequisites for developpers

1. Toolchains for _ARM_ and _Xilinx_ hybrid-processors like `Zynq`  

    + Through _Xilinx_ `SDK` or `Vivado`  
    + With [Sourcery CodeBench](https://www.mentor.com/embedded-software/sourcery-tools/sourcery-codebench/editions/lite-edition/)  

2. _Xilinx_ `Vivado` to generate `FSBL`  

3. U-Boot package `u-boot-tools` to generate `Linux` kernel image  

    + Install through package manager (e.g: APT)  

### `Parallella` behavior
#### Boot
Boot code (First Stage BootLoader, Second Phase Loader) is located in flash,  
accessible through QSPI.  

Boot sequence is:  

1. FSBL
2. U-Boot
3. Look for OS in SD card

##### FSBL
[Build](http://www.wiki.xilinx.com/Build+FSBL)

##### U-Boot
U-Boot for `Parallella` or [`Parallella U-Boot`](https://github.com/parallella/parallella-uboot) is a patched 2012.10 version.  
Unfortunately, configuration has not been adapted to 2016.09 version.  
Of course U-Boot 2016.09 support Zynq, but not specifically `Parallella`.  

### Classic manipulations (with UNIX-Like system as host, OSX excluded)
#### Prepare SD card with [`Parabuntu`](https://github.com/parallella/parabuntu)

1. Download image (z7020, headless, with _Epiphany_ support)  
2. Find SD card device (e.g: `/dev/mmcblk0` or `/dev/sd<X>` if we use USB-adapter)  
    + Tips: use `lsblk` or `df -h` command in _terminal_ to list block devices  
3. In `Bash` _terminal_, type (without lines starting with `#`):  
```bash
# if device has mounted partitions
umount /dev/<devname><partition>

# go to folder containing img.gz archive
cd <directory>

# extract IMG from gzip compressed archive
gunzip -k parabuntu-<something>-z7020.img.gz

# copy decompressed img to sd card
dd if=parabuntu-<something>-z7020.img of=/dev/<devname> bs=4M
```

#### In [`U-Boot`](https://www.denx.de/wiki/U-Boot) - Load file from SD card
```bash
fatls mmc 0
fatload mmc 0 <memaddr> <file>
```

## Step 1 - [`U-Boot`](https://www.denx.de/wiki/U-Boot) (deadline: 2017.10.25)
### About [`U-Boot`](https://www.denx.de/wiki/U-Boot)

### Part 1 - [`U-Boot`](../../u-boot-vexpress) command `hello` in _VExpress_
In this part, [`U-Boot`](../../u-boot-vexpress) version is 2014.04.  
This means to add our command in U-Boot, subfolder `common` is our base of work.  

First we create new C source file.
Inside, we have to use following includes:  
```c
#include <common.h>
#include <command.h>
```

Command algorithm has to be a function, with following declaration model:  
```c
static int do_<commandname> (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
```

Then, command should instantiated with help of macro `U_BOOT_CMD` from `command.h`.  
This macro allow to link function with U-Boot command table, and specify few options.  
Macro fields are:  
```c
U_BOOT_CMD(_name, _maxargs, _rep, _cmd, _usage, _help)
```

Note that `_maxargs` should be > 0.  

Finally, `Makefile` in `common` should be modified to add object file target.  
For instance, if source file is `cmd_hello.c`, then `Makefile` is modified like:  
```make
obj-y += cmd_hello.o
```

#### Tests on [`QEMU`](https://www.qemu.org/)
Start [`QEMU`](https://www.qemu.org/) and stop `U-Boot` before boot |  Execute `hello` command
:------------------------------------------------:|:-----------------------------------------------------:
![Loading U-Boot](./img/step1_vexpress_qemu.png)  |  ![Executing `hello`](./img/step1_vexpress_hello.png)

### Part 2 - deploy [`U-Boot`](https://github.com/parallella/parallella-uboot)
As explained before, [`U-Boot 2016.09`](../../u-boot-2016.09) does not possess specific configuration for `Parallella`.  
Moreover, porting configuration would be as mess, because [`Parallella U-Boot`](https://github.com/parallella/parallella-uboot) is an old version, before 2014.  

Therefore, adding U-Boot command should directly done in [`Parallella U-Boot`](https://github.com/parallella/parallella-uboot), by using same method as in [_U-Boot for vExpress_](../../u-boot-vexpress).  

_Note that compiling could be done with_ `arm-<something>-<prefix>eabi-` _toolchains._  
_In current case, I used_ `arm-xilinx-none-eabi-`.  

Then, here goes next issue, we have to run our modified U-Boot on `Parallella`.  
To do so, 2 method came in mind.

#### Method 1

+ Through overriding boot code
    + Erase flash with JTAG / U-Boot / Linux ([`Parabuntu`](https://github.com/parallella/parabuntu) image z7020)
    + And write our bootimage to flash

This method can be troublesome, because a failure of new boot code would constraint us to use JTAG.  
JTAG use specific device, not included within `Parallella` board.  

#### Method 2 (recommended)

Use onboard U-Boot to launch our modified U-Boot.  
To proceed, we need to load our modified `u-boot.bin` on `Parallella`, from SD card or over network.  

From SD card, `fatload` command is used.  

Over network, we have to set _MAC_ and _IP_ addresses, then use `tftp`.  

Launching our loaded binary is done with `go` command.  

_Note that_ `u-boot.bin` _is a flat binary, meaning compiler can used absolute addresses._  
_It may imply to change U-Boot base address in configuration._

#### Test of method 2
I put my command in [`Parallella U-Boot`](https://github.com/parallella/parallella-uboot).  
I changed linker script `arch/arm/cpu/uboot.lds` in order to be able to load U-Boot at address `0x4000000`.  
This address is known to be in RAM, far away from U-Boot on board.  

After compiling U-Boot with toolchain `arm-xilinx-none-eabi-`, I put `u-boot.bin` on SD card formatted in VFAT.  

Here goes results:  

Load and launch `U-Boot` from `U-Boot`                |  Execute `hello` command
:----------------------------------------------------:|:-------------------------------------------------------:
![Loading U-Boot](./img/step1_parallella_deploy.png)  |  ![Executing `hello`](./img/step1_parallella_hello.png)

#### Just in case

_I made a dump of SQPI flash, through device `mtdblockX` from_ [`Parabuntu`](https://github.com/parallella/parabuntu)_,_  
_because onboard U-Boot fail on write in SD card._  
_See project_ [`Parallella-flash`](https://github.com/parallella/parallella-flash)_, script `linux/linux-flash.sh` where I found commands._  

### Part 3 - access `Epiphany` extension
`Epiphany` possess its own ISA, meaning instructions are different than from ARM processors.  
It means another compiler, specific to `Epiphany` should be used to translate C to `Epiphany` assembly to _opcode_.  

Moreover, some `Epiphany` library is available, but pratical use cases are only in user-space.  

In order to do something on `Epiphany` core(s) in bare metal world, I have to:

+ Install SDK with appropriate toolchain.
+ Write some task for `Epiphany`.
+ Write code to manage `Epiphany` core.
+ Compile it separately from ARM code.

Here comes the issues:  

+ _How compiler know which C code is for `Epiphany`, when mixed with other codes ?_  
+ _To be sure to succeed, should I write some `Epiphany` assembly ?_  
+ _If lib is not available in bare metal mode, do I have to rewrite driver to control `Epiphany` from `Zynq` ?_

#### Method

#### Test

## Step 2 - Generic Interrupt Controller (GIC) emulation (deadline: 2017.11.08)
In this step, [`QEMU`](https://www.qemu.org/) and `vExpress` will be used to developp `GIC` driver.  

### About `GIC`
### Implement driver
As help, we use specification of `GIC-400`.  
Driver is inspired from [`Linux-3.18`](../../linux-3.18) sources.  

### Tests
#### Device tree parsing
![Device tree parsed](./img/step2_devicetree.png)  

#### IRQ working with timers
![IRQ working](./img/step2_irqok.png)  
