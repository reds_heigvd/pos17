#!/bin/bash
# 

boot_img=boot.img
config=socfpga_cyclone5_defconfig

echo -n "Configuring build for Altera Cyclone 5..."
make $config > /dev/null
echo " done."

echo -n "Building U-Boot..."
make -j4 > /dev/null
echo " done."

read -p "Deploy U-Boot to a device (Y/N)? " choice
if [ "$choice" == "y" ] || [ "$choice" == "Y" ]; then
	read -p "Device path? " device
	if [ -e "$device" ]; then
		echo -n "Creating bootable image..."
		cat preloader-mkpimage.bin u-boot.img > $boot_img
		echo " done."
		if [ ! -e "$boot_img" ] || [ ! -s "$boot_img" ]; then
			echo "An error occured while creating $boot_img. Quitting..."
			exit 1
		fi
		echo -n "Deploying to $device..."
		sudo dd of=$device bs=512 if=$boot_img status=none
		sync
		echo " done."
	else
		echo "$device: Device not found."
		exit 1
	fi
fi
