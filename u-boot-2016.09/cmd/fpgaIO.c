/*
 * U-Boot command that allows to read the state of one push button on the FPGA.
 * This command also allows to turn on one LED on the FPGA.
 *
 * File    : fpgaIO.c
 * Date    : 22.10.2017
 *
 * Authors : Luca Sivillica & Gaëtan Othenin Girard
 */

#include <common.h>
#include <command.h>

typedef __u8 		     u8;
typedef volatile __u32   vu32;

#define FPGA_AXI_GPIO0_DATA_LEDS *(vu32 *) 0x41210000 // FPGA register of LEDs
#define FPGA_AXI_GPIO0_DATA_BTNS *(vu32 *) 0x41200000 // FPGA register of push buttons

#define LED_ON_PARAM					   "led-on"
#define BTN_STATE_PARAM					   "btn-state"

#define IO_NUMBER                          '4'

#define CMD_EXCPECTED_ARGS				   3		  // the name of the command is included

//----------------------------------------------------------------------------------------------
// Function prototypes

/**
 * This function prints a message that give informations about errors of the command.
 */
static void printErrorMessage(void);

/**
 * This function allows to read the state of a push button.
 *
 * @param buttonNumber number of the push button (counting from 0).
 *
 * @return 1 if the push button is pressed, otherwise 0
 */
static u8 readButton(u8 buttonNumber);

/**
 * This function allows to set the state of a led.
 *
 * @param ledNumber number of the led (counting from 0).
 */
static void  setLed(u8 ledNumber);

/**
 * This function allows to get a number (integer between 0 and 9)
 * from a character that represents this number.
 *
 * @param number the character representing the number
 *
 * @return the number represented by the character
 */
static u8 getNumberFromChar(char number);

//----------------------------------------------------------------------------------------------
// Main function of the command
static int do_fpgaIO(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
	u8 ioNumber;

	/* checking number of parameters */
	if (argc != CMD_EXCPECTED_ARGS) {
		printErrorMessage();

		return 1;
	}

	/* checking the validity of the last parameter */
	if (strlen(argv[2]) == 1 && argv[2][0] >= '0' && argv[2][0] < IO_NUMBER) {

		ioNumber = getNumberFromChar(argv[2][0]);

		/* the user want to turn on a led */
		if (strcmp(argv[1], LED_ON_PARAM) == 0) {
			setLed(ioNumber);

		/* the user want to read the state of a push button */
		} else if (strcmp(argv[1], BTN_STATE_PARAM) == 0) {
			printf("State: %d\n", readButton(ioNumber));

		/* the user gave a wrong parameter */
		} else {
			printErrorMessage();

			return 1;
		}

	/* the last parameter isn't valid */
	} else {
		printErrorMessage();

		return 1;
	}

	return 0;
}

//----------------------------------------------------------------------------------------------
void printErrorMessage(void) {
	printf("Parameters error:\n"
		   "\n"
		   "Usage: fpgaIO led-on | btn-state <IO number>\n"
		   "       IO number must be between 0 and %c\n",
		   IO_NUMBER - 1);
}

//----------------------------------------------------------------------------------------------
u8 readButton(u8 buttonNumber)  {
	return FPGA_AXI_GPIO0_DATA_BTNS & (1 << buttonNumber) ? 1 : 0;
}

//----------------------------------------------------------------------------------------------
void  setLed(u8 ledNumber) {
	FPGA_AXI_GPIO0_DATA_LEDS = (1 << ledNumber);
}

//----------------------------------------------------------------------------------------------
u8 getNumberFromChar(char number) {
	if (number < '0' || number > '9') {
		return -1;
	}

	return (u8) (number - '0');
}

/**
 * Create the command in the command table of U-Boot.
 */
U_BOOT_CMD(
	fpgaIO,	CMD_EXCPECTED_ARGS,	0,	do_fpgaIO,
	"allows to read the state of one push button and turn on one led on the FPGA.",
	" led-on | btn-state <IO number>\n"
	"led operation:\n"
	"      - led-on    <led number>   turn on a led\n"
	"push button operation:\n"
	"      - btn-state <btn number>   read the state of a button: 1 if is activated, otherwise 0"
);
