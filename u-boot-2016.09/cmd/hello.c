/*
 * U-Boot command that prints "hello world" on the console.
 *
 * File    : hello.c
 * Date    : 05.10.2017
 *
 * Authors : Luca Sivillica & Gaëtan Othenin Girard
 */

#include <common.h>
#include <command.h>

static int do_hello(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
	printf("Hello world !\n");

	return 0;
}

U_BOOT_CMD(
	hello,	1,	0,	do_hello,
	"print \"hello world\" on the console",
	"\n"
	"      - print \"hello world\" on the console"
);
