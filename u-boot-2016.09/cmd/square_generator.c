/*
 * square_generator.c : a utility program that generates a square signal on
 * GPIO26.
 *
 * Authors: Sydney Hauke, Camilo Pineda Serna
 * Created on: 2017-10-12
 */

#include <common.h>
#include <command.h>
#include <asm/gpio.h>
#include <timer.h>
#include <asm/arch/gpio.h>

#define NUM_ARGS 2

#define GPSET0 	((volatile unsigned int*)(BCM2835_GPIO_BASE + 0x0000001cUL)) // Bank 0 set
#define GPCLR0 	((volatile unsigned int*)(BCM2835_GPIO_BASE + 0x00000028UL)) // Bank 0 clear
#define GPFSEL2 ((volatile unsigned int*)(BCM2835_GPIO_BASE + 0x00000008UL)) // function select register 2 for gpio26
#define GPIO26_MASK (0x1 << 26) // Bit position for gpio 26
#define FSEL2_OFFSET (18) // Bits 20-18 are used to set the function selection of gpio26

/* deux banks de GPIO car chaque bank = 32 GPIO et il y en a 54 */

static int do_square_generator_gpio(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
    if(argc < NUM_ARGS) {
        return CMD_RET_USAGE;
    }

    unsigned long freq = simple_strtoul(argv[1], NULL, 10); // Convert from a base 10 number string

    if(freq == 0) {
    	puts("Error: couldn't parse value for frequency or zero frequency\n");
    	return CMD_RET_FAILURE;
    }

    unsigned long halfPeriod = (1000000 / freq) / 2; // Convert delay to microseconds;

    printf("Pulsating a square signal at %lu Hz on GPIO26...\n", freq);

    *GPFSEL2 |= (0x1 << FSEL2_OFFSET); // Set gpio26 as output

    while(1) {
    	*GPSET0 = GPIO26_MASK;
        udelay(halfPeriod);
        *GPCLR0 = GPIO26_MASK;
        udelay(halfPeriod);
    }

    return 0;
}

U_BOOT_CMD(square_generator, NUM_ARGS, 0, do_square_generator_gpio,
        "Generates a square signal and outputs on the GPIO 26",
        "<freq> in Hertz");
