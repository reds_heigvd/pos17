/*
 * fpga_leds.c
 *
 *  Created on: Oct 12, 2017
 *      Author: reds
 */
#include <common.h>
#include <command.h>
#include <stdlib.h>

#define AXI_BRIDGE	*(volatile unsigned short *)0xFF200000

/*
 * Read or write in the FPGA LEDs register.
 */
static int do_fpgaleds(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	if (argc != 2 && argc != 3) {
		printf("Usage: %s <mode> [<pattern>]\n", argv[0]);
		printf("\t- <mode> is 'r' or 'w'\n");
		printf("\t- <pattern> is the pattern to display (hex - only in w mode)\n");

		return 1;
	}

	const char *mode = argv[1];

	if (!strncmp(mode, "r", 1)) {
		printf("Leds pattern: 0x%03X\n", AXI_BRIDGE);
	} else if (!strncmp(mode, "w", 1)) {
		unsigned short pattern = simple_strtoul(argv[2], NULL, 16);

		AXI_BRIDGE = pattern;
	} else {
		printf("ERROR: Unexpected mode: %s\n", mode);

		return 1;
	}

	return 0;
}

/*
 * Create a cmd_tbl_t using U_BOOT_CMD macro.
 */
U_BOOT_CMD(
	fpgaleds,	3,	1,	do_fpgaleds,
	"read or write in the FPGA LEDs register.",
	"<mode> [<pattern>]\n"
	"\t- <mode> is 'r' or 'w'\n"
	"\t- <pattern> is the pattern to display (hex - only in w mode)"
);
