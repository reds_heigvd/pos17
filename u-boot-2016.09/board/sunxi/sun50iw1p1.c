/*
 * (C) Copyright 2007-2015
 * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
 * Jerry Wang <wangflord@allwinnertech.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * SPDX-License-Identifier:	GPL-2.0
 */

#include <common.h>
#include <sunxi_mbr.h>
#include <boot_type.h>
#include <sys_partition.h>
#include <sys_config.h>
#include <mmc.h>
#include <power/sunxi/axp.h>
#include <asm/io.h>
#include <power/sunxi/pmu.h>
#include <asm/arch/dram.h>
#include <asm/arch/clock.h>

DECLARE_GLOBAL_DATA_PTR;

#if 0
typedef struct _boot_dram_para_t
{
	//normal configuration
	unsigned int dram_clk;
	unsigned int dram_type;//dram_type	DDR2: 2		DDR3: 3	   LPDDR2: 6   DDR3L: 31
	unsigned int dram_zq;
	unsigned int dram_odt_en;

	//control configuration
	unsigned int dram_para1;
	unsigned int dram_para2;

	//timing configuration
	unsigned int dram_mr0;
	unsigned int dram_mr1;
	unsigned int dram_mr2;
	unsigned int dram_mr3;
	unsigned int dram_tpr0;
	unsigned int dram_tpr1;
	unsigned int dram_tpr2;
	unsigned int dram_tpr3;
	unsigned int dram_tpr4;
	unsigned int dram_tpr5;
	unsigned int dram_tpr6;

	//reserved for future use
	unsigned int dram_tpr7;
	unsigned int dram_tpr8;
	unsigned int dram_tpr9;
	unsigned int dram_tpr10;
	unsigned int dram_tpr11;
	unsigned int dram_tpr12;
	unsigned int dram_tpr13;

}__dram_para_t;

#endif

/* Currently not used - just for testings... */
#if 0
__dram_para_t dram_para = {
		.dram_clk = 624,
		.dram_type = 7,    //0x04 -> auto 7
		.dram_zq = 0x3939b9,      //0x08 -> auto 3939b9
		.dram_odt_en = 0xc,            //0x0c

		.dram_para1 = 0xb000b0,     //0x10 -> auto 0xb000b0
		.dram_para2 = 0x1,         //0x14 -> auto 0x1

		.dram_mr0 = 0x0,            //0x18
		.dram_mr1 = 0x03,           //0x1c
		.dram_mr2 = 0x11,           //0x20
		.dram_mr3 = 0x2,            //0x24 -> auto 2

		.dram_tpr0 = 0x0048A258,
		.dram_tpr1 = 0x01B1B28F,
		.dram_tpr2 = 0x00047028,
		.dram_tpr3 = 0x050005dc,
		.dram_tpr4 = 0,
		.dram_tpr5 = 0,
		.dram_tpr6 = 0x40,  //0x40

		.dram_tpr7 = 0x2a066270,     //0x44 -> auto mode (lower bits)
		.dram_tpr8 = 0,             //0x48
		.dram_tpr9 = 0,             //0x4c -> auto 0 (pll setting)
		.dram_tpr10 = 0x446f, //0x0000442b,     //0x50 -> auto 0x446F
		.dram_tpr11 = 0x18634a8,//0x13860053,     //0x54 -> auto 0x18634a8
		.dram_tpr12 = 0x88770000,//0x88770000,     //0x58 -> auto 0x88770000
		.dram_tpr13 = 0x5c //0x04002904,	// 0x5c
		};
#endif




/*
 ************************************************************************************************************
 *
 *                                             function
 *
 *    name          :
 *
 *    parmeters     :
 *
 *    return        :
 *
 *    note          :
 *
 *
 ************************************************************************************************************
 */
/* add board specific code here */
int board_init(void) {
//	gd->bd->bi_arch_number = LINUX_MACHINE_ID;
//	gd->bd->bi_boot_params = (PHYS_SDRAM_1 + 0x100);
//	debug("board_init storage_type = %d\n",uboot_spare_head.boot_data.storage_type);
	u32 reg_val;
	//set sram for vedio use, default is boot use
	reg_val = readl(0x01c00004);
	reg_val &= ~(0x1 << 24);
	writel(reg_val, 0x01c00004);

	//VE gating :brom set this bit, but not require now
	reg_val = readl(0x01c20064);
	reg_val &= ~(0x1 << 0);
	writel(reg_val, 0x01c20064);

	//VE Bus Reset: brom set this bit, but not require now
	reg_val = readl(0x1c202c4);
	reg_val &= ~(0x1 << 0);
	writel(reg_val, 0x1c202c4);

	return 0;
}
/*
 ************************************************************************************************************
 *
 *                                             function
 *
 *    name          :
 *
 *    parmeters     :
 *
 *    return        :
 *
 *    note          :
 *
 *
 ************************************************************************************************************
 */
void dram_init_banksize(void) {
	gd->bd->bi_dram[0].start = PHYS_SDRAM_0;
	gd->bd->bi_dram[0].size = gd->ram_size;
}
/*
 ************************************************************************************************************
 *
 *                                             function
 *
 *    name          :
 *
 *    parmeters     :
 *
 *    return        :
 *
 *    note          :
 *
 *
 ************************************************************************************************************
 */

/* AllWinner pre-built libdram */
extern int init_DRAM(int type, __dram_para_t *buff);

int dram_init(void) {
	uint dram_size = 0;
	//dram_size = uboot_spare_head.boot_data.dram_scan_size;
	if (dram_size) {
		gd->ram_size = dram_size * 1024 * 1024;
	} else {
		gd->ram_size = get_ram_size((long *) PHYS_SDRAM_0,
				PHYS_SDRAM_0_SIZE);
	}

	gd->ram_size = PHYS_SDRAM_0_SIZE;
#if 0
	//reserve trusted dram
	if (gd->securemode == SUNXI_SECURE_MODE_WITH_SECUREOS)
	gd->ram_size -= PLAT_TRUSTED_DRAM_SIZE + 64 * 1024 * 1024;
	else
	gd->ram_size -= PLAT_TRUSTED_DRAM_SIZE;
#endif

	/*
	 * Presumed that a warm reset might touch the DRAM controller configuration, but actually seems not..
	 */
	//init_DRAM(0, (void *) &dram_para);

	printf("Re-setting PLLs...\n");
	reset_pll();


	debug("DRAM init ok\n");
	return 0;
}

#ifdef CONFIG_GENERIC_MMC

extern int sunxi_mmc_init(int sdc_no);

int board_mmc_init(bd_t *bis) {
	sunxi_mmc_init(bis->bi_card_num);

	return 0;
}

void board_mmc_pre_init(int card_num) {
	bd_t *bd;

	bd = gd->bd;
	gd->bd->bi_card_num = card_num;
	mmc_initialize(bd);

}

int board_mmc_get_num(void) {
	return gd->boot_card_num;
}

void board_mmc_set_num(int num) {
	gd->boot_card_num = num;
}

#endif

#ifdef CONFIG_DISPLAY_BOARDINFO
int checkboard(void)
{
	printf("Board: SUN6I\n");
	return 0;
}
#endif

int cpu0_set_detected_paras(void) {
	return 0;
}

ulong get_spare_head_size(void) {
	return (ulong) sizeof(struct spare_boot_head_t);
}

extern int axp81_probe(void);

/**
 * platform_axp_probe -detect the pmu on  board
 * @sunxi_axp_dev_pt: pointer to the axp array
 * @max_dev: offset of the property to retrieve
 * returns:
 *	the num of pmu
 */

int platform_axp_probe(sunxi_axp_dev_t *sunxi_axp_dev_pt[], int max_dev) {
	if (axp81_probe()) {
		printf("probe axp81X failed\n");
		sunxi_axp_dev_pt[0] = &sunxi_axp_null;
		return 0;
	}

	/* pmu type AXP81X */
	tick_printf("PMU: AXP81X found\n");
#if 0
	sunxi_axp_dev_pt[0] = &sunxi_axp_81;
#endif /* does not compile... */

	//sunxi_axp_dev_pt[PMU_TYPE_81X] = &sunxi_axp_81;
	//find one axp
	return 1;

}

/*
 * Note this function gets called multiple times.
 * It must not make any changes to env variables which already exist.
 */
static void setup_environment(const void *fdt) {
	char serial_string[17] = { 0 };
	unsigned int sid[4];
	uint8_t mac_addr[6];
	char ethaddr[16];
	int i, ret;

#if 0 /* At the moment... */
	ret = sunxi_get_sid(sid);
	if (ret == 0 && sid[0] != 0) {
		/*
		 * The single words 1 - 3 of the SID have quite a few bits
		 * which are the same on many models, so we take a crc32
		 * of all 3 words, to get a more unique value.
		 *
		 * Note we only do this on newer SoCs as we cannot change
		 * the algorithm on older SoCs since those have been using
		 * fixed mac-addresses based on only using word 3 for a
		 * long time and changing a fixed mac-address with an
		 * u-boot update is not good.
		 */
#if !defined(CONFIG_MACH_SUN4I) && !defined(CONFIG_MACH_SUN5I) && \
    !defined(CONFIG_MACH_SUN6I) && !defined(CONFIG_MACH_SUN7I) && \
    !defined(CONFIG_MACH_SUN8I_A23) && !defined(CONFIG_MACH_SUN8I_A33)
		sid[3] = crc32(0, (unsigned char *)&sid[1], 12);
#endif

		/* Ensure the NIC specific bytes of the mac are not all 0 */
		if ((sid[3] & 0xffffff) == 0)
		sid[3] |= 0x800000;

		for (i = 0; i < 4; i++) {
			sprintf(ethaddr, "ethernet%d", i);
			if (!fdt_get_alias(fdt, ethaddr))
			continue;

			if (i == 0)
			strcpy(ethaddr, "ethaddr");
			else
			sprintf(ethaddr, "eth%daddr", i);

			if (getenv(ethaddr))
			continue;

			/* Non OUI / registered MAC address */
			mac_addr[0] = (i << 4) | 0x02;
			mac_addr[1] = (sid[0] >> 0) & 0xff;
			mac_addr[2] = (sid[3] >> 24) & 0xff;
			mac_addr[3] = (sid[3] >> 16) & 0xff;
			mac_addr[4] = (sid[3] >> 8) & 0xff;
			mac_addr[5] = (sid[3] >> 0) & 0xff;

			eth_setenv_enetaddr(ethaddr, mac_addr);
		}

		if (!getenv("serial#")) {
			snprintf(serial_string, sizeof(serial_string),
					"%08x%08x", sid[0], sid[3]);

			setenv("serial#", serial_string);
		}
	}
#endif

}

int ft_board_setup(void *blob, bd_t *bd) {
	int __maybe_unused r;

	/*
	 * Call setup_environment again in case the boot fdt has
	 * ethernet aliases the u-boot copy does not have.
	 */
	setup_environment(blob);

#ifdef CONFIG_VIDEO_DT_SIMPLEFB
	r = sunxi_simplefb_setup(blob);
	if (r)
	return r;
#endif
	return 0;
}

char *board_hardware_info(void) {
	static char * hardware_info = "sun50iw1p1";
	return hardware_info;
}

#ifdef CONFIG_CMD_NET
#ifdef CONFIG_USB_ETHER
extern int sunxi_udc_probe(void);
#ifdef CONFIG_SUNXI_SERIAL
extern int get_serial_num_from_chipid(char* serial);

static int sunxi_serial_num_is_zero(char *serial)
{
	int i = 0;

	get_serial_num_from_chipid(serial);
	while(i < 20) {
		if (serial[i] != '0')
		break;
		i++;
	}
	if (i == 20)
	return 0;
	else
	return 1;
}
#endif

static void sunxi_random_ether_addr(void)
{
	int i = 0;
	char serial[128] = {0};
	ulong tmp = 0;
	char tmp_s[5] = "";
	unsigned long long rand = 0;
	uchar usb_net_addr[6];
	char mac[18] = "";
	char tmp_mac = 0;
	int ret = 0;

	/*
	 * get random mac address from serial num if it's not zero, or from timer.
	 */
#ifdef CONFIG_SUNXI_SERIAL
	ret = sunxi_serial_num_is_zero(serial);
#endif
	if (ret == 1) {
		for(i = 0; i < 6; i++) {
			if(i == 0)
			strncpy(tmp_s, serial+16, 4);
			else if ((i == 1) || (i == 4))
			strncpy(tmp_s, serial+12, 4);
			else if (i == 2)
			strncpy(tmp_s, serial+8, 4);
			else
			strncpy(tmp_s,serial+4,4);

			tmp = simple_strtoul(tmp_s, NULL, 16);
			rand = (tmp) * 0xfedf4fd;

			rand = rand * 0xd263f967 + 0xea6f22ad8235;
			usb_net_addr[i] = (uchar)(rand % 0x100);
		}
	} else {
		for(i = 0; i < 6; i++) {
			rand = get_timer_masked() * 0xfedf4fd;
			rand = rand * 0xd263f967 + 0xea6f22ad8235;
			usb_net_addr[i] = (uchar)(rand % 0x100);
		}
	}

	/*
	 * usbnet_hostaddr, usb_net_addr[0] = 0xxx xx10
	 */
	tmp_mac = usb_net_addr[0] & 0x7e;
	tmp_mac = tmp_mac | 0x02;
	sprintf(mac, "%02x:%02x:%02x:%02x:%02x:%02x", tmp_mac, usb_net_addr[1],usb_net_addr[2],
			usb_net_addr[3],usb_net_addr[4],usb_net_addr[5]);
	setenv("usbnet_hostaddr", mac);

	/*
	 * usbnet_devaddr, usb_net_addr[0] = 1xxx xx10
	 */
	tmp_mac = usb_net_addr[0] & 0xfe;
	tmp_mac = tmp_mac | 0x82;
	sprintf(mac, "%02x:%02x:%02x:%02x:%02x:%02x", tmp_mac, usb_net_addr[1],usb_net_addr[2],
			usb_net_addr[3],usb_net_addr[4],usb_net_addr[5]);
	setenv("usbnet_devaddr", mac);
}
#endif

int board_eth_init(bd_t *bis) {
	int rc = 0;

#if defined(CONFIG_USB_ETHER)
	sunxi_random_ether_addr();
	sunxi_udc_probe();
	usb_eth_initialize(bis);
#endif

	return rc;
}
#endif
