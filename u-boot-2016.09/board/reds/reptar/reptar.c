/*
 * (C) Copyright 2011
 * Logic Product Development <www.logicpd.com>
 *
 * Author :
 *	Peter Barada <peter.barada@logicpd.com>
 *
 * Derived from Beagle Board and 3430 SDP code by
 *	Richard Woodruff <r-woodruff2@ti.com>
 *	Syed Mohammed Khasim <khasim@ti.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */
#include <common.h>
#include <dm.h>
#include <ns16550.h>
#include <netdev.h>
#include <flash.h>
#include <nand.h>
#include <i2c.h>
#include <twl4030.h>
#include <asm/io.h>
#include <asm/arch/mmc_host_def.h>
#include <asm/arch/mux.h>
#include <asm/arch/mem.h>
#include <asm/arch/sys_proto.h>
#include <asm/gpio.h>
#include <asm/mach-types.h>
#include "reptar.h"

/* FIXME: debug, correct? */
#define REPTAR_GPIO_ETH_RST 137

DECLARE_GLOBAL_DATA_PTR;

/*
 * two dimensional array of strucures containining board name and Linux
 * machine IDs; row it selected based on CPU column is slected based
 * on hsusb0_data5 pin having a pulldown resistor
 */

static const struct ns16550_platdata reptar_serial = {
	OMAP34XX_UART3,
	2,
	V_NS16550_CLK
};

U_BOOT_DEVICE(reptar_uart) = {
	"ns16550_serial",
	&reptar_serial
};

/*Table containing FGPS register constants*/
static const u32 gpmc_fpga_regs[GPMC_MAX_REG] = {
    FPGA_REGS_GPMC_CONFIG1,
    FPGA_REGS_GPMC_CONFIG2,
    FPGA_REGS_GPMC_CONFIG3,
    FPGA_REGS_GPMC_CONFIG4,
    FPGA_REGS_GPMC_CONFIG5,
    FPGA_REGS_GPMC_CONFIG6,
    0 /* CONFIG7 computed automatically in enable_gpmc_cs_config() */
};



/*
 * Routine: setup_fpga_chip
 * Description: Setting up the configuration GPMC registers specific to the
 *		fpga hardware.
 */
static void setup_fpga_chip(void)
{	
	struct ctrl *ctrl_base = (struct ctrl *)OMAP34XX_CTRL_BASE;
	/* Configure GPMC registers for fpga_reg device */
	enable_gpmc_cs_config(gpmc_fpga_regs, &gpmc_cfg->cs[3], 
					CONFIG_FPGAS_BASE, CONFIG_FPGAS_GPMC_SIZE);

	/* Enable off mode for NWE in PADCONF_GPMC_NWE register */
	writew(readw(&ctrl_base->gpmc_nwe) | 0x0E00, &ctrl_base->gpmc_nwe);
	/* Enable off mode for NOE in PADCONF_GPMC_NADV_ALE register */
	writew(readw(&ctrl_base->gpmc_noe) | 0x0E00, &ctrl_base->gpmc_noe);
	/* Enable off mode for ALE in PADCONF_GPMC_NADV_ALE register */
	writew(readw(&ctrl_base->gpmc_nadv_ale) | 0x0E00,
		&ctrl_base->gpmc_nadv_ale);	
	
	/* Do some other initialization (eg make blink a led through 
		writing to a fpga reg 
	*/
}




/*
 * Routine: board_init
 * Description: Early hardware init.
 */
int board_init(void)
{
	gpmc_init(); /* in SRAM or SDRAM, finish GPMC */

	/* boot param addr */
	gd->bd->bi_boot_params = (OMAP34XX_SDRC_CS0 + 0x100);

	return 0;
}

#if defined(CONFIG_GENERIC_MMC) && !defined(CONFIG_SPL_BUILD)
int board_mmc_init(bd_t *bis)
{
	return omap_mmc_init(0, 0, 0, -1, -1);
}
#endif

#if defined(CONFIG_GENERIC_MMC)
void board_mmc_power_init(void)
{
	twl4030_power_mmc_init(0);
}
#endif

static void board_gpio_init(void)
{
/*Make LED gpio as output,Push buttons and Switches gpio as input */
	if (!gpio_request(uP_PB0, "") &&
	    !gpio_request(uP_PB1, "") &&
        !gpio_request(uP_PB2, "") &&
	    !gpio_request(uP_PB3, "") &&
        !gpio_request(uP_LED0, "") &&
	    !gpio_request(uP_LED1, "") &&
        !gpio_request(uP_LED2, "") &&
	    !gpio_request(GPIO_SW_0, "") &&
        !gpio_request(GPIO_SW_1, "") &&	    
        !gpio_request(GPIO_LED_0, "") &&
	    !gpio_request(GPIO_LED_1, "")
           )   
	{
	    gpio_direction_input(uP_PB0);
	    gpio_direction_input(uP_PB1);
	    gpio_direction_input(uP_PB2);
	    gpio_direction_input(uP_PB3);
	    gpio_direction_output(uP_LED0, 0);
	    gpio_direction_output(uP_LED1, 0);
	    gpio_direction_output(uP_LED2, 0);

	    gpio_direction_input(GPIO_SW_0);
	    gpio_direction_input(GPIO_SW_1);	    
	    gpio_direction_output(GPIO_LED_0, 0);
	    gpio_direction_output(GPIO_LED_1, 0);	    
	}

#if CONFIG_REPTAR_VERSION==1
     /* On Reptar V1 we only initialize GPIO switches and leds if wifi is not used.
      *  * (conflicting lines on same pins...). */
#ifndef CONFIG_WIFI
   if (!gpio_request(GPIO_SW_2, "") &&
     !gpio_request(GPIO_SW_3, "") &&
     !gpio_request(GPIO_SW_4, "") &&           
     !gpio_request(GPIO_LED_2, "") && 
     !gpio_request(GPIO_LED_3, "") &&
     !gpio_request(GPIO_LED_4, "") &&
     !gpio_request(GPIO_LED_5, "")
     )    
     {    
        gpio_direction_input(GPIO_SW_2);
        gpio_direction_input(GPIO_SW_3);
        gpio_direction_input(GPIO_SW_4);

        gpio_direction_output(GPIO_LED_2, 0);
        gpio_direction_output(GPIO_LED_3, 0);
        gpio_direction_output(GPIO_LED_4, 0);
        gpio_direction_output(GPIO_LED_5, 0);
    }

#endif
#elif CONFIG_REPTAR_VERSION==2
    if (!gpio_request(GPIO_SW_2, "") &&
       !gpio_request(GPIO_SW_3, "") &&
       !gpio_request(GPIO_SW_4, "") &&
       !gpio_request(GPIO_LED_2, "") &&
       !gpio_request(GPIO_LED_3, ""))
    {
        gpio_direction_input(GPIO_SW_2);
        gpio_direction_input(GPIO_SW_3);
        gpio_direction_input(GPIO_SW_4);

        gpio_direction_output(GPIO_LED_2, 0);
        gpio_direction_output(GPIO_LED_3, 0);
    }
#endif 
}

int misc_init_r(void)
{
    board_gpio_init();

    setup_fpga_chip();

#if defined(CONFIG_CMD_NET)
	setup_net_chip();
	reset_net_chip();
#endif
	return 0;
}

/*
 * IEN  - Input Enable
 * IDIS - Input Disable
 * PTD  - Pull type Down
 * PTU  - Pull type Up
 * DIS  - Pull type selection is inactive
 * EN   - Pull type selection is active
 * M0   - Mode 0
 * The commented string gives the final mux configuration for that pin
 */

/*
 * Routine: set_muxconf_regs
 * Description: Setting up the configuration Mux registers specific to the
 *		hardware. Many pins need to be moved from protect to primary
 *		mode.
 */
void set_muxconf_regs(void)
{
	MUX_EVM();
}

#ifdef CONFIG_CMD_NET

/*
 * Routine: setup_net_chip
 * Description: Setting up the configuration GPMC registers specific to the
 *		Ethernet hardware.
 */
static void setup_net_chip(void)
{
	struct ctrl *ctrl_base = (struct ctrl *)OMAP34XX_CTRL_BASE;

	/* Configure GPMC registers */
	writel(NET_GPMC_CONFIG1, &gpmc_cfg->cs[5].config1);
	writel(NET_GPMC_CONFIG2, &gpmc_cfg->cs[5].config2);
	writel(NET_GPMC_CONFIG3, &gpmc_cfg->cs[5].config3);
	writel(NET_GPMC_CONFIG4, &gpmc_cfg->cs[5].config4);
	writel(NET_GPMC_CONFIG5, &gpmc_cfg->cs[5].config5);
	writel(NET_GPMC_CONFIG6, &gpmc_cfg->cs[5].config6);
	writel(NET_GPMC_CONFIG7, &gpmc_cfg->cs[5].config7);

	/* Enable off mode for NWE in PADCONF_GPMC_NWE register */
	writew(readw(&ctrl_base ->gpmc_nwe) | 0x0E00, &ctrl_base->gpmc_nwe);
	/* Enable off mode for NOE in PADCONF_GPMC_NADV_ALE register */
	writew(readw(&ctrl_base->gpmc_noe) | 0x0E00, &ctrl_base->gpmc_noe);
	/* Enable off mode for ALE in PADCONF_GPMC_NADV_ALE register */
	writew(readw(&ctrl_base->gpmc_nadv_ale) | 0x0E00,
		&ctrl_base->gpmc_nadv_ale);
}

/**
 * Reset the ethernet chip.
 */
static void reset_net_chip(void)
{
	int ret;
	int rst_gpio;

        rst_gpio = REPTAR_GPIO_ETH_RST;

	ret = gpio_request(rst_gpio, "");
	if (ret < 0) {
		printf("Unable to get GPIO %d\n", rst_gpio);
		return ;
	}

	/* Configure as output */
	gpio_direction_output(rst_gpio, 0);

	/* Send a pulse on the GPIO pin */
	gpio_set_value(rst_gpio, 1);
	udelay(1);
	gpio_set_value(rst_gpio, 0);
	udelay(1);
	gpio_set_value(rst_gpio, 1);
}




int board_eth_init(bd_t *bis)
{
	int rc = 0;

#ifdef CONFIG_SMC911X
#define STR_ENV_ETHADDR "ethaddr"

        struct eth_device *dev;
        uchar eth_addr[6];

        rc = smc911x_initialize(0, CONFIG_SMC911X_BASE);

        if (!eth_getenv_enetaddr(STR_ENV_ETHADDR, eth_addr)) {
                dev = eth_get_dev_by_index(0);
                if (dev) {
                        eth_setenv_enetaddr(STR_ENV_ETHADDR, dev->enetaddr);
                } else {
                        printf("omap3evm: Couldn't get eth device\n");
                        rc = -1;
                }
        }
#endif
	return rc;
}
#endif /* CONFIG_CMD_NET */
