/*
 * Configuration settings for the Allwinner A64 (sun50i) CPU
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/*
 * A64 specific configuration
 */

#define CONFIG_TARGET_NAME      merida

#define CONFIG_SUNXI_USB_PHYS	1

#define CONFIG_AXP_USE_RSB

/* *** Used by SPL *** */

#define CONFIG_SYS_SRAM_BASE             (0x10000)

#define BOOT_PUB_HEAD_VERSION           "1100"
#define CONFIG_BOOT0_RET_ADDR            (CONFIG_SYS_SRAM_BASE)
#define CONFIG_BOOT0_RUN_ADDR            (0x10000)
#define SUNXI_DRAM_PARA_MAX              32

#define CONFIG_FES1_RET_ADDR             (CONFIG_SYS_SRAM_BASE + 0x7210)

#define DRAM_PARA_STORE_ADDR             (CONFIG_SYS_SDRAM_BASE + 0x00800000)

#define CONFIG_BOOTPKG_STORE_IN_DRAM_BASE   (CONFIG_SYS_SDRAM_BASE + 0x2e00000)

#define SUNXI_RCPUCFG_BASE		         (0x01f01C00L)

#define CONFIG_SYS_SRAMA2_BASE           (0x40000)
#define CONFIG_SYS_SRAMA2_SIZE           (0x14000)

#define PLAT_SDRAM_BASE                      0x40000000
//trusted dram area
#define PLAT_TRUSTED_DRAM_BASE             (PLAT_SDRAM_BASE)
#define PLAT_TRUSTED_DRAM_SIZE             0x01000000

#define ELMGR_BASE			  (0x44000)
#define ELMGR_SIZE                        (0x100000)  //1M

#define SCP_SRAM_BASE                        (CONFIG_SYS_SRAMA2_BASE)
#define SCP_SRAM_SIZE                        (CONFIG_SYS_SRAMA2_SIZE)
#define SCP_DRAM_BASE                        (PLAT_TRUSTED_DRAM_BASE + ELMGR_SIZE)
#define SCP_DRAM_SIZE                        (0x4000)

#define SUNXI_RPRCM_BASE			 (0x01f01400L)

#define FEL_BASE                         0x20

/* Invoke last_stage_init function */
#define CONFIG_LAST_STAGE_INIT		1

/* *** */


#define UBOOT_VERSION		    "3.0.0"
#define UBOOT_PLATFORM		    "1.0.0"

#define COUNTER_FREQUENCY	CONFIG_TIMER_CLK_FREQ
#define GICD_BASE		0x1c81000
#define GICC_BASE		0x1c82000

#define ELMGR_BASE			  (0x44000)
#define ELMGR_SIZE                        (0x100000)  //1M

#define SUNXI_CPUX_CFG_BASE_A32           (0x01700000)
#define RVBARADDR0_L		             (SUNXI_CPUX_CFG_BASE_A32+0xA0)
#define RVBARADDR0_H		             (SUNXI_CPUX_CFG_BASE_A32+0xA4)
#define RVBARADDR1_L		             (SUNXI_CPUX_CFG_BASE_A32+0xA8)
#define RVBARADDR1_H		             (SUNXI_CPUX_CFG_BASE_A32+0xAC)
#define RVBARADDR2_L		             (SUNXI_CPUX_CFG_BASE_A32+0xB0)
#define RVBARADDR2_H		             (SUNXI_CPUX_CFG_BASE_A32+0xB4)
#define RVBARADDR3_L		             (SUNXI_CPUX_CFG_BASE_A32+0xB8)
#define RVBARADDR3_H		             (SUNXI_CPUX_CFG_BASE_A32+0xBC)

/*
 * Include common sunxi configuration where most the settings are
 */
#include <configs/sunxi-common.h>

#endif /* __CONFIG_H */
