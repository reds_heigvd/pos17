/*
************************************************************************************************************************
*                                                         eGON
*                                         the Embedded GO-ON Bootloader System
*
*                             Copyright(C), 2006-2008, SoftWinners Microelectronic Co., Ltd.
*											       All Rights Reserved
*
* File Name : Boot0_head.c
*
* Author : Gary.Wang
*
* Version : 1.1.0
*
* Date : 2007.11.06
*
* Description : This file defines the file head part of Boot0, which contains some important
*             infomations such as magic, platform infomation and so on, and MUST be allocted in the
*             head of Boot0.
*
* Others : None at present.
*
*
* History :
*
*  <Author>        <time>       <version>      <description>
*
* Gary.Wang       2007.11.06      1.1.0        build the file
*
************************************************************************************************************************
*/
#include <configs/sun50i.h>
#include "common.h"
#include <private_boot0.h>

//#define MERIDA
#define PINE64


const boot0_file_head_t  fes1_head = 
{
    {
        /* jump_instruction */         
        ( 0xEA000000 | ( ( ( sizeof( boot0_file_head_t ) + sizeof( int ) - 1 ) / sizeof( int ) - 2 ) & 0x00FFFFFF ) ),
        BOOT0_MAGIC,
        STAMP_VALUE,
		32,
        sizeof( boot_file_head_t ),
        BOOT_PUB_HEAD_VERSION,
        CONFIG_BOOT0_RET_ADDR,
        CONFIG_BOOT0_RUN_ADDR,
        0,
        {
        0, 0,0,0, '4','.','0',0
        },
    },

    {
        //__u32 prvt_head_size;
        0,
        //char prvt_head_vsn[4];      
        1,
#if defined(MERIDA)
        //unsigned int                dram_para[32] ;
        {120,           //0x00
         7,             //0x04 -> auto 7
         0x3939b9,      //0x08 -> auto 3939b9
        0x3,            //0x0c
        0x10E210E2,     //0x10 -> auto 0xb000b0
        0x1001,         //0x14 -> auto 0x1
        0x0,            //0x18
        0x03,           //0x1c
        0x11,           //0x20
        0x2,            //0x24 -> auto 2
        0x0048A258,
        0x01B1B28F,
        0x00047028,
        0x050005dc,
        0,
        0,
       0,               //0x40
        0x2a066270,     //0x44 -> auto mode (lower bits)
         0,             //0x48
         0,             //0x4c -> auto 0 (pll setting)
        0x0000442b,     //0x50 -> auto 0x446F
        0x13860053,     //0x54 -> auto 0x18634a8
        0x88770000,     //0x58 -> auto 0x88770000
        0x04002904,},   //0x5c
#elif defined(PINE64)
        //unsigned int                dram_para[32] ;
        {0x2a0,           
         3,             
         0x3b3bbb,      
        0x1,            
        0x10E410E4,     
        0x1000,         
        0x1840,            
        0x40,          
        0x1800,          
        0x2,            
        0x004a2195,
        0x02424190,
        0x08b060,
        0x04b005dc,
        0,
        0,
       0,               
        0,    
         0,             
         0,             
        0x00008808,     
        0x20250000,     
        0,    
        0x040008,},  
#else
		{}, 
#endif
        //__s32			     uart_port;   
        0,
        //normal_gpio_cfg       uart_ctrl[2];  
        {
        //{ 6, 2, 4, 1, 1, 0, {0}},//PB8: 4--RX
        //{ 6, 4, 4, 1, 1, 0, {0}},//PB9: 4--TX
            { 2, 8, 4, 1, 0xff, 0xff, {0}},//PB8: 4--RX
            { 2, 9, 4, 1, 0xff, 0xff, {0}},//PB9: 4--TX
        },
        //__s32                         enable_jtag;  
        0,
        //normal_gpio_cfg	      jtag_gpio[5];   
        {{0},{0},{0},{0},{0}},
        //normal_gpio_cfg        storage_gpio[32]; 
        {
        //PC1,5-6,8-16: 3-SDC2
         {0},{0},{0},{0},{0},{0},{0},{0},
            {0},{0},{0},{0},{0},{0},{0},{0},
        { 3, 5, 3, 1, 3, 0xff, {0}},
        { 3, 6, 3, 1, 3, 0xff, {0}},
        { 3, 8, 3, 1, 3, 0xff, {0}},
        { 3, 9, 3, 1, 3, 0xff, {0}},
        { 3, 10, 3, 1, 3, 0xff, {0}},
        { 3, 11, 3, 1, 3, 0xff, {0}},
        { 3, 12, 3, 1, 3, 0xff, {0}},
        { 3, 13, 3, 1, 3, 0xff, {0}},
        { 3, 14, 3, 1, 3, 0xff, {0}},
        { 3, 15, 3, 1, 3, 0xff, {0}},
        { 3, 16, 3, 1, 3, 0xff, {0}},
        { 3, 1, 3, 1, 3, 0xff, {0}},

        },
        {0}
    }

};


/*******************************************************************************
*
*                  关于Boot_file_head中的jump_instruction字段
*
*  jump_instruction字段存放的是一条跳转指令：( B  BACK_OF_Boot_file_head )，此跳
*转指令被执行后，程序将跳转到Boot_file_head后面第一条指令。
*
*  ARM指令中的B指令编码如下：
*          +--------+---------+------------------------------+
*          | 31--28 | 27--24  |            23--0             |
*          +--------+---------+------------------------------+
*          |  cond  | 1 0 1 0 |        signed_immed_24       |
*          +--------+---------+------------------------------+
*  《ARM Architecture Reference Manual》对于此指令有如下解释：
*  Syntax :
*  B{<cond>}  <target_address>
*    <cond>    Is the condition under which the instruction is executed. If the
*              <cond> is ommitted, the AL(always,its code is 0b1110 )is used.
*    <target_address>
*              Specified the address to branch to. The branch target address is
*              calculated by:
*              1.  Sign-extending the 24-bit signed(wro's complement)immediate
*                  to 32 bits.
*              2.  Shifting the result left two bits.
*              3.  Adding to the contents of the PC, which contains the address
*                  of the branch instruction plus 8.
*
*  由此可知，此指令编码的最高8位为：0b11101010，低24位根据Boot_file_head的大小动
*态生成，所以指令的组装过程如下：
*  ( sizeof( boot_file_head_t ) + sizeof( int ) - 1 ) / sizeof( int )
*                                              求出文件头占用的“字”的个数
*  - 2                                         减去PC预取的指令条数
*  & 0x00FFFFFF                                求出signed-immed-24
*  | 0xEA000000                                组装成B指令
*
*******************************************************************************/

