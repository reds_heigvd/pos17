/**
 * FILE: cmd_hello.c
 *
 * @brief Console `Hello World!`.
 *
 * @author Julien Baeriswyl (julien.baeriswyl@heig-vd.ch)
 * @since  2017-10-07
 */
#include <common.h>
#include <command.h>

#define HELLO_STR "Hello World!"

static int do_hello (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	puts(HELLO_STR "\n");
	return 0;
}

/*
 * Command instantiation with:
 *  - 1 arg max (0 won't work)
 *  - Allow repeat
 */
U_BOOT_CMD(
	hello,	1,	1,	do_hello,
	"Print `" HELLO_STR "` in console",
	""
);

#undef HELLO_STR
