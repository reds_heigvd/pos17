POS-A: Projects
==================================================

University of Applied Science                    | Institute
------------------------------------------------ | -------------------------------------------
![HEIG-VD](./documentation/img/heig-vd_logo.png) | ![ReDS](./documentation/img/ReDS_logo.jpg)

## Reptar

## Lime2

## Raspberry Pi 3
### Team
Lastname                  | Firstname            
------------------------- | ---------------------
Hauke                     | Sydney
Pineda Serna              | Camilo

### [Project report](./documentation/rpi3/README.md)

## Pine64

## Altera DE1

## Zybo
### Team
Lastname                  | Firstname            
------------------------- | ---------------------
Othenin-Girard            | Gaëtan
Sivillica                 | Luca

### [Project report](./documentation/zybo/Rapport_Intermediaire.md)

## Parallella
### Team
Lastname                  | Firstname            
------------------------- | ---------------------
Baeriswyl                 | Julien

### [Project report](./documentation/parallella/README.md)
