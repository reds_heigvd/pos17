#!/bin/bash

################################################################################
# --------------- POS-A: project - Automated deployment script --------------- #
################################################################################
#                                                                              #
# @author    : Julien Baeriswyl                                                #
# @date      : 2017-09-28                                                      #
#                                                                              #
################################################################################

cd $(readlink -f $(dirname $0))
. ./pOS_func.sh

script_help ()
{
    echo -e "USAGE:   $0 <option> <boardname> [<device> <partitionN>]"
    echo -e 'OPTIONS:
\t--bin       compile binaries for given board
\t--sd        prepare SD card with precompiled binaries (need optional args)
\t--full      do `--bin` and `--sd` (need optional args)
\t--cleanall  remove all objects and precompiled binaries from folders
\t--sim       launch simulation of `vExpress` in QEMU
\t--help      display this help

SUPPORTED BOARDS:
\tvexpress
\treptar
\tlime2
\tparallella'
}

arg=$1
shift
board=$2
shift

case $arg in
    --bin)
        pOS_${board}_bin
        ;;
    --sd)
        pOS_${board}_load $@
        ;;
    --full)
        pOS_${board}_bin
        pOS_${board}_load $@
        ;;
    --cleanall)
        pOS_clean
        ;;
    --sim)
        ./stv
        ;;
    --help)
        script_help
        ;;
    *)
        echo -e 'ERROR: invalid option\n'
        script_help
        ;;
esac
