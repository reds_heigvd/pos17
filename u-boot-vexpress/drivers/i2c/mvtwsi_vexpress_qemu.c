/*
 * POS : (MFH) custom I2C inspired by mvtwsi driver.
 */

#include <common.h>
#include <i2c.h>
#include <asm/errno.h>
#include <asm/io.h>

/*
 * include a file that will provide CONFIG_I2C_MVTWSI_BASE
 * and possibly other settings
 */
#include <asm/arch/i2c.h>

/********* led rgb **********/
static int led_reg_r = 0x00;
static int led_reg_g = 0x00;
static int led_reg_b = 0x00;
static int led_reg_pwm = 0x00;
static int led_reg_mode = 0x00;

/*
 * I2C init called by cmd_i2c when doing 'i2c reset'.
 * Sets baud to the highest possible value not exceeding requested one.
 */
void i2c_init(int requested_speed, int slaveadd)
{
	/* reset controller */
	led_reg_r = 0x00;
	led_reg_g = 0x00;
	led_reg_b = 0x00;
	led_reg_pwm = 0x00;
	led_reg_mode = 0x00;
}

/*
 * Begin I2C transaction with expected start status, at given address.
 * Common to i2c_probe, i2c_read and i2c_write.
 * Expected address status will derive from direction bit (bit 0) in addr.
 */
static int i2c_begin(int expected_start_status, u8 addr)
{
	int status = 0;
	return status;
}

/*
 * I2C probe called by cmd_i2c when doing 'i2c probe'.
 * Begin read, nak data byte, end.
 */
int i2c_probe(uchar chip)
{
	int status;

	if(chip == 0x40)
		status = 0;
	else
		status = -1;

	/* return 0 or status of first failure */
	return status;
}

/*
 * I2C read called by cmd_i2c when doing 'i2c read' and by cmd_eeprom.c
 * Begin write, send address byte(s), begin read, receive data bytes, end.
 */
int i2c_read(u8 dev, uint addr, int alen, u8 *data, int length)
{
	int status = 0;

	/* begin i2c write to send the address bytes */
	status = i2c_begin(0, 0);

	/* begin i2c read to receive eeprom data bytes */
	if (status == 0)
		status = i2c_begin(0, 0);

	/* Emule the RGB led */
	if(dev == 0x040 && addr == 0x82 && alen == 1) {
		*data = led_reg_r;
	}
	else if (dev == 0x040 && addr == 0x84 && alen == 1) {
		*data = led_reg_g;
	}
	else if (dev == 0x040 && addr == 0x83 && alen == 1) {
		*data = led_reg_b;
	}
	else if (dev == 0x040 && addr == 0x0c && alen == 1) {
		*data = led_reg_pwm;
	}
	else if (dev == 0x040 && addr == 0x00 && alen == 1) {
		*data = led_reg_mode;
	}
	else {
		*data = 0x00;
	}

	/* return 0 or status of first failure */
	return status;
}

/*
 * I2C write called by cmd_i2c when doing 'i2c write' and by cmd_eeprom.c
 * Begin write, send address byte(s), send data bytes, end.
 */
int i2c_write(u8 dev, uint addr, int alen, u8 *data, int length)
{

	int status = 0;

	/* begin i2c write to send the eeprom adress bytes then data bytes */
	status = i2c_begin(0, 0);

	/* Emule the RGB led */
	if (dev == 0x40 && addr == 0x82 && alen == 1 && length == 1) {
		led_reg_r = *data;
	}
	else if (dev == 0x40 && addr == 0x84 && alen == 1 && length == 1) {
		led_reg_g = *data;
	}
	else if (dev == 0x40 && addr == 0x83 && alen == 1 && length == 1) {
		led_reg_b = *data;
	}
	else if (dev == 0x40 && addr == 0x0c && alen == 1 && length == 1) {
		led_reg_pwm = *data;
	}
	else if (dev == 0x40 && addr == 0x00 && alen == 1 && length == 1) {
		led_reg_mode = *data;
	}

	if (led_reg_mode == 0x80 && led_reg_pwm == 0x2a) {
		printf("\nLED RGB %d %d %d\n",led_reg_r, led_reg_g, led_reg_b);
	}

	/* return 0 or status of first failure */
	return status;

}

/*
 * Bus set routine: we only support bus 0.
 */
int i2c_set_bus_num(unsigned int bus)
{
	if (bus > 0) {
		return -1;
	}
	return 0;
}

/*
 * Bus get routine: hard-return bus 0.
 */
unsigned int i2c_get_bus_num(void)
{
	return 0;
}
