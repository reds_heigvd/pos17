#!/bin/bash

################################################################################
# -------------------- POS-A: Automated install functions -------------------- #
################################################################################
#                                                                              #
# @author    : Julien Baeriswyl                                                #
# @date      : 2017-09-28                                                      #
#                                                                              #
################################################################################

pOS_assert ()
{
    if ! [ ${@:1:$(($#-1))} ]; then
        echo "ASSERT: ${@:1:$(($#-1))}, ${@: -1}"
        exit 1
    fi
}

pOS_clean ()
{
    for $folder in 'qemu-2.6.0' 'u-boot-vexpress' 'u-boot-2016.09' 'so3' 'linux-3.18' 'x-loader-1.51-reptar' 'u-boot-parallella-gen1'
    do
        cd $folder
        if [ $? -eq 0 ]; then
            make clean
            cd ..
        fi
    done
}

pOS_uboot ()
{
    cd u-boot-2016.09 || exit 1
    make $1'_defconfig'
    make -j8
    cd ..
}

pOS_linux ()
{
    cd linux-3.18 || exit 1
    make $1'_defconfig'
    make zImage -j8
    make dtbs
    cd ..
}

pOS_so3 ()
{
    cd so3 || exit 1
    make $1'_defconfig'
    make -j 8
    cd ..
}

pOS_term ()
{
    cd filesystems || exit 1
    ./mount_cpio.sh
    sudo sed -i 's/^tty/#tty/g' initrd/etc/inittab
    sudo sed -i 's/^\#tty'$1'/tty'$1'/g' initrd/etc/inittab
    ./umount_cpio.sh
    cd ..
}

pOS_itb ()
{
    cd target || exit 1
    ./mkuboot.sh $1
    cd ..
}

pOS_vexpress_bin ()
{
    cd qemu-2.6.0 || exit 1
    ./configure --target-list=arm-softmmu --disable-attr --disable-werror --disable-docs
    make -j8
    cd ..
    
    cd u-boot-vexpress || exit 1
    make vexpress_ca15_tc2_qemu_config
    make -j8
    cd ..
    
    pOS_linux 'vexpress'
    pOS_so3   'vexpress'
    pOS_term  'AMA0'
    pOS_itb   'vexpress'
}

pOS_reptar_bin ()
{
    cd x-loader-1.51-reptar || exit 1
    make reptar_config
    make -j8
    cd ..
    
    pOS_uboot 'omap3_reptar'
    pOS_linux 'omap3_reptar'
    pOS_term  'O2'
    pOS_itb   'reptar'
}

pOS_lime2_bin ()
{
    pOS_uboot 'A20-OLinuXino-Lime2'
    pOS_linux 'sunxi'
    pOS_term  'S0'
    pOS_itb   'lime2'
}

pOS_parallella_tools ()
{
    sudo apt-get install -y u-boot-tools
}

pOS_parallella_bin ()
{
    export ARCH=arm
    export CROSS_COMPILE=arm-xilinx-eabi-
    export PATH="/opt/Xilinx/SDK/gnu/arm/lin/bin:$PATH"
    cd parallella-uboot
    make parallella_config
    make -j8
    cd ..
}

pOS_reptar_load ()
{
    echo -e "o\nx\nh\n255\ns\n63\nc\n15\nr\nn\np\n\n\n\na\nt\nc\np\nw" | sudo fdisk $1 && sudo mkfs.vfat -F 32 $1$2
    
    cd filesystems || exit 1
    sudo mount $1$2 fs || exit 1
    sudo cp ../u-boot-2016.09/u-boot.bin fs
    sudo cp ../x-loader-1.51-reptar/MLO fs
    sudo cp ../target/reptar.itb fs
    sudo umount fs
    cd ..
}

pOS_lime2_load ()
{
    echo -e "o\nn\np\n1\n\n+16M\np\nw" | sudo fdisk $1 && sudo mkfs.vfat $1$2
    
    sudo dd if=./u-boot-2016.09/u-boot-sunxi-with-spl.bin of=$1 bs=1024 seek=8 || exit 1
    
    cd filesystems || exit 1
    sudo mount $1$2 fs || exit 1
    sudo cp ../target/lime2.itb fs
    sudo umount fs
    cd ..
}

pOS_parallella_load ()
{
    echo -e "o\nx\nh\n255\ns\n63\nc\n15\nr\nn\np\n\n\n\na\nt\nc\np\nw" | sudo fdisk $1 && sudo mkfs.vfat -F 32 $1$2
    
    cd filesystems || exit 1
    sudo mount $1$2 fs || exit 1
    sudo cp ../u-boot-parallella-gen1/u-boot.bin fs
    sudo umount fs
    cd ..
}
